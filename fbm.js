/**
 * @Date:   2019-04-09T11:20:16+02:00
 * @Last modified time: 2019-10-24T15:04:03+02:00
 */



/**
 * This module provides functionality to create visual and auditory feedback.
 * @namespace fbm
 * @memberof MTLG.utils
 * @example
 *
 * //Start by creating a target object.
 * var targetcontainer = new createjs.Container();
 * targetcontainer.name="Ziel";
 * //Add some children to the container
 * targetcontainer.addChild(childA,childB, ...);
 * targetcontainer.x = 1000;
 * targetcontainer.y = 500;
 * //Setting the bounds is neccessary since containers do not by default have bounds
 * targetcontainer.setBounds(0,0,width,height);
 * //This is set to fine-tune the feedback position
 * targetcontainer.feedbackY=-0.13;
 *
 * //Create a content settings object for the feedback.
 * //Shows the hand-animation going from [50,50] to the target
 * var movetotarget = MTLG.utils.fbm.createContent("movetotarget");
 * movetotarget.pointCounter = 1;
 * movetottarget.pointDelay = 0;
 * movetotarget.pointStartX = 50;
 * movetotarget.pointStartY = 50;
 * movetotarget.pointEndX = null;
 * movetotarget.pointEndY = null;
 * movetotarget.pointRotation = 0;
 * movetotarget.pointSpeed = 400;
 * movetotarget.pointSize = 200;
 *
 * //Create a timing settings object for the feedback.
 * var overallTiming = MTLG.utils.fbm.createTiming("overallTiming");
 * overallTiming.movable=false;
 * overallTiming.movableMini=false;
 * overallTiming.openDuration=0;
 * overallTiming.miniDuration=0;
 * overallTiming.totalDuration=0;
 * overallTiming.closeable=false;
 * overallTiming.requestable=true;
 * overallTiming.minimizable=true;
 * overallTiming.reqPointer=false;
 * overallTiming.rotateOnStart=true;
 *
 * //Create a style settings object for the feedback.
 * var neutralStyle = MTLG.utils.fbm.createStyle("neutralStyle");
 * neutralStyle.screenLocked= true;
 * neutralStyle.fontSize = null;
 * neutralStyle.focus = 0;
 * neutralStyle.fontType = "Arial";
 * neutralStyle.textAlign = "center";
 * neutralStyle.color = 'LightCyan';
 * neutralStyle.bordercolor = 'LightBlue';
 * neutralStyle.buttonColor = 'LightCyan';
 * neutralStyle.curve=0.5;
 *
 * //Trigger the feedback
 * MTLG.utils.fbm.giveFeedback(targetcontainer,"movetotarget","overallTiming","neutralStyle")
 */
var FBModule = (function(){
  var idb = require('idb'); //.idb;
  require.context('./img');
  require.context('./sounds');
  var currentUser = "TestUser"; //Sobald das Framework ein User-System bereitstellt, kann der Identifier des Users hier reingeschrieben werden
  var options;

  //Vorladen der Templates und Config aus IDB -> Asynchron
  var init = function( opt, callback ){
    options=opt; //Speichert Spieloptionen
    allManager.load(currentUser,function(){callback("fbm-loaded");}); //Lädt alle gespeicherten Templates und config
    return "fbm-loaded"; //Gibt Identifier des Callbacks zurück
  };


  //Lädt IDB
  var IDB = (function (){
    //Verbesserung von IDB mit Promises; Source: https://github.com/jakearchibald/idb
    const dbPromise = idb.open('FeedbackDB-store', 1, upgradeDB => {
      upgradeDB.createObjectStore('FeedbackDB');
    });

    const idbfeedback = {
      get(key) {
        return dbPromise.then(db => {
          return db.transaction('FeedbackDB')
            .objectStore('FeedbackDB').get(key);
        });
      },
      set(key, val) {
        return dbPromise.then(db => {
          const tx = db.transaction('FeedbackDB', 'readwrite');
          tx.objectStore('FeedbackDB').put(val, key);
          return tx.complete;
        });
      },
      delete(key) {
        return dbPromise.then(db => {
          const tx = db.transaction('FeedbackDB', 'readwrite');
          tx.objectStore('FeedbackDB').delete(key);
          return tx.complete;
        });
      },
      clear() {
        return dbPromise.then(db => {
          const tx = db.transaction('FeedbackDB', 'readwrite');
          tx.objectStore('FeedbackDB').clear();
          return tx.complete;
        });
      },
      keys() {
        return dbPromise.then(db => {
          const tx = db.transaction('FeedbackDB');
          const keys = [];
          const store = tx.objectStore('FeedbackDB');

          // This would be store.getAllKeys(), but it isn't supported by Edge or Safari.
          // openKeyCursor isn't supported by Safari, so we fall back
          (store.iterateKeyCursor || store.iterateCursor).call(store, cursor => {
            if (!cursor) return;
            keys.push(cursor.key);
            cursor.continue();
          });

          return tx.complete.then(() => keys);
        });
      }
    };
    return {
      FeedbackDB: idbfeedback,
    }
  })();

  //Sammelt Einstellungen, steuert Erstellung und Erbringung des Feedbacks
  var Controller = (function (){
    var config; //Standartkonfiguration

    /**
     * This function can be used to create and display feedback defined by the given settings.
     * @param {object} target The target object for the visual feedback. Mainly used for size, positioning and rotation. Should have bounds defined.
     * @see {@link MTLG.utils.fbm.createTarget} for addititional properties.
     * @param {object} content The content settings for the feedback, created using createContent.
     * @param {object} timing The timing settings for the feedback, created using createTiming.
     * @param {object} style The style settings for the feedback, created using createStyle.
     * @return {object} The feedback object.
     * @function giveFeedback
     * @memberof MTLG.utils.fbm#
     */
    function giveFeedback(target, content, timing, style){
        //Bereitet Einstellungen und Target vor
        var SettingObject = SettingsLoader(content, timing, style); //lädt alle Einstellungen in ein Objekt
        var TargetObject = TargetLoader(target);

        //Erstellt Feedback aus den Einstellungen
        var FBObject = Creator.createFeedback(TargetObject, SettingObject);

        //Zeigt Feedback an
        Viewer.viewFeedback(FBObject, TargetObject, SettingObject);

        //Aufbereitung Daten für Learning Analytics
        var LearningAnalyticsData = createLAData(TargetObject,content,timing,style);
        //Übergabe der Daten ans Framework
        //"Erst von nächster Version des MTLG-Frameworks unterstützt, Modul wurde jedoch für frühere Version entwickelt, kann bei Aktualisierung reinkommentiert werden."
        //MTLG.datacollector.storeEvents([LearningAnalyticsData],MTLG.getToken(username),function(){},function(){});

        //Übergibt Feedback, damit z.B: object.deleteFB() genutzt werden kann.
        return FBObject;
      }

    //Nimmt Referenzen auf TargetTemplates, sowie TargetTemplate und Objekte entgegen. Gibt einheitlich ein TargetTemplate zurück mit den benötigten Werten
    function TargetLoader(target){
      if (typeof(target)=="string"){
        //Falls Target ein String ist, wird das referenzierte Objekt geladen
        TargetObject = TargetManager.get(target);
      }else{
        //Überprüft, ob evt. Template übergeben wurde, wenn nicht:
        if(target.type!="Target"){
          //Erzeugt leeres TargetTemplate und überträgt Werte
          TargetObject = new TargetManager.create();
          TargetObject.object=target;
          TargetObject.name=target.name;

          //Setzt Template auf rel. Position des Objekts
          TargetObject.x=(target.x)/options.width;
          TargetObject.y=(target.y)/options.height;
          TargetObject.rotation=target.rotation;

          //Probiert Mitte des Objekts zu bestimmen und verbessert Position, falls möglich
          var bounds = target.getBounds();
          if (bounds != null){
            TargetObject.x+=(bounds.width/2)/options.width;
            TargetObject.y+=(bounds.height/2)/options.height;
          }

          //Falls Regestrierungspunkte verändert wurden, muss Mitte ebenfalls angepasst werden.
          if(target.regX!=0){TargetObject.x-=target.regX/options.width;}
          if(target.regY!=0){TargetObject.y-=target.regY/options.height;}

          //Der manuell bestimmte Offset, falls keiner bestimmt wurde, wird dieser auf 0 gesetzt
          if(target.feedbackX!=null){TargetObject.offsetX=target.feedbackX*options.width}else{TargetObject.offsetX=0}
          if(target.feedbackY!=null){TargetObject.offsetY=target.feedbackY*options.height}else{TargetObject.offsetY=0}

          //Manuell bestimmter Rotationsoffset vom Targetobjekt, sowie Richtung des RequestPointer
          if(target.feedbackRoation!=null){TargetObject.rotation+=target.feedbackRotation};
          if(target.reqPointerRotation!=null){TargetObject.reqPointerRotation=target.reqPointerRotation}else{TargetObject.reqPointerRotation=calcAngle(TargetObject.x*options.width+TargetObject.offsetX,TargetObject.y*options.height+TargetObject.offsetY,TargetObject.x*options.width,TargetObject.y*options.height)}

          //Hilfsfunktionen zur Berechnung des Winkels vom Indicator aufs Objekt (Source Tabula RegEx)
          function calcAngle(x1,y1,x2,y2) {
                ankathete = y2 - y1;
                gegenkathete = x2 - x1;
            if (ankathete > 0) {
              var degree = (Math.atan(gegenkathete / ankathete) * 180 / Math.PI) % 360;
            } else {
              var degree = ((Math.atan(gegenkathete / ankathete) * 180 / Math.PI) - 180) % 360;
            }
            return degree;
          }

        //übergebenes Objekt ist TargetTemplate und muss nicht bearbeitet werden
        }else{return target;}
      }
      return TargetObject;
    }

    //Lädt alle benötigten Einstellungen in ein Objekt
    function SettingsLoader(content, timing, style){
        var SettingObject = Object.assign({},ContentManager.get(content),TimingManager.get(timing),StyleManager.get(style)); //merged Kopie der Objekte; Benötigt ECM5!
        for(var setting in SettingObject){
          //sucht nach fehlenden Einstellungen, Ersetz sie mit Standartconfig
          if((typeof(SettingObject[setting]) == 'undefined')||(SettingObject[setting] == null)){
            SettingObject[setting]=config[setting];
          }
        }
        SettingObject = sanitizeObject(SettingObject); //Bereinigt SettingObject
        return SettingObject; //Gibt Objekt mit allen Einstellungen zurück
      }

    //Entfernt problematische Werte aus der Nutzerkonfiguration
    function sanitizeObject(SettingObject){
        if (SettingObject.text == ""){
          //createjs.text verhält sich unschön bei leeren Strings
          SettingObject.text = null;
          console.log("Warning: empty string, use null if you don´t want to display text");
        }

        //Bei Erweiterungen können hier weitere problematische Werte abgefangen werden

        return SettingObject;
    }

    //erzeugt ein zentrales Objekt mit den wichtigstens Daten des Feedbacks zur späteren Analyse
    function createLAData(target, content, timing, style){
      var laData = {};
      laData.target=getName(target);
      laData.x=target.x;
      laData.y=target.y;
      laData.content=getName(content);
      laData.timing=getName(timing);
      laData.style=getName(style);
      laData.time=new Date();
      return laData;

      //Hilfsfunktion um Namen unabhängig vom Objekttypen festzustellen
      function getName(object){
        if(typeof(object)!='undefined'){
          if (typeof(object)=='string'){
            return object;
          }else{
            if (typeof(object.name)=='string'){
              return object.name;
            }else{
              return "No name, specify .name manually for objects";
            }
          }
        }
      }

    }

    //Lädt Standart-Configuration
    async function loadConfig(){
        var Keys = await IDB.FeedbackDB.keys(); //lädt gespeicherte Referenzen
        if (Keys.includes("LocalConfig:"+currentUser)){
          //Local Config vorhanden
          config = await IDB.FeedbackDB.get("LocalConfig:"+currentUser);
        }else{
          //Keine Local Config vorhanden
          if (Keys.includes("GlobalConfig")){
            //Global Config vorhanden, erzeuge neue Local Config
            config = await IDB.FeedbackDB.get("GlobalConfig");
            saveConfig(config);
          }else{
            //Keine Local/Global Config vorhanden, erzeuge und speichere neue Config
            console.log("Warning: No Config found");
            resetConfig();
          }
        }
      }

    //Speichert Configuration
    function saveConfig(config){
      IDB.FeedbackDB.set("LocalConfig:"+currentUser, config);
    }

    /**
     * Method to reset the config to default values.
     * Also reverts changes in IDB.
     * @function resetConfig
     * @memberof MTLG.utils.fbm#
     */
    function resetConfig(){
      config = {
        text: null,
        textWidth: 1,
        textHeight: 1,
        textX: 0.5,
        textY: 0.5,
        img: null,
        imgWidth: 1,
        imgHeight: 1,
        imgX: 0.5,
        imgY: 0.5,
        imgRatioLocked: true,
        totalWidth: 0.1,
        totalHeight: 0.1,
        sound: null,
        soundProperties: null,
        soundLoop: false,
        pointCounter : null,
        pointDelay: 0,
        pointStartX : null,
        pointStartY : null,
        pointEndX : null,
        pointEndY : null,
        pointRotation : 0,
        pointSpeed : null,
        pointSize : null,
        flair: null,
        flairSize: 25,
        flairTime: 0.5,
        flairIntensity: 1,
        delay : 0,
        totalDuration : 60,
        openDuration: 0,
        miniDuration: 0,
        requestable : 0,
        reqPointer: true,
        closeable : 1,
        minimizable: 1,
        movable: 1,
        movableMini: false,
        rotateOnStart: false,
        rotateOnMove: true,
        openCallback: null,
        closeCallback: null,
        theme: 'Dark',
        color: '#FFFFFF',
        buttonColor: '#FFFFFF',
        curve: 0.5,
        visibility: 1, //bisher nur 1 Fenster-Typ bei dem Transparenz unschön aussieht, zur Erweiterung aber definiert
        focus: 0,
        screenLocked: true,
        windowtype: 'window', //bisher nur 1 Typ, zur Erweiterung aber definiert
        fontType: 'Arial',
        fontSize: null,
        fontColor: '#000000',
        textAlign: 'left',
        volume: 1,
      }
      saveConfig(config);
    }

    /**
     * Method to get the config from outside of the module
     * @return {object} the configuration object
     * @function getConfig
     * @memberof MTLG.utils.fbm#
     */
    function getConfig(){
      return config;
    }

    /**
     * Method to set new configs for the module.
     * Saves config in module and local storage (IDB).
     * @parameter {object} the new configuration object
     * @function setConfig
     * @memberof MTLG.utils.fbm#
     */
    function setConfig(newconfig){
      config = newconfig;
      saveConfig(config);
    }

    //Gibt Interface von Controller zurück
    return{
        getConfig: getConfig,
        loadConfig: loadConfig,
        setConfig: setConfig,
        resetConfig: resetConfig,
        giveFeedback: giveFeedback,
      }
  })();

  //Factory für Manager mit beliebigen Typen
  var Manager = function(Type,List){
      var Keys = new Set(); //Speichert Keys der Templates

      //new
      function create(key){
        var object = new Type(key); //erzeugt neues Template
        if (key != null){
          //falls key übergeben, wird Template direkt gespeichert
          set(object,key);
        }
        return object;
      }

      //get
      function get(key){
        if (typeof(key) == "string"){
        //Es wurde Referenz übergeben
          if (typeof(List[key]) ==  'undefined'){
            //Kein Objekt unter dieser Referenz gespeichert
            console.log("Error: "+key+" nicht gefunden, fehlerhafte Referenz");
            key = create();
            return key; //leeres Objekt erzeugen und zurückgeben
          }else{
            //Objekt vorhanden
            return List[key];
          }
        }else{
        //Es wurde keine Referenz übergeben
          if (key == null){
            //Es wurde kein Parameter übergeben
            key = create();
            return key; //leerese Objekt erzeugen und zurückgeben
          }else{
            //Es wurde ein Objekt übergeben
            return key;
          }
        }
      }

      //getcopy
      function getCopy(key){
        //um Template als Vorlagen für neue Templates zu übernehmen
        return Object.assign({},get(key));
      }

      //set
      function set(obj, key){
        //Objekt wird unter Referenz gespeichert
        if(key==null){
          //Keine Referenz zum speichern, probiere Namen als Referenz zu nehmen
          if (obj.name != null){
            key=obj.name;
          }else{
            console.log("Error: Keine Referenz oder Namen zum Abspeichern übergeben");
            return;
          }
        }
        List[key] = obj; //wird in Liste die Templates verwaltet gespeichert
        updatelist(key); //wird in IDB gespeichert
      }

      //del
      function del(key){
        //referenziertes Objekt wird gelöscht
        delete List[key]; //Objekt wird gelöscht aus live-Umgebung
        IDB.FeedbackDB.delete(key+":"+currentUser); //Objekt wird gelöscht aus IDB
        Keys.delete(key); //Referenz auf Objekt wird gelöscht in Live-Umgebung
        IDB.FeedbackDB.set(Type().type+"KeyList"+":"+currentUser, Keys) //Referenzen werden aktuallisiert gespeichert in IDB
      }

      //clear
      function clear(){
        IDB.FeedbackDB.clear(); //alle Einträge der IDB werden gelöscht
      }

      //Aktuallisiert Liste der Keys und speichert das referenzierte Template
      function updatelist(key){
          Keys.add(key); //wird zu den Referenzen hinzugefügt
          savefile(key); //wird in IDB gespeichert
          IDB.FeedbackDB.set(Type().type+":KeyList"+":"+currentUser, Keys); //speichert aktuallisierte Liste der Referenzen
      }

      //Lädt gesamte Liste
      async function loadlist(otherUser){
        var User = otherUser || currentUser; //falls kein User übergeben, nehme aktuellen User
        Keys = await IDB.FeedbackDB.get(Type().type+":KeyList"+":"+User); //lädt Liste der Referenzen des Users
        if (typeof(Keys)!='undefined'){
          //iteriert durch Keys und lädt alle referenzierten Templates
          iterator = Keys.entries();
          for (let entry of iterator){
            await loadfile(entry[0],User);
          }
        }else{
          //Keine Keys gespeichert unter dem Nutzernamen
          Keys = new Set();
          IDB.FeedbackDB.set(Type().type+":KeyList"+":"+User, Keys);
          console.log("Error: Corrupted or missing "+Type().type+"-Templates for User: "+User);
      }
      }

      //laden einzelner Templates
      async function loadfile(key,User){
        var obj = await IDB.FeedbackDB.get(Type().type+":"+key+":"+User);
        set(obj,key);
      }

      //speichert gesamte Liste
      function savelist(){
        //Speichert jedes Element der Liste
        for(var key in List){
          Keys.add(key);
          savefile(key);
        }
        //Speichert die Referenzen der Liste
        IDB.FeedbackDB.set(Type().type+":KeyList:"+currentUser, Keys);
      }

      //speichern einzelnes Objekt
      function savefile(key){
        var Obj = get(key);
        var sanitizedObj = sanitize(Obj);
        IDB.FeedbackDB.set(Type().type+":"+key+":"+currentUser, sanitizedObj);
      }

      //Entfernt kritische Einträge für IDB (wie z.B. functions)
      function sanitize(obj){
        //Bei TimingObjektn müssen die Einträge mit Callbacks entfernt werden, damit Object in IDB gespeichert werden kann.
        if ((obj.type=="Timing")&&((typeof(obj.openCallback)=='object')||(typeof(obj.closeCallback)=='object'))){
          var cleanObj = JSON.parse(JSON.stringify(obj)); //Beim Parsen werden die Functions des Objects entfernt

          return cleanObj;
        }

        //Bei Erweiterung mit Werten die nur beim Abspeichern und nicht zur Laufzeit kritisch sind hier santizen

        return obj; //Nicht TimingObjekte sind unkritisch
      }

      //Interface des Managers wird zurückgegeben
      return{
        create: create,
        get: get,
        getCopy: getCopy,
        set: set,
        del: del,
        save: savelist,
        load: loadlist,
        clear: clear,
      }
    }

  //zentrale Funktionen für alle instanzierten Manager
  var allManager = (function(){
    /**
     * Loads all templates of all managers and config of controller from IDB
     * @param {string} user The user name (used as IDB storage name)
     * @param {function} callback The function to be called after loading complete
     * @function load
     * @memberof MTLG.utils.fbm#
     */
    var load = async function (user,callback){
      await ContentManager.load(user);
      await StyleManager.load(user);
      await TimingManager.load(user);
      await TargetManager.load(user);
      await Controller.loadConfig();
      console.log("FBM loaded");
      if(typeof(callback)=="function"){callback();}
    }
    /**
     * Saves all templates to IDB
     * @function save
     * @memberof MTLG.utils.fbm#
     */
    var save = function (){
      ContentManager.save();
      StyleManager.save();
      TimingManager.save();
      TargetManager.save();
    }
    /**
     * Deletes all templates from IDB
     * @function clear
     * @memberof MTLG.utils.fbm#
     */
    var clear = function(){
      ContentManager.clear();
      StyleManager.clear();
      TimingManager.clear();
      TargetManager.clear();
    }
    //Interface von allManager, config wird nun direkt vom Controller bearbeitet -> Schnittstelle von Controller
    return {
      load: load,
      save: save,
      clear: clear,
    }
  })();

  //verwaltet Einstellungen zum Inhalt des Feedbacks
  var ContentManager = (function(){
      //Templates werden im jeweiligen Manger gespeichert
      var ContentList = {};

      //Definition eines Content-Templates
      function Content(key){
        this.type = "Content";
        this.name = key;
        this.text = null;
        this.textWidth = null;
        this.textHeight = null;
        this.textX = null;
        this.textY = null;
        this.img = null;
        this.imgWidth = null;
        this.imgHeight = null;
        this.imgX = null;
        this.imgY = null;
        this.imgRatioLocked = null;
        this.sound = null;
        this.soundProperties = null;
        this.soundLoop = null;
        this.totalWidth = null;
        this.totalHeight = null;
        this.pointCounter = null;
        this.pointDelay = null;
        this.pointStartX = null;
        this.pointStartY = null;
        this.pointEndX = null;
        this.pointEndY = null;
        this.pointRotation = null;
        this.pointSpeed = null;
        this.pointSize = null;
        this.flair = null;
        this.flairSize = null;
        this.flairTime = null;
        this.flairIntensity = null;
        return this;
      }
      //Instanzierung als Manager
      var CManager = new Manager (Content,ContentList)
      //Rückgabe der Schnittstelle
      return{
        /**
         * This function can be used to create a content configuration for feedback.
         * It is a wrapper for the Manager.create function and returns a configuration object.
         * See the properties for additional configuration properties.
         * @example
         * //Create a new content settings object using createContent
         * var startHelp = MTLG.utils.fbm.createContent("startHelp");
         * //Edit the properties of the content using the available properties.
         * startHelp.text="Bringe das passende Objekt ins Ziel";
         * startHelp.img="img/TestBild.png";
         * startHelp.textX=0;
         * startHelp.textY=0;
         * startHelp.textWidth=0.75;
         * startHelp.textHeight=1;
         * startHelp.imgX=0.9;
         * startHelp.imgY=0.5;
         * startHelp.imgWidth=0.25;
         * startHelp.imgHeight=0.8;
         * startHelp.imgRatioLocked=true;
         * startHelp.totalWidth=0.2;
         * startHelp.totalHeight=0.05;
         * @property {string} text Set to string of content if text should be displayed
         * @property {number} textWidth [0,1] Limit to width of text relative to feedback width
         * @property {number} textHeight [0,1] Limit to height of text relative to feedback height
         * @property {number} textX [0,1] Relative x position of text inside of feedback
         * @property {number} textY [0,1] Relative y position of text inside of feedback
         * @property {string} img Set to image path/identifier if image should be displayed
         * @property {number} imgWidth [0,1] Width of img relative to feedback width
         * @property {number} imgHeight [0,1] Height of img relative to feedback height
         * @property {number} imgX [0,1] Set relative x position of img inside of feedback
         * @property {number} imgY [0,1] Set relative y position of img inside of feedback
         * @property {boolean} imgRatioLocked Set to true if the image aspect ratio should be conserved
         * @property {string} sound Set to sound source path/identifier or the presets "positive" and "negative" to play that sound
         * @property {object} soundProperties Set to sound properties object. (See createjs docs for more information)
         * @property {boolean} soundLoop Set to true if sound should be looped.
         * @todo TODO: soundLoop not yet supported, can be done manually using soundProperties
         * @property {number} totalWidth [0,1] Set total width of feedback relative to screen width
         * @property {number} totalHeight [0,1] Set total height of feedback relative to screen height
         * @property {number} pointCounter Number of pointer (hand-animation) repetitions (0 or null to not use this feature)
         * @todo pointCounter of 0 will draw the hand but not trigger the animation
         * @property {number} pointDelay Milliseconds between each repetition. (default 0 immediate repetition)
         * @property {number} pointStartX [0,1] Relative x position of pointer start coordinate, set null to use target coordinates
         * @property {number} pointStartY [0,1] Relative y position of pointer start coordinate, set null to use target coordinates
         * @property {number} pointEndX [0,1] Relative x position of pointer end coordinate, set null to use target coordinates
         * @property {number} pointEndY [0,1] Relative y position of pointer end coordinate, set null to use target coordinates
         * @property {number} pointRotation Rotation of hand
         * @property {number} pointSpeed Speed of animation in pixels per second
         * @property {number} pointSize Absolute size of hand
         * @todo Pointer position depends on resolution (different in fullscreen) and changing size drastically changes positions
         * @property {string} flair Set to "positive" (green), "neutral" (blue) or "negative" (red) to get that kind of flair (colored aura feedback) on the given target
         * @property {number} flairSize Absolute size of flair around given object
         * @property {number} flairTime Time to display flair in seconds
         * @property {number} flairIntensity [0,1] Intensity of flair
         * @param {object} key The key for the content configuration
         * @return {object} The content configuration object corresponding to the specified key.
         * @function createContent
         * @memberof MTLG.utils.fbm#
         */
        create: CManager.create,
        /**
         * Returns a reference to the specified content settings
         * @param {object} key The key for the content configuration
         * @return {object} The content configuration object corresponding to the specified key.
         * @function getContent
         * @memberof MTLG.utils.fbm#
         */
        get: CManager.get,
        /**
         * Returns a (shallow) copy of the specified content settings
         * @param {object} key The key for the content configuration
         * @return {object} A (shallow) copy of the content configuration object corresponding to the specified key.
         * @function getCopyContent
         * @memberof MTLG.utils.fbm#
         */
        getCopy: CManager.getCopy,
        /**
         * Set a content object for the specified key.
         * @param {object} obj The settings object the key refers to
         * @param {object} key The key for the content configuration
         * @return {object} A (shallow) copy of the content configuration object corresponding to the specified key.
         * @function setContent
         * @memberof MTLG.utils.fbm#
         */
        set: CManager.set,
        /**
         * Delete the content object of the specified key.
         * @param {object} key The key for the content configuration to be deleted
         * @function delContent
         * @memberof MTLG.utils.fbm#
         */
        del: CManager.del,
        save: CManager.save,
        load: CManager.load,
        clear: CManager.clear,
      }
    })();

  //verwaltet Einstellungen zum Timing und zu den Interaktionsmöglichkeiten des Feedbacks - Aufbau analog zu ContentManager
  var TimingManager = (function(){
      var TimingList = {};
      function Timing(key){
        this.type = "Timing";
        this.name = key;
        this.delay = null;
        this.totalDuration = null;
        this.openDuration = null;
        this.miniDuration = null;
        this.requestable = null;
        this.reqPointer = null;
        this.closeable = null;
        this.minimizable = null;
        this.movable = null;
        this.movableMini = null;
        this.rotateOnStart = null;
        this.rotateOnMove = null;
        this.openCallback = null;
        this.closeCallback = null;
        return this;
      }
      var TManager = new Manager (Timing,TimingList)
      return{
        /**
         * This function can be used to create a timing configuration for feedback.
         * It is a wrapper for the Manager.create function and returns a configuration object.
         * See the properties for additional configuration properties.
         * @example
         * var targetTiming = MTLG.utils.fbm.createTiming("targetTiming");
         * targetTiming.movable=false;
         * targetTiming.closeable=true;
         * targetTiming.requestable=true;
         * targetTiming.reqPointer=true;
         * targetTiming.minimizable=true;
         * targetTiming.rotateOnStart=false;
         * targetTiming.openCallback = function(){targetfeedback=true};
         * targetTiming.closeCallback = function(){targetfeedback=false};
         * @property {number} delay Delay till feedback is shown after it was triggered in seconds
         * @property {number} totalDuration Total time feedback is shown in seconds
         * @property {number} openDuration Total time feedback is shown maximized in seconds. Resets if minimized
         * @property {number} miniDuration Total time feedback is shown minimized in seconds. Resets if maximized
         * @property {boolean} requestable If true: Draws button that can be clicked to request feedback. If false: feedback shown directly
         * @property {boolean} reqPointer If true: Draws indicator when minimized. If false: No indicator when minimized
         * @todo what is reqPointer there for? It always points down?
         * @property {boolean} closeable If true: Draws button to close feedback (can be used in combination with minimizable). If false: user can not close feedback
         * @property {boolean} minimizable If true: Draws button to minimize feedback (can be used in combination with closeable). If false: user can not minimize feedback
         * @property {boolean} movable If true: Feedback can be moved. If false: Feedback can not be moved
         * @property {boolean} movableMini If true: Feedback can be moved when minimized (movable has to be true). If false: Feedback can not be moved when minimized
         * @property {boolean} rotateOnStart If true: Feedback will be auto-rotated to "point" towards nearest corner. If false: Feedback is not rotated on start
         * @property {boolean} rotateOnMove If true: Feedback will be auto-rotated when moving. If false: Feedback preserves initial rotation when moved
         * @property {function} openCallback Callback that is called when feedback is drawn
         * @property {function} closeCallback Callback that is called when feedback is deleted
         * @param {object} key The key for the content configuration
         * @return {object} The content configuration object corresponding to the specified key.
         * @function createTiming
         * @memberof MTLG.utils.fbm#
         */
        create: TManager.create,
        get: TManager.get,
        getCopy: TManager.getCopy,
        set: TManager.set,
        del: TManager.del,
        save: TManager.save,
        load: TManager.load,
        clear: TManager.clear,
      }
    })();

  //verwaltet Einstellungen zur Darstellung des Feedbacks - Aufbau analog zu ContentManager
  var StyleManager = (function(){
      var StyleList = {};
      function Style(key){
        this.type = "Style";
        this.name = key;
        this.theme = null;
        this.color = null;
        this.buttonColor = null;
        this.curve = null;
        this.visibility = null;
        this.focus = null;
        this.windowtype = null;
        this.fontType = null;
        this.fontSize = null;
        this.fontColor = null;
        this.textAlign = null;
        this.volume = null;
        this.screenLocked = null;
        return this;
      }
      var SManager = new Manager (Style,StyleList)
      return{
        /**
         * This function can be used to create a style configuration for feedback.
         * It is a wrapper for the Manager.create function and returns a configuration object.
         * See the properties for additional configuration properties.
         * @example
         * var targetStyle = MTLG.utils.fbm.createStyle("targetStyle");
         * targetStyle.fontSize = 25;
         * targetStyle.fontType = "Arial";
         * targetStyle.focus=0;
         * targetStyle.textAlign = "center";
         * targetStyle.color = 'LightCyan';
         * targetStyle.bordercolor = 'LightBlue';
         * targetStyle.buttonColor = 'LightCyan';
         * targetStyle.curve=0.5;
         * @property {string} color Background color of feedback box.
         * @property {string} buttonColor Background-color of close button (x)
         * @property {number} curve [0,1] Roundness of box-corners. 0 is square, 1 is circle
         * @property {number} visibility Set transparency
         * @todo visibility is currently ignored, is this really transparency?
         * @property {number} focus Intensity of feedback focus. 1 is complete focus, 0 without focus
         * @property {string} windowtype Feedback box frame style, currently only "window"
         * @property {string} theme Feedback box frame theme, currently supports "Dark" and "Light" theme
         * @property {string} fontType The font type for feedback text (e.g. "Arial")
         * @property {number} fontSize The font size for feedback text
         * @property {string} fontColor The font color for feedback text
         * @property {string} textAlign The text alignment type for feedback text (e.g. "left", "center" or "right")
         * @property {number} volume The volume for feedback sound.
         * @todo volume: Currently not supported. Can be set manually using soundProperties.
         * @property {boolean} screenLocked Set to true to make sure that feedback object is on screen.
         * @todo screenLocked only works in fullscreen.
         * @param {object} key The key for the content configuration
         * @return {object} The content configuration object corresponding to the specified key.
         * @function createStyle
         * @memberof MTLG.utils.fbm#
         */
        create: SManager.create,
        get: SManager.get,
        getCopy: SManager.getCopy,
        set: SManager.set,
        del: SManager.del,
        save: SManager.save,
        load: SManager.load,
        clear: SManager.clear,
      }
    })();

  //verwaltet Einstellungen zur Positionierung des Feedbacks - Aufbau analog zu ContentManager
  var TargetManager = (function(){
      var TargetList = {};
      function Target(key){
        this.type = "Target";
        this.name = key;
        this.x = 0;
        this.y = 0;
        this.rotation = 0;
        this.reqPointerRotation = null;
        return this;
      }
      var TManager = new Manager (Target,TargetList)
      return{
        /**
         * This function can be used to create a target configuration for feedback.
         * It is a wrapper for the Manager.create function and returns a configuration object.
         * createjs objects can also be used as targets, this is often useful when connecting feedback with a specific object.
         * In this case the properties starting with "feedback" and a name can be added to the target.
         * See the properties for additional configuration properties.
         * @example
         * // Create manual position at top center independant of specific objects
         * var TopScreen = MTLG.utils.fbm.createTarget("TopScreen");
         * TopScreen.y=0.06;
         * TopScreen.x=0.5;
         * @property {number} x [0,1] x-Position of target relative to width
         * @property {number} y [0,1] y-Position of target relative to height
         * @property {number} rotation Rotation of feedback in degrees
         * @property {number} reqPointerRotation Rotation of pointer (hand) for minimizable feedback, automatically calculated when object is given for dynamic calculation.
         * @property {number} feedbackX [-1,1] For createjs objects: x-Position of target relative to object x-Position
         * @property {number} feedbackY [-1,1] For createjs objects: y-Position of target relative to object y-Position
         * @property {number} feedbackRotation For createjs objects: Rotation of target relative to object rotation
         * @property {number} name For createjs objects: Can be used to identify object when using Learning Analytics
         * @return {object} The content configuration object corresponding to the specified key.
         * @function createTarget
         * @memberof MTLG.utils.fbm#
         */
        create: TManager.create,
        get: TManager.get,
        getCopy: TManager.getCopy,
        set: TManager.set,
        del: TManager.del,
        save: TManager.save,
        load: TManager.load,
        clear: TManager.clear,
      }
    })();

  //Erstellt alle Elemtente des Feedbacks und gibt FeedbackObjekt zurück
  var Creator = (function (){
      //Schnittstelle innerhalb des Moduls zum Erstellen von Feedback
      function createFeedback(target, SettingObject){
        //berechnet absolute Größe des zu erstellenden Feedbacks
        var renderwidth = options.width*SettingObject.totalWidth;
        var renderheight = options.height*SettingObject.totalHeight;

        //erzeugt einen zentrierten, passend rotierten Container für alle Inhalte
        var FBObject = new createjs.Container();
        FBObject.regX = 0.5*renderwidth;
        FBObject.regY = 0.5*renderheight;
        FBObject.rotation = target.rotation;

        /*Fallunterscheidungen dazu, welche Inhalte den Einstellugnen entsprechend erzeugt werden müssen
          Für eine leichtere modulare Anpassungen funktionieren alle hier verwendeten Hilfsfunktionen unabhängig von dem Settingobject,
          sondern mit der Übergabe der einzelnen Einstellungen. Dies führt zwar zu teilweise langen Funktionsaufrufen, lässt aber leicht erkennen,
          welche Einstellungen in welcher Funktion verwendet werden.*/

        if ((typeof(SettingObject.text) == "string")||(SettingObject.img != null)) {
          //Feedback enthält Bild oder Text, Window wird erzeugt
          var windowobj = createWindow(SettingObject.windowtype, SettingObject.theme, renderwidth, renderheight, SettingObject.color, SettingObject.curve);
          FBObject.addChild(windowobj);
          FBObject.setBounds(0,0,renderwidth,renderheight);

          if (SettingObject.closeable){
            //Close-Button benötigt
            var closeobj = createButton("Close",SettingObject.windowtype, SettingObject.theme, renderwidth, renderheight, SettingObject.buttonColor, SettingObject.curve);
            FBObject.addChild(closeobj);
          }

          if ((SettingObject.requestable)||(SettingObject.minimizable)){
            //Request-Button benötigt
            var requestobj = createButton("Request",SettingObject.windowtype, SettingObject.theme, renderwidth, renderheight, SettingObject.buttonColor, SettingObject.curve, null, SettingObject.reqPointer, target.reqPointerRotation);
            FBObject.addChild(requestobj);

            if (SettingObject.minimizable){
              //Minimize-Button benötigt
              var minimizeobj = createButton("Minimize",SettingObject.windowtype, SettingObject.theme, renderwidth, renderheight, SettingObject.buttonColor, SettingObject.curve, SettingObject.closeable);
              FBObject.addChild(minimizeobj);
            }
          }

          if (typeof(SettingObject.text) == "string"){
            //Feedback enthält Text, textobj wird erzeugt und positioniert im Window
            var textobj = createText(SettingObject.text, SettingObject.textWidth*renderwidth, SettingObject.textHeight*renderheight, SettingObject.fontType, SettingObject.fontSize, SettingObject.fontColor, SettingObject.textAlign, SettingObject.curve);
            textobj.x=renderwidth*((1-SettingObject.textWidth)*(SettingObject.textX));
            textobj.y=renderheight*((1-SettingObject.textHeight)*(SettingObject.textY));
            FBObject.addChild(textobj);
          }

          if (SettingObject.img != null){
            //Feedback enthält Bild, imageobj wird erzeugt
            var imgobj = createImage(SettingObject.img, SettingObject.imgWidth*renderwidth, SettingObject.imgHeight*renderheight, SettingObject.imgRatioLocked);
            //Falls Ratiolocked, entspricht Bildgröße nicht den Settingswerten, sie werden deswegen dem skalierten Bild entsprechend angepasst
            var bounds = imgobj.getBounds();
            var imgWidth = bounds.width/renderwidth;
            var imgHeight = bounds.height/renderheight;

            //Positionierung des Bildes im Window
            imgobj.x=(renderwidth)*(1-imgWidth)*SettingObject.imgX;
            imgobj.y=(renderheight)*(1-imgHeight)*SettingObject.imgY;
            FBObject.addChild(imgobj);
          }
        }

        if (SettingObject.sound != null) {
          //Feedback enthält Sound, soundobj wird erzeugt
          var soundobj = createSound(SettingObject.sound, SettingObject.soundProperties, SettingObject.soundLoop);
          FBObject.sound=soundobj;
        }

        if (SettingObject.flair != null){
          //Feedback enthält Flair auf einem Object, flair wird erzeugt
          if(target.object!='undefined'){
            var flairobj = createFlair(target.object, SettingObject.flair, SettingObject.flairSize, SettingObject.flairTime, SettingObject.flairIntensity);
            FBObject.flair=flairobj;
          }
        }

        if(SettingObject.pointCounter != null){
          //Feedback enthält Pointer(Hand), handobj wird erzeugt
          var handobj = createPointer(target,SettingObject.pointSpeed, SettingObject.pointRotation, SettingObject.pointSize, SettingObject.pointCounter, SettingObject.pointDelay, SettingObject.pointStartX, SettingObject.pointStartY, SettingObject.pointEndX, SettingObject.pointEndY);
          FBObject.hand=handobj;
        }

        if((SettingObject.focus != null)&&((SettingObject.flair != null)||(typeof(SettingObject.text) == "string")||(SettingObject.img != null))){
          //Feedback enthält Flair, FeedbackObjekt mit Text oder Bild, die fokussiert werden sollen, focusobj wird erzeugt
          var focusobj = createFocus(SettingObject.focus);
          FBObject.focus=focusobj;
        }

        //manuell bestimmter Offset vom übergebenen Objekt
        var offsetX = target.offsetX;
        var offsetY = target.offsetY;

        //Offset wird in abhängigkeit von der Rotation berechnet
        if((offsetX!=null)||(offsetY!=null)){
            //Schmerzhafte manuelle Berechnung (oder Mathe war eventuell doch sinnvoll fürs Leben, wie man es halt sehen mag)
            var radianRotation = target.rotation * Math.PI / 180; //Grad -> Radian
            var cos = Math.cos;
            var sin = Math.sin;
            FBObject.x=offsetX * cos(radianRotation) - offsetY * sin(radianRotation);
            FBObject.y=offsetX * sin(radianRotation) + offsetY * cos(radianRotation);
        }
        return FBObject;
      }

      //Hilfsfunktionen zum Erstellen der Inhalte

      //Erstellt Textobjekt
      function createText(text, textWidth, textHeight, fontType, fontSize, fontColor, textAlign, curve){
          var textscale = 0.5; //Annahme jeder Char ist 0.5 so breit wie hoch im Durchschnitt(Good guess for arial)

          //Hier berechnung in einer Zeile, zur Übersicht im Beispiel der schrftlichen Arbeit etwas umstrukturiert
          var fontSizeCalc=0.95*Math.sqrt((textWidth*0.95*textHeight)/(text.length*textscale));

          if(fontSizeCalc<fontSize){
            //Falls definierte Schriftgröße zu groß, nehme berechnete
            fontSize=fontSizeCalc
          }else{
            //Falls keine Schriftgröße definiert, nehme berechnte
            if(fontSize==null){fontSize=fontSizeCalc;}
          }

          //Erzeugen des entsprechenden Textobjekt
          var textobj = MTLG.utils.gfx.getText(text, fontSize+"px "+fontType, fontColor);
          textobj.lineWidth = (1-curve/8)*textWidth;
          textobj.textAlign = textAlign;
          textobj.textBaseline = "middle";

          //Überprüfung ob Grenzen eingehalten werden
          while ((textobj.getBounds().width>(1-curve/8)*textWidth)||(textobj.getBounds().height>0.95*textHeight)){
            //Zu viele breite Character, angenommene Schriftgröße reduzieren
            fontSize=fontSize-(0.1*fontSize);
            textobj.font=fontSize+"px "+fontType;
          }

          //Setzt Regestrierungspunkte im Verhältnis zum Text gleich abhängig von Formatierungen um diese einheitlich nutzen zu können.
          switch (textAlign){
            case "left":  textobj.regX= -(textWidth-textobj.getBounds().width)/2; break;
            case "center": textobj.regX= -textWidth/2; break;
            case "right": textobj.regX= -(textWidth+textobj.getBounds().width)/2; break;
            default:  textobj.regX= -(textWidth-textobj.getBounds().width)/2;
          }
          textobj.regY= -((textHeight-textobj.getBounds().height)/2+fontSize/2);
          textobj.name = "Text";
          return textobj;
        }

      //Erstellt Bitmap
      function createImage(img, imgWidth, imgHeight, ratiolocked){
          var imgobj = MTLG.assets.getBitmap(img);
          var bounds = imgobj.getBounds();

          //Falls Verhältnis erhalten bleiben soll, muss überprüft werden, welche maximale Skalierung möglich ist im definierten Bereich
          if (ratiolocked){
            //Orietierung am definierten Bereich
            if(imgHeight>imgWidth){
              //Breite ist kurze Seite, daran orentieren
              imgobj.scaleX = imgobj.scaleY =  imgWidth/bounds.width;
              imgobj.setBounds(0,0,imgWidth,bounds.height*imgWidth/bounds.width);
            }else{
              //Höhe ist kurze Seite, daran orentieren
              imgobj.scaleX = imgobj.scaleY =  imgHeight/bounds.height;
              imgobj.setBounds(0,0,bounds.width*imgHeight/bounds.height,imgHeight);
            }
          }else{
            //Verhältnis muss nicht berücksichtigt werden -> maximale Skalierung
            imgobj.scaleX = imgWidth/bounds.width;
            imgobj.scaleY = imgHeight/bounds.height;
            imgobj.setBounds(0,0,imgWidth,imgHeight);
          }

          imgobj.name = "Image";
          return imgobj;
        }

      //Erstellt function zum abspielen eines Sounds
      function createSound(sound, prop, soundLoop){
        switch(sound){
          case "wrong":
          case "negative":
          case "negativ":
            sound="negSound";
            break;
          case "right":
          case "positive":
          case "positiv":
            sound="posSound";
            break;
          case "time":
          case "timer":
          case "counter":
          case "clock":
            sound="tickSound"
            break;
        }
        var playsound = function(){
          //soundLoop + Volume noch nicht unterstützt für nicht BackgroundMusic vom AssetManager
          MTLG.assets.playSound("fbm/"+sound, prop); //aktuell nicht Funktional in Chrome
        }
          return playsound;
        }

      //Erstellt Container, welcher das Window repräsentiert
      function createWindow(windowtype, theme, renderwidth, renderheight, color,  curve){
          //Benötigten Werte, wie Eckengröße oder Kantenbreite werden berechnet
          if (renderwidth<renderheight){
            var cornersize = 0.5*curve*renderwidth;
            var edgewidth = renderwidth*0.5;
          }else{
            var cornersize = 0.5*curve*renderheight;
            var edgewidth = renderheight*0.5;
          }

          //Benötigte Objekte werden erzeugt
          var windowobj = new createjs.Container();
          var borderobj = new createjs.Container();
          var backgroundobj = new createjs.Container();

          //Arrays zum Zwischenspeichern der erstellten Ecken, Kanten bzw. Hintergründe
          var Circles = [];
          var Corners = [];
          var Rectangles = [];
          var Edges = [];

          //Iterative Erstellung der Objekte
          for (i=0; i<4; i++){
            //4 Iterationen für jeweils eine Seite, also Ecke + Kante

            //Setzen der Werte abhängig davon, welche Seite berechnet wird
            switch (i){
              case 0:
                cornerXpos=0;
                cornerYpos=0;
                edgeXpos=cornersize;
                edgeYpos=0;
                edgeXscale=(renderwidth-2*cornersize)/500;
                circleXpos = cornersize;
                circleYpos = cornersize;
                Rectangles[i]=createEdgeBackground(0,cornersize,renderwidth,renderheight-2*cornersize);
                break;
              case 1:
                cornerXpos=renderwidth;
                cornerYpos=0;
                edgeXpos=renderwidth;
                edgeYpos=cornersize;
                edgeXscale=(renderheight-2*cornersize)/500;
                circleXpos = renderwidth-cornersize;
                circleYpos = cornersize;
                Rectangles[i]=createEdgeBackground(cornersize,0,renderwidth-2*cornersize,renderheight);
                break;
              case 2:
                cornerXpos=renderwidth;
                cornerYpos=renderheight;
                edgeXpos=renderwidth-cornersize;
                edgeYpos=renderheight;
                edgeXscale=(renderwidth-2*cornersize)/500;
                circleXpos = renderwidth-cornersize;
                circleYpos = renderheight-cornersize;
                break;
              case 3:
                cornerXpos=0;
                cornerYpos=renderheight;
                edgeXpos=0;
                edgeYpos=renderheight-cornersize;
                edgeXscale=(renderheight-2*cornersize)/500;
                circleXpos = cornersize;
                circleYpos = renderheight-cornersize;
                break;
            }
            //Erstellen und abspeichern der Objekte entsprechend der berechneten Werte
            Corners[i] = createCorner(cornerXpos,cornerYpos, 90*i, cornersize/500);
            Edges[i] = createEdge(edgeXpos, edgeYpos, 90*i, edgeXscale, cornersize/500);
            Circles[i] = createCornerBackground(circleXpos,circleYpos,cornersize);

            //Hinzufügen zu jeweiligen Containern
            borderobj.addChild(Corners[i],Edges[i]);
            backgroundobj.addChild(Circles[i]);
            if(i<2){backgroundobj.addChild(Rectangles[i])}
          }

          //Zusammenfügen zu Window
          windowobj.addChild(backgroundobj,borderobj);
          windowobj.name = "Window";
          return windowobj;

          //Hilfsfunktionen

          //Erstellt Ecke
          function createCorner(x,y,rotation,scale){
            var corner = MTLG.assets.getBitmap("img/fbm/"+windowtype+"Corner"+theme+".png"); //Searchword ImgageLoad
            corner.rotation = rotation;
            corner.scaleX = scale;
            corner.scaleY = scale;
            corner.x=x
            corner.y=y;
            return corner;
          }

          //Erstellt Kante
          function createEdge(x,y,rotation,scaleX,scaleY){
            if (scaleY == 0){scaleY = edgewidth/500;} //Falls es keine Rundungen gibt, ist skalierung der Kanten unabhängig von curve
            var edge =  MTLG.assets.getBitmap("img/fbm/"+windowtype+"Edge"+theme+".png"); //Searchword ImgageLoad
            edge.rotation = rotation;
            edge.scaleX = scaleX;
            edge.scaleY = scaleY;
            edge.x=x
            edge.y=y;
            return edge;
          }

          //Erstellt Hintergrund der Ecke
          function createCornerBackground(x,y,r){
            var circle = MTLG.utils.gfx.getShape();
            circle.graphics.beginFill(color).drawCircle(x, y, 0.99*r);
            return circle;
          }
          //Erstellt Hintergrund der Kante
          function createEdgeBackground(x,y,width,height){
            var rectangle = MTLG.utils.gfx.getShape();
            rectangle.graphics.beginFill(color).drawRect(x, y,width,height);
            return rectangle;
          }

        }

      //Erstellt und positioniert den jeweiligen Button abhängig vom Typen
      function createButton(buttontype, windowtype, theme, renderwidth, renderheight, buttonColor, curve, closebuttonSlotTaken, reqPointer, reqPointerRotation){
          //Erstellung der benötigten Objekte
          var buttoncontainer = new createjs.Container();
          var button = MTLG.assets.getBitmap("img/fbm/"+windowtype+buttontype+theme+".png"); //Searchword ImgageLoad
          var background = MTLG.utils.gfx.getShape();

          //Erstellung des ReqPointer, falls gebraucht
          if((reqPointer)&&(reqPointerRotation!=null)){
            //Überprüfung ob Rotation gesetzt wurde, um Fehler abzufangen in der automatischen berechnung der Rotation
            var reqContainer  = new createjs.Container();
            var reqPointer = MTLG.assets.getBitmap("img/fbm/"+windowtype+"RequestPointer"+theme+".png");
            reqContainer.addChild(reqPointer);
          }else{reqPointer=false}

          //Berechnung der benötigen Parameter
          var scale = getRelativeScale(0.1, 500); //Skaliert die Zielgröße auf 10% der kürzeren Bildschirmseite
          var qlength = Math.sqrt(renderwidth*renderheight); //Seitenlänge des Quadrates mit Fläche entsprechend der Fläche vom Feedback
          var adjust = ((0.3*qlength+150)/500); //Verhältnis Größe Button zu Größe Feedback, 30% relativ zur Flächefeedback, mindestens 150 pixel
          if(buttontype == "Request"){adjust = 1.7* adjust}//Request-Button muss größer sein
          scale = scale * adjust; //Nachjustierung

          //Skalierung und Positionierung der Objekte anhand Parameter
          button.scaleX = button.scaleY = scale;
          button.x=-scale*500/2;
          button.y=-scale*500/2;
          background.graphics.beginFill(buttonColor).drawCircle(0, 0, 0.75*scale*500/2);

          //Getrennte Erstellung und Positionierung von reqPinter zur Übersichtlichkeit
          if(reqPointer){
            reqPointer.scaleX = reqPointer.scaleY = scale;
            reqPointer.regX = 800/2;
            reqPointer.regY = 685/2;
            reqContainer.rotation =(180+reqPointerRotation) % 360;
          }

          //Positionierung vordefinierter Buttons
          switch(buttontype){
            case "Close":
              buttoncontainer.x = (1-0.5*curve/8)*renderwidth-scale*400/2; //renderwidth/2+scale*400/2+3;//
              buttoncontainer.y = -scale*400/2-3;
              break;
            case "Minimize":
              buttoncontainer.x = (1-0.5*curve/8)*renderwidth-scale*400/2;
              if(closebuttonSlotTaken){buttoncontainer.x -= scale*450; }
                //renderwidth/2-scale*400/2-3;//(1-curve/8)*renderwidth-scale*400/2-scale*500; //(1-curve/8)*renderwidth-scale*400/2;

              buttoncontainer.y = -scale*400/2-3;
              break;
            case "Request":
              buttoncontainer.x = renderwidth/2;
              buttoncontainer.y = renderheight/2;
              break;
            default:
              console.log("Error: Button "+buttontype+" is unknown");
          }

          //Zusammenfügen, referenzierbar machen und zurückgeben
          buttoncontainer.name=buttontype;
          if(reqPointer){buttoncontainer.addChild(reqContainer);}
          buttoncontainer.addChild(background,button);

          return buttoncontainer;
        }

      //Erstellt Flair
      function createFlair(target,flair,flairSize,flairTime,flairIntensity){
        var flairobj = MTLG.assets.getBitmap("img/fbm/"+flair+"flair.png"); //Searchword ImgageLoad
        var Bounds = target.getBounds();

        if (Bounds != null){
          //Falls Größe von Objekt ermittelt werden konnte, wird Flair entsprechend erstellt
          flairobj.scaleX= (Bounds.width+flairSize)/500; //Flair ist groß wie Objekt+ definierte Größe um Objekt herum
          flairobj.scaleY= (Bounds.height+flairSize)/500;
          flairobj.x=-0.5*flairSize+Bounds.x;
          flairobj.y=-0.5*flairSize+Bounds.y;
          flairobj.alpha=flairIntensity;
          flairobj.time=flairTime;
          return flairobj;
        }else{
          console.log("Error: Can´t calculate size of target to add flair, please use setBounds()")
        }
      }

      //Erstellt Pointer (Hand)
      function createPointer(target, speed, rotation, size, counter, delay, xStart, yStart, xEnd, yEnd){
        //berechnet aus relativen Koordinaten die absoluten Werte, falls keine gesetzt wird dynamische Position von TargetObjekt verwendet
        if(xStart==null){xStart=target.x};
        if(yStart==null){yStart=target.y};
        xStart=xStart*options.width;
        yStart=yStart*options.height;

        if(xEnd==null){xEnd=target.x};
        if(yEnd==null){yEnd=target.y};
        xEnd=xEnd*options.width;
        yEnd=yEnd*options.height;

        //berechnet Animationszeit abhängig von Stercke und Geschwindigkeit für eine konstante Animationsgeschwindigkeit
        var abstandX2 = Math.pow(xStart-xEnd, 2);
        var abstandY2 = Math.pow(yStart-yEnd, 2);
        var distance = Math.sqrt(abstandX2+abstandY2);
        var time = 1000*distance/speed;

        //Erzeugt Hand-Objekt und setzt alle benötigten Werte
        var hand = MTLG.assets.getBitmap("img/fbm/hand.png"); //Searchword ImgageLoad
        hand.counter=counter;
        hand.delay = delay; // how much time between each counter cycle
        hand.scaleX = hand.scaleY = size/200;
        hand.regX = 53; // Um den Finger genau im Fokus zu haben
        hand.regY = 0;
        hand.rotation=rotation;
        hand.time=time;
        hand.startX=xStart;
        hand.startY=yStart;
        hand.endX=xEnd;
        hand.endY=yEnd;
        return hand;
      }

      //Erstellt Focus
      function createFocus(focus){
        //Lädt und skaliert Bild auf ganzen Bildschirm
        var focusobj = MTLG.assets.getBitmap("img/fbm/focus.png");
        focusobj.scaleX = options.width/1920;
        focusobj.scaleY = options.height/1080;
        focusobj.alpha=focus;
        return focusobj;
      }

      //Berechnet relativen Skalierungsfaktor um ein Wert imgsize auf ratio% der kürzeren Bildschirmseite zu skalieren
      //Wird für optisch und haptisch angenehme Buttongröße benötigt
      function getRelativeScale(ratio, imgsize){
          if (options.height<options.width){
            var shortside = options.height;
          }else{
            var shortside = options.width;
          }
          var scale=ratio*shortside/imgsize;
          return scale;
        }

      //Rückgabe des Interfaces vom Creator
      return{
        createFeedback: createFeedback,
      }
    })();

  //Viewer - Zeigt FeedbackObjekt an
  var Viewer = (function (){
      var activeFeedback = {}; //Liste zur Verwaltung der dargestellten Feedbacks
      var fbIDcounter = 0; //Damit jedem Feedback eine eindeutige ID zugewiesen wird
      var stage; //Damit Stage nur einmal in zentraler Anzeigefunktion angefordert werden muss

      //Zentrale Funktion zum Anzeigen aller erstellten Feedback-Objekte
      function viewFeedback(FBObject, target, SettingObject){
        stage = MTLG.getStageContainer();
        var delFunctions = {}; //hier werden die Funktionen zum Löschen der einzelnen Objekte gespeichert

        //Aufrufen der jweiligen Funktionen zum Anzeigen der Objekte -
        //Teilweise lange Aufrufe, da nur benötigten Werte des SettingsObjektes übergeben werden um AnzeigefUnktion Modular unabhängig der Umsetzung mittels SettingObject zu halten
        if(typeof(FBObject.focus)!='undefined'){delFunctions["Focus"] = displayFocus(FBObject.focus)}
        if(typeof(FBObject.flair)!='undefined'){delFunctions["Flair"] = displayFlair(FBObject.flair,target,SettingObject.focus, delFunctions["Focus"])}
        if(typeof(FBObject.hand)!='undefined'){delFunctions["Hand"] = displayPointer(FBObject.hand,target)}
        if(typeof(FBObject.sound)!='undefined'){displaySounds(FBObject.sound, SettingObject.soundLoop, SettingObject.volume)}
        if(FBObject.getBounds()!= null){displayWindow(FBObject, delFunctions, target, SettingObject.delay, SettingObject.totalDuration, SettingObject.openDuration, SettingObject.miniDuration, SettingObject.requestable, SettingObject.closeable, SettingObject.minimizable, SettingObject.movable, SettingObject.movableMini, SettingObject.visibility,SettingObject.focus, SettingObject.openCallback, SettingObject.closeCallback, SettingObject.screenLocked, SettingObject.rotateOnStart, SettingObject.rotateOnMove);}
      }

      //Zeigt Feedback-Objekt an
      function displayWindow(FBObject, delFunctions, target, delay, totalDuration, openDuration, miniDuration, requestable, closeable, minimizable, movable, movableMini, visibility, focus, openCallback, closeCallback, screenLocked, rotateOnStart, rotateOnMove){
        //Deklaration
        var myID;
        var stage = MTLG.getStageContainer();
        var bounds = FBObject.getBounds();
        var request, close, minimize;
        var iterator = FBObject.children;
        var miniTimer, openTimer;

        //Positionierung des Objekts abhängig von Auflösung
        FBObject.x += target.x*(options.width);
        FBObject.y += target.y*(options.height);

        //Überprüft, wenn Option von Entiwckler gesetzt wurde, ob Feedback teilweise offscreen angezeigt wird
        if(screenLocked){
          checkFBoffscreen();
        }

        //Rotiert Objekt wenn gewünscht zur Mitte.
        if(rotateOnStart){
          rotation(FBObject);
        }
        var smoothTransation = false; //Wird später für flüßige Rotation gebraucht

        //Fügt Objekt nach Delay zur Stage hinzu und ruft Callback auf
        setTimeout(function(){
          myID = addActiveFB(FBObject);
          stage.addChild(FBObject);
          handleCallback(openCallback);
          delay=0; //Andere wiederholende Timer (open/mini) verwenden Delay um nicht zu früh zu starten, nach Ablauf von delay aber nicht gewünscht
        },delay*1000);

        //Bringt Feedback immer in den Vordergrund bei Interaktion
        FBObject.on("mousedown", function(evt) {
          if ((stage.getChildIndex(FBObject)<stage.getNumChildren()-1)&&(evt.target.parent.name!="Close")&&(evt.target.parent.name!="Minimize")){
            stage.setChildIndex(FBObject, stage.getNumChildren()-1);
          }
        });

        //Feedback kann per Drag and Drop bewegt werden.
        if (movable){
          //Offset des Klicks im Objekt wird berücksichtigt
          var xoffset, yoffset;
          FBObject.on("mousedown", function(evt) {
            xoffset = evt.stageX-FBObject.x;
            yoffset = evt.stageY-FBObject.y;
          });

          //Änderung der Position bei Bewegung
          FBObject.on("pressmove", function(evt) {
            //Bewegung nur auf nicht Buttons, bei Request falls movableMini gewünscht ist
            if((evt.target.parent.name!="Close")&&(evt.target.parent.name!="Minimize")&&(movableMini||evt.target.parent.name!="Request")){ //Falls ein Teil des Close-Buttons gdrückt wird, soll das FBObjekt nicht bewegt werden, damit man vom Close-button runter gehen kann um das schließen abzubrechen.
              FBObject.x = evt.stageX-xoffset;
              FBObject.y = evt.stageY-yoffset;
              //Falls Rotation bei Bewegung angepasst werden soll
              if(rotateOnMove){
                rotation(FBObject);
              }
            }
          });
        }

        //Funktion zum Berechnen der Rotation beim Bewegen (Source: Tabula RegEx)
        function rotation(object) {
          let height = options.height;
              width = options.width,
              ankathete = object.y - height / 2,
              gegenkathete = object.x - width / 2;
          if (object.y > height / 2) {
            var degree = (Math.atan(-gegenkathete / ankathete) * 180 / Math.PI) % 360;
          } else {
            var degree = ((Math.atan(-gegenkathete / ankathete) * 180 / Math.PI) - 180) % 360;
          }


          //Erweiterung der Funktion für fließendere Übergange bei Anfangsrotation
          if((rotateOnStart)||(smoothTransation)){
            //Objekt schon rotiert, kein fließender Übergang nötig
            object.rotation = degree
          }else{
            //Falls richtige Rotation erreicht, muss keine Gewichtete Rotation mehr durchgeführt werden
            if(Math.round(object.rotation)==Math.round(degree)){smoothTransation=true;}
            //Fließender Übergang von aktueller Rotation (neue Rotation geht nur mit Gewichtung 25% ein)
            if (Math.abs(object.rotation-((degree+360) % 360))>180){
              //Abstand zwischen neuer und alter Rotation > 180, weswegen der Übergang von 360 zu 0 bedacht werden muss
              object.rotation = (0.25*((degree) % 360) + 0.75* (object.rotation)) % 360 ;
            }else{
              //(degree+360) mod 360 ist nötig, da wert sonst negativ (thx javascript mod implementation)
              object.rotation = (0.25*((degree+360) % 360) + 0.75* (object.rotation)) % 360 ;
            }
          }
        }

        //Fügt Interaktion zum Minimize-Button hinzu
        if (minimizable){
          minimize = FBObject.getChildByName("Minimize");
          if(typeof(minimize)!=null){minimize.on("click",hideFB)}else{console.log("Error: Missing Minimize-Button");};
        }

        //Fügt Interaktivität zum Close-Button hinzu
        if (closeable){
          close = FBObject.getChildByName("Close");
          if(typeof(close)!=null){close.on("click",deleteFB)}else{console.log("Error: Missing Close-Button");};
        }

        //Fügt Interaktion zum Request-Button hinzu
        if ((requestable)||(minimizable)){
          //referenziert request-button und blendet restliches Feedback aus
          request = FBObject.getChildByName("Request");
          if(requestable){
            hideFB();
          }else{
            request.visible=false;
          }

          //falls referenzieren erfolgreich, fügt Interaktivität hinzu
          if(typeof(request)!=null){
            //Speichert x und y Position des Feedbacks bei Touch-Interaktion mit Request-Button
            var mousedownX, mousedownY;
            request.on("mousedown",function(evt){
              mousedownX = FBObject.x;
              mousedownY = FBObject.y;
            });

            //Überprüft bei Click auf Request-Button, ob dieser (mehr als 1% der Bildschirmgröße) bewegt wurde und verhindert dann die interpretierung als Click
            request.on("click",function(evt){
              if ((Math.abs(mousedownX-FBObject.x)<0.01*options.width)&&(Math.abs(mousedownY-FBObject.y)<0.01*options.height)){
                showFB(); //falls keine (oder nur minimale) Bewegung, interpretiere als Click und zeige Feedback
              }
            });
          }else{
            console.log("Error: Missing Request-Button");
          };
        }

        // Löscht Feedback nach totalDuration;
        if (totalDuration!=0){
          setTimeout(deleteFB,(totalDuration+delay)*1000);
        }

        //Hilfsfunktionen

        //Behandelt Callbacks
        function handleCallback(callback){
          //Falls Callback nach Laden hinzugefügt wurde, wird es aufgerufen
          if(typeof(callback)=="function"){callback(FBObject);}
          //Falls Callback beim speichern entfernt wurde und nach Laden nicht neugesetzt, wird dies dem Entwickler hier signalisiert
          if(typeof(callback)=="string"){console.log("Warning: Callback wurde beim Abspeichern entfernt");}
        }

        //Löscht neben Fenster alle anderen zugehörigen Objekte
        function deleteFB(){
          FBObject.deleteFB();
          for(key in delFunctions){
            delFunctions[key].deleteFB();
          }
        }

        //Entfernt FeedbackFenster von der Stage und alle Listener und ruft Callback auf
        FBObject.deleteFB = function (){
          delete activeFeedback[myID];
          FBObject.removeAllEventListeners();
          if (stage.contains(FBObject)){
            handleCallback(closeCallback);
            stage.removeChild(FBObject);
          }
        }

        //Versteckt Feedback und zeigt Request-Button
        function hideFB(){
          //Iteration über alle Objekte um diese auszublenden
          var i=0;
          while (i<iterator.length){ //Warum eine while statt einer for-Schleife fragt man sich? Gute Frage, for-schleife und childrenarrays => 1 eintrag mehr wird iteriert als im array ?!!?!?
              if(iterator[i].name!="Request"){
                iterator[i].visible=false;
              }else{
                iterator[i].visible=true;
              }
            i++;
          }

          //Falls das Feedback begrenzte Zeit minimiert bleiben soll, wird Timer gesetzt
          if (miniDuration!=0){
            miniTimer = setTimeout(deleteFB,(miniDuration+delay)*1000);
          }

          //Timer des geöffneten Feedbacks wird abgebrochen, falls gesetzt
          if (openDuration!=0){
            if(typeof(openTimer)!='undefined'){clearTimeout(openTimer)};
          }
        }

        //Zeigt Feedback und versteck Request-Button
        function showFB(){
          //Iteration über alle Objekte um diese einzublenden
          var i=0;
          while (i<iterator.length){
            //if(typeof(iterator[i])!='undefined'){
              if(iterator[i].name!="Request"){
                iterator[i].visible=true;
              }else{
                iterator[i].visible=false;
              }
            i++;
          }

          //Falls das Feedback begrenzte Zeit geöffnet bleiben soll, wird Timer gesetzt
          if (openDuration!=0){
            openTimer = setTimeout(deleteFB,(openDuration)*1000);
          }

          //Timer des minimierten Feedbacks wird abgebrochen falls gesetzt
          if (miniDuration!=0){
            if(typeof(miniTimer)!='undefined'){clearTimeout(miniTimer)};
          }
        }

        //Überprüft ob Teile des Feedbacks Offscreen dargestellt werden und setzt es dann an die nächste onscreen positon
        function checkFBoffscreen(){
          if(FBObject.x-FBObject.regX<0){FBObject.x=FBObject.regX-bounds.x}
          if(FBObject.x-FBObject.regX>options.width-bounds.width){FBObject.x=options.width-bounds.width-bounds.x+FBObject.regX}
          if(FBObject.y-FBObject.regY<bounds.height/2){FBObject.y=FBObject.regY-bounds.y}
          if(FBObject.y-FBObject.regY>options.height-bounds.height){FBObject.y=options.height-bounds.height+FBObject.regY-bounds.y}
        }
      }

      //Zeigt dunkle Fläche über Spiel um Feedback zu fokussieren
      function displayFocus(focusobj){
        var myID;
        var stage = MTLG.getStageContainer();
        stage.addChild(focusobj);
        myID = addActiveFB(focusobj);
        //Funktion zum Entfernen
        focusobj.deleteFB = function(){
          delete activeFeedback[myID];
          if (stage.contains(focusobj)){stage.removeChild(focusobj)};
        }
        return focusobj;
      }

      //Spielt Sound vom AssetManager ab
      function displaySounds(playSound, soundLoop, volume){
        //soundLoop + Volume noch nicht unterstützt für nicht BackgroundMusic vom AssetManager
        playSound();
      }

      //Zeigt und animiert Hand
      function displayPointer(hand, target){
          var myID;
          stage.addChild(hand);
          myID = addActiveFB(hand);
          moveHand();

          //Animation der Hand
          function moveHand(){
            if(hand.counter>0){
              //Startposition
              hand.x = hand.startX;
              hand.y = hand.startY;
              hand.visible = true;

              //Bewegung zur Endposition
              createjs.Tween.get(hand, {loop: false}).to({x: hand.endX}, hand.time).call(function(){hand.visible=false;});
              createjs.Tween.get(hand, {loop: false}).to({y: hand.endY}, hand.time).call(function(){
                hand.counter--;
                hand.visible=false;
                setTimeout(moveHand,1 + hand.delay); //Da unsichtbarkeit länger dauern kann, als positionsveränderung (hand zuckt sonst rum)
              });
            }else{
              hand.deleteFB();
            }
          }

          //Funktion zum Entfernen der Hand
          hand.deleteFB = function(){
            delete activeFeedback[myID];
            hand.removeAllEventListeners();
            if (stage.contains(hand)){
              stage.removeChild(hand);
            }
          }
          return hand;
      }

      //Fügt Flair zum Zielobjekt hinzu
      function displayFlair(flairobj, target, focus, delFocus){
        myID = addActiveFB(flairobj);
        if(typeof(target.object.addChild)=='function'){
          //Zielobjekt können Elemente hinzugefügt werden
          target.object.addChild(flairobj);
          target.object.setChildIndex(flairobj, 0); //Wird unter alle anderen Objekte im Ziel gelegt
          if(focus>0){stage.setChildIndex(target.object, stage.getNumChildren()-1);} //Falls Focus gesetzt, muss Target des FLairs in den Vordergrund geholt werden
          if(flairobj.time>0){ //Falls Flairzeit eingeschränkt ist
            setTimeout(function(){
              if(typeof(delFocus)=='function'){delFocus();}
              flairobj.deleteFB();
            },flairobj.time*1000);
          }
        }else{
          console.log("Error: Übergebenem Target kann kein Objekt hinzugefügt werden. Verwende Container zum Anzeigen von Flairs.")
        }

        //Funktion zum Löschen des Flairs
        flairobj.deleteFB = function(){
          delete activeFeedback[myID];
          if (target.object.contains(flairobj)){target.object.removeChild(flairobj);};

        }
        return flairobj;

      }

      //Fügt übergebenes FB zur Liste der aktiven FBs hinzu und vergibt eindeutige IDs
      function addActiveFB(FBObject){
        fbIDcounter++;
        activeFeedback[fbIDcounter]=FBObject;
        return fbIDcounter;
      }

      /**
       * Removes all active feedback objects
       * @function removeAllFeedback
       * @memberof MTLG.utils.fbm#
       */
      function removeActiveFB(){
        var stage = MTLG.getStageContainer();

        //Iteration über alle aktiven FB-Objekte
        for (var fb in activeFeedback){
          activeFeedback[fb].deleteFB();
        }

      }

      //Gibt Interface vom Viewer zurück
      return{
        viewFeedback: viewFeedback,
        removeAllFeedback: removeActiveFB,
      }
    })();

    //Interface des Moduls
    return{
      giveFeedback: Controller.giveFeedback,
      getConfig: Controller.getConfig,
      setConfig: Controller.setConfig,
      resetConfig: Controller.resetConfig,
      removeAllFeedback: Viewer.removeAllFeedback,
      createContent: ContentManager.create,
      getContent: ContentManager.get,
      getCopyContent: ContentManager.getCopy,
      setContent: ContentManager.set,
      delContent: ContentManager.del,
      createTiming: TimingManager.create,
      /**
       * @function getTiming
       * @memberof MTLG.utils.fbm#
       */
      getTiming: TimingManager.get,
      /**
       * @function getCopyTiming
       * @memberof MTLG.utils.fbm#
       */
      getCopyTiming: TimingManager.getCopy,
      /**
       * @function setTiming
       * @memberof MTLG.utils.fbm#
       */
      setTiming: TimingManager.set,
      /**
       * @function delTiming
       * @memberof MTLG.utils.fbm#
       */
      delTiming: TimingManager.del,
      createStyle: StyleManager.create,
      /**
       * @function getStyle
       * @memberof MTLG.utils.fbm#
       */
      getStyle: StyleManager.get,
      /**
       * @function getCopyStyle
       * @memberof MTLG.utils.fbm#
       */
      getCopyStyle: StyleManager.getCopy,
      /**
       * @function setStyle
       * @memberof MTLG.utils.fbm#
       */
      setStyle: StyleManager.set,
      /**
       * @function delStyle
       * @memberof MTLG.utils.fbm#
       */
      delStyle: StyleManager.del,
      createTarget: TargetManager.create,
      /**
       * @function getTarget
       * @memberof MTLG.utils.fbm#
       */
      getTarget: TargetManager.get,
      /**
       * @function getCopyTarget
       * @memberof MTLG.utils.fbm#
       */
      getCopyTarget: TargetManager.getCopy,
      /**
       * @function setTarget
       * @memberof MTLG.utils.fbm#
       */
      setTarget: TargetManager.set,
      /**
       * @function delTarget
       * @memberof MTLG.utils.fbm#
       */
      delTarget: TargetManager.del,
      clear: allManager.clear,
      save: allManager.save,
      load: allManager.load,
      init: init,
  }

})();
module.exports = FBModule;
