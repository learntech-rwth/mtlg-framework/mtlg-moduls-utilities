/**
 * This module provides graphics effects that are applied on DisplayObjects, a createjs.Text and a createjs.Shape wrapper to be used with StageGL and a means to do stage transitions.
 * <br><br>
 * The graphics effects are all called the same with <code>MTLG.utils.gfx.effectName(displayobject, options_object, label_string)</code>.
 * The label can be used to selectively remove effects (see [reset]{@link MTLG.utils.gfx#reset}).
 *
 * <br><br>
 * To use Text and Shape with StageGL, their content must be cached (see [easeljs/StageGL]{@link https://www.createjs.com/docs/easeljs/classes/StageGL.html}). Autocached memberof MTLG.utils.gfxs that work with StageGL can be obtained with [MTLG.utils.gfx.getText()]{@link MTLG.utils.gfx#getText} and [MTLG.utils.gfx.getShape()]{@link MTLG.utils.gfx#getShape}.
 * <br><br>
 * For scene transitions either specify <code>gameState.transition</code> or set a strategy with <code>MTLG.lc.setTransitioner()</code> or use the default transition.
 * <br><br>
 * Colors in this module are RGB colors in one of the forms <code>#rgb</code>, <code>#rgba</code>, <code>#rrggbb</code>, <code>#rrggbbaa</code> or <code>[r, g, b, a]</code>.
 *
 * @namespace gfx
 * @memberof MTLG.utils
 **/

/**
 * Substitutes <code>new createjs.Text()</code> to enable seamless usage with StageGL.
 * See {@link https://www.createjs.com/docs/easeljs/classes/Text.html} for more information.
 * @example
 * var text = MTLG.utils.gfx.getText("Hello World", "20px Arial", "#ff7700");
 * text.x = 100;
 * text.textBaseline = "alphabetic";
 * MTLG.getStageContainer().addChild(text);
 * @param {string} text (optional) The text to display.
 * @param {string} font (optional) The font style to use. Any valid value for the CSS font attribute is acceptable (ex. "bold 36px Arial").
 * @param {string} color (optional) The color to draw the text in. Any valid value for the CSS color attribute is acceptable (ex. "#F00", "red", or "#FF0000").
 * @memberof MTLG.utils.gfx#
 **/
var getText = (text, font, color) => {
  let _text = new createjs.Text(text, font, color);
  if(MTLG.getOptions().webgl) {
    let recache = () => {
      // reset bounds and uncache to recalculate bounds
      _text.setBounds(null);
      _text.uncache();
      let {x,y,width,height} = _text.getBounds() || {x:0,y:0,width:0,height:0};
      // add tolerance
      x -= 20; y -= 20; width += 40; height += 40;
      _text.cache(x,y,width,height);
      _text.setBounds(x,y,width,height);
    }
    _text._set = _text.set;
    _text.set = (...param) => {
      _text._set(...param);
      recache();
      return _text;
    }
    _text._text = text;
    Object.defineProperty(_text, 'text', {
      get: function() { return _text._text },
      set: function(v) { _text._text = v; recache(); }
    });
    recache();
  }
  return _text;
};

/**
 * Substitutes <code>new createjs.Shape()</code> to enable seamless usage with StageGL.
 * See {@link https://www.createjs.com/docs/easeljs/classes/Shape.html} for more information.
 * @example
 * var shape = MTLG.utils.gfx.getShape();
 * shape.graphics.beginFill("red").drawCircle(20, 20, 100);
 * MTLG.getStageContainer().addChild(shape);
 * @memberof MTLG.utils.gfx#
 **/
var getShape = () => {
  let _shape = new createjs.Shape();
  // track bounds and update when drawing via graphics
  let bounds = {x:0,y:0,width:0,height:0};
  // track state of moveTo, lineTo
  let cursor = {x:0,y:0}
  let g = _shape.graphics;
  let extend = (x,y,width,height) => {
    if(g._strokeStyle) {
      let sw = g._strokeStyle.width;
      x -= .5*sw; y -= .5*sw; width += sw; height += sw;
    }
    if(bounds.width * bounds.height != 0) {
      bounds.x = Math.min(bounds.x, x);
      bounds.y = Math.min(bounds.y, y);
      bounds.width = Math.max(bounds.width, x-bounds.x+width);
      bounds.height = Math.max(bounds.height, y-bounds.y+height);
    } else {
      bounds.x = x; bounds.y = y; bounds.width = width; bounds.height = height;
    }
  }
  g._drawRoundRect = g.drawRoundRect;
  g.rr = g.drawRoundRect = (x, y, w, h, radius) => {
    extend(x, y, w, h);
    return g._drawRoundRect(x, y, w, h, radius);
  }
  g._rect = g.rect;
  g.r = g.rect = g.dr = g.drawRect = (x, y, w, h) => {
    extend(x, y, w, h);
    return g._rect(x, y, w, h);
  }
  g._drawCircle = g.drawCircle;
  g.dc = g.drawCircle = (x, y, radius) => {
    extend(x-radius, y-radius, 2*radius, 2*radius);
    return g._drawCircle(x, y, radius);
  }
  g._clear = g.clear;
  g.clear = () => {
    // assume that something new will be drawn in the next process ticks, so trigger an update afterwards
    setTimeout(()=>_shape.cache(bounds.x, bounds.y, bounds.width, bounds.height))

    Object.assign(bounds, {x:0,y:0,width:0,height:0});
    return g._clear();
  }
  g._moveTo = g.moveTo;
  g.mt = g.moveTo = (x, y) => {
    Object.assign(cursor, {x, y});
    return g._moveTo(x, y);
  }
  g._lineTo = g.lineTo;
  g.lt = g.lineTo = (x, y) => {
    // add some tolerance so that lines are not swallowed in bounds
    extend(Math.min(x,cursor.x), Math.min(y,cursor.y), Math.abs(x-cursor.x)+1, Math.abs(y-cursor.y)+1);
    Object.assign(cursor, {x, y});
    return g._lineTo(x, y);
  }
  g._arc = g.arc;
  g.a = g.arc = (x, y, radius, startAngle, endAngle, anticlockwise) => {
    extend(x-radius, y-radius, 2*radius, 2*radius);
    return g._arc(x, y, radius, startAngle, endAngle, anticlockwise);
  }

  if(MTLG.getOptions().webgl)
    _shape.on("added", () => {
      // possibly the shape is added before drawing on it, so cache the shape after the current execution block
      setTimeout(() => _shape.cache(bounds.x, bounds.y, bounds.width, bounds.height))
    });
  return _shape;
}


class OutlineFilter extends createjs.Filter {
  constructor(opt) {
    super();
    opt = Object.assign({color:[1.0,0.0,0.0,1.0]}, opt);
    this.color = opt.color;
    this.FRAG_SHADER_BODY =
      `uniform vec2 pixelSize;
      uniform vec4 color;
      uniform float timeNormalized;
      float rand(vec2 co){
        return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
      }
      void main(void) {
        vec4 outline;
        float strength = 0.0;
        vec4 base = texture2D(uSampler, vTextureCoord);
        vec2 offset;
        float seed = rand((timeNormalized + 0.1) * vTextureCoord);

        for(int i=-5; i<=5; i++) {
          for(int j=-5; j<=5; j++) {
            offset = vTextureCoord + (seed * pixelSize * vec2(float(i), float(j)));
            strength += texture2D(uSampler, offset).a / (1.0 + abs(float(i)) + abs(float(j)));
          }
        }
        outline.a = min(strength, 1.0);
        outline = outline.a * vec4(color.rgb * color.a, color.a);
        gl_FragColor = mix(outline, base, base.a);
      }`;
  }
  shaderParamSetup(gl, stage, shaderProgram) {
    // what is the size of a single pixel in -1, 1 (webGL) space
    gl.uniform2f(gl.getUniformLocation(shaderProgram, "pixelSize"),      2/stage._viewportWidth, 2/stage._viewportHeight);
    gl.uniform4f(gl.getUniformLocation(shaderProgram, "color"),          ...this.color);
    gl.uniform1f(gl.getUniformLocation(shaderProgram, "timeNormalized"), (Date.now()%1000)/1000);
  }
  getBounds(rect) { return (rect || new createjs.Rectangle()).pad(10,10,10,10); }
  _applyFilter(imageData) { console.warn('Filter is only implemented for WebGL.'); return false; }
}

/*
 * The base idea for fading is applying a greyscale mask over time. For fadeout, as time t goes
 * from 0.0 to 1.0, the grey value v is mapped alpha(v) =
 *  0, v < t
 *  1, t < v
 */
class FadeFilter extends createjs.Filter {
  constructor(opt) {
    super();
    opt = Object.assign({mask:'circle', duration:2000}, opt);
    this.duration = opt.duration;
    this.start = Date.now();
    var header=``, mask_alpha;
    switch(opt.mask) {
      case 'circle':
        header =
          `float mask(vec2 xy, float time) {
            float ratio = (distance(vec2(0.5, 0.5), vTextureCoord)/distance(vec2(0.5, 0.5), vec2(1.0, 1.0)));
            return smoothstep(1.2 * timeNormalized - 0.2, 1.2 * timeNormalized, ratio);
          }`; break;
      case 'horizontal':
        header =
          `float mask(vec2 xy, float time) {
            return smoothstep(1.2 * timeNormalized - 0.2, 1.2 * timeNormalized, vTextureCoord.x);
          }`; break;
      case 'perlin':
        // perlin noise from https://github.com/stegu/webgl-noise/blob/master/src/classicnoise2D.glsl (MIT license)
        // minified using https://ctrl-alt-test.fr/minifier/index
        header =
          `vec4 t(vec4 v){return v-floor(v*(1./289.))*289.;}vec4 v(vec4 v){return t((v*34.+1.)*v);}vec4 f(vec4 v){return 1.79284-.853735*v;}vec2 d(vec2 v){return v*v*v*(v*(v*6.-15.)+10.);}float perlinNoise(vec2 g){vec4 r=floor(g.rgrg)+vec4(0.,0.,1.,1.),m=fract(g.rgrg)-vec4(0.,0.,1.,1.);r=t(r);vec4 a=r.rbrb,b=r.ggaa,e=m.rbrb,c=m.ggaa,o=v(v(a)+b),l=fract(o*(1./41.))*2.-1.,u=abs(l)-.5,n=floor(l+.5);l=l-n;vec2 x=vec2(l.r,u.r),i=vec2(l.g,u.g),s=vec2(l.b,u.b),Z=vec2(l.a,u.a);vec4 Y=f(vec4(dot(x,x),dot(s,s),dot(i,i),dot(Z,Z)));x*=Y.r;s*=Y.g;i*=Y.b;Z*=Y.a;float X=dot(x,vec2(e.r,c.r)),W=dot(i,vec2(e.g,c.g)),V=dot(s,vec2(e.b,c.b)),U=dot(Z,vec2(e.a,c.a));vec2 T=d(m.rg),S=mix(vec2(X,V),vec2(W,U),T.r);float R=mix(S.r,S.g,T.g);return 2.3*R;}
          float mask(vec2 xy, float time) {
            float ratio = pow(perlinNoise(vTextureCoord.xy * 100.0) * perlinNoise(vTextureCoord.xy * 20.0), 0.5);
            return smoothstep(1.2 * timeNormalized - 0.2, 1.2 * timeNormalized, ratio);
          }`; break;
      case 'pixel':
        // pseudorandom-number-generator see https://stackoverflow.com/questions/12964279
        header =
          `float mask(vec2 xy, float time) {
            float ratio = fract(sin(dot(vTextureCoord.xy ,vec2(12.9898,78.233))) * 43758.5453);
            return smoothstep(1.2 * timeNormalized - 0.2, 1.2 * timeNormalized, ratio);
          }`; break;
      case 'noise':
        header =
          `float mask(vec2 xy, float time) {
            float r1 = fract(sin(dot(xy ,vec2(12.9898,78.233))) * 43758.5453);
            float r2 = fract(sin(dot(time*xy ,vec2(12.9898,78.233))) * 43758.5453);
            return mix(1., mix(0.0, r1, 1.-time), time) * smoothstep(1.1 * time - 0.1, 1.1 * time, r2);
          }`; break;
    }
    this.color = opt.color;
    this.FRAG_SHADER_BODY =
      `uniform float timeNormalized;
      ${header}
      void main(void) {
        vec4 base = texture2D(uSampler, vTextureCoord);
        gl_FragColor.a = mask(vTextureCoord, timeNormalized);
        gl_FragColor.rgb = base.rgb * gl_FragColor.a;
      }`;
  }
  shaderParamSetup(gl, stage, shaderProgram) {
    var progress = Date.now() >= this.start + this.duration ? 1.0 : ((Date.now()-this.start)%this.duration)/this.duration;
    gl.uniform1f(gl.getUniformLocation(shaderProgram, "timeNormalized"), progress);
  }
  getBounds(rect) { return rect; }
  _applyFilter(imageData) { console.warn('Filter is only implemented for WebGL.'); return false; }
}

/*
 * GlowFilter relies on alpha-bleeding images to produce a soft-looking result.
 */
class GlowFilter extends createjs.Filter {
  constructor(opt) {
    super();
    opt = Object.assign({color:[1.0,0.0,0.0,1.0], size:1}, opt);
    this.color = opt.color;
    this.size = opt.size;
    this.FRAG_SHADER_BODY =
      `uniform vec2 size;
      uniform vec4 color;
      uniform float timeNormalized;
      void main(void) {
        float strength = 0.0;
        float ratio = abs(timeNormalized - 0.5) * 0.8 + 0.3;
        vec4 base = texture2D(uSampler, vTextureCoord);
        vec4 glow;

        // to keep gpu usage low, only few samples must be taken
        for(int i=0; i<8; i++) {
          // lp-norm
          strength += pow(texture2D(uSampler, vTextureCoord + size*vec2(
            sin(2.*3.1415*float(i)/8.),
            cos(2.*3.1415*float(i)/8.)
          )).a, 3.);
        }
        strength = .5 * pow(strength, 1./3.);
        glow.a = min(strength, ratio);
        glow = glow.a * vec4(color.rgb * color.a, color.a);
        gl_FragColor = mix(strength*glow, base, base.a);
      }`;
  }
  shaderParamSetup(gl, stage, shaderProgram) {
    // transform from pixel-size to -1, 1 (webGL) space
    gl.uniform2f(gl.getUniformLocation(shaderProgram, "size"),           this.size*2/stage._viewportWidth, this.size*2/stage._viewportHeight);
    gl.uniform4f(gl.getUniformLocation(shaderProgram, "color"),          ...this.color);
    gl.uniform1f(gl.getUniformLocation(shaderProgram, "timeNormalized"), (Date.now()%1000)/1000);
  }
  getBounds(rect) { return (rect || new createjs.Rectangle()).pad(12,12,12,12); }
  _applyFilter(imageData) { console.warn('Filter is only implemented for WebGL.'); return false; }
}

// color alternation is visually not wholy satisfactory, probably the mixing function can be improved
class AlternateColorFilter extends createjs.Filter {
  constructor(opt) {
    super();
    opt = Object.assign({color:[1.0,0.0,0.0,1.0], interval:1000}, opt);
    this.color = opt.color;
    this.interval = opt.interval;
    this.FRAG_SHADER_BODY =
      `uniform vec4 color;
      uniform float timeNormalized;
      vec3 gamma(vec3 c) { return pow(c, vec3(1.,1.,1.)/2.2); }
      vec3 ungamma(vec3 c) { return pow(c, vec3(1.,1.,1.)*2.2); }
      void main(void) {
        vec4 base = texture2D(uSampler, vTextureCoord);
        float ratio = 1. - abs(2.*timeNormalized-1.);
        gl_FragColor.a = base.a * mix(color.a, 1., ratio);
        gl_FragColor.rgb = gamma(mix(ungamma(base.a*color.a*color.rgb), ungamma(base.rgb), ratio));
      }`;
  }
  shaderParamSetup(gl, stage, shaderProgram) {
    // transform from pixel-size to -1, 1 (webGL) space
    gl.uniform4f(gl.getUniformLocation(shaderProgram, "color"),          ...this.color);
    gl.uniform1f(gl.getUniformLocation(shaderProgram, "timeNormalized"), (Date.now()%this.interval)/this.interval);
  }
  getBounds(rect) { return (rect || new createjs.Rectangle()).pad(12,12,12,12); }
  _applyFilter(imageData) { console.warn('Filter is only implemented for WebGL.'); return false; }
}


// utility functions
var _parseColor = (color) => {
  if(!color) {
    return color;
  } else if(Array.isArray(color)) {
    if(color.length == 4) return color;
    else                  return [...color, 1];
  } else if(typeof color == 'string') {
    var base = null;
    if(color.match(/^#\w\w\w$/)) color += 'f'; // fully opaque
    if(color.match(/^#\w\w\w\w$/)) base = 16;
    if(color.match(/^#\w\w\w\w\w\w$/)) color += 'ff'; // fully opaque
    if(color.match(/^#\w\w\w\w\w\w\w\w$/)) base = 256;
    if(base) {
      var code = parseInt(color.slice(1), 16);
      return [3,2,1,0].map((i) => (Math.trunc(code/base**i)%base)/(base-1));
    } else {
      return null;
    }
  } else return null;
}
var _applyFilter = (d, Filter, opt, label) => {
  // process options
  if(opt && opt.color) opt.color = _parseColor(opt.color);
  var bounds = d.getBounds();
  var filter = new Filter(opt);
  filter.label = label;
  d.filters = (d.filters || []).filter((f) => f.label != label).concat(filter);
  // see https://github.com/CreateJS/EaselJS/issues/977
  if(!d.hitArea)
    d.hitArea = d.clone().set({scaleX:1,scaleY:1,x:d.regX,y:d.regY});
}
// @actions is of form [1000, {x:100,y:100}, 1000, {x:0,y:0}]
// The tween's transformation adds up on existing transformations.
var _transformTween = (d, actions, label) => {
  // Implementation uses 'transformMatrix'. Another approach would be using a parent Container.
  if(!d._transforms) {
    d._transforms = [];
    Object.defineProperty(d, 'transformMatrix', {
      get: function() {
        var mtx = new createjs.Matrix2D().appendTransform(d.x, d.y, d.scaleX, d.scaleY, d.rotation, d.skewX, d.skewY, d.regX, d.regY);
        d._transforms = d._transforms.filter((t) => t.apply(Date.now(), mtx));
        return mtx;
      },
      set: function(v) {}
    });
  }
  var identity = {"x":0,"y":0,"scaleX":1,"scaleY":1,"rotation":0,"skewX":0,"skewY":0,"regX":0,"regY":0};
  var keyframes = [];
  var repeat = false;
  var values = Object.assign({}, identity);
  var time = Date.now();
  keyframes.push({time,values:identity});
  for(let a of actions) {
    if(typeof a == "number") {
      time += a;
    } else if(typeof a == 'string') {
      if(a == 'repeat') repeat = true;
    } else {
      Object.assign(values, a);
      keyframes.push({time, values:Object.assign({}, values)});
    }
  }
  // return to 0(1)
  keyframes.push({time,values:identity});
  d._transforms.push({
    apply: (time, mtx) => {
      // linear interpolate
      var now = Date.now();
      if(repeat)
        now = ((now - keyframes[0].time) % (keyframes[keyframes.length-1].time - keyframes[0].time)) + keyframes[0].time;
      var i = keyframes.findIndex((f) => f.time > now);
      if(i == -1) return false;
      var prev = keyframes[i-1];
      var next = keyframes[i];
      var transform = ["x", "y", "scaleX", "scaleY", "rotation", "skewX", "skewY", "regX", "regY"]
        .map(v => ((next.time-now)*prev.values[v] + (now-prev.time)*next.values[v]) / (next.time-prev.time));
      transform[0 /*x*/   ] += d.regX;
      transform[7 /*regX*/] += d.regX;
      transform[1 /*y*/   ] += d.regY;
      transform[8 /*regY*/] += d.regY;
      mtx.appendTransform(...transform);
      return true;
    }, label
  })
}

// Curtain/Transition effects when scene changes.
MTLG.lc._nextScene = MTLG.lc.nextScene;
MTLG.lc.nextScene = (entry, gameState) => {
  // keep scaling
  var currentScene = MTLG.getStageContainer().clone();
  // decouple scene from lifecycle
  while(MTLG.getStageContainer().children[0])
    currentScene.addChild(MTLG.getStageContainer().children[0]);
  MTLG.lc._nextScene(entry, gameState);
  if(MTLG.getOptions().webgl) {
    if(currentScene.children.length == 0) return;
    // ensure whole screen blending
    currentScene.setBounds(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
    currentScene.mouseEnabled = false;
    MTLG.getStage().addChild(currentScene);
    // choose transition in following priority
    // - as defined in gameState
    // - as defined by MTLG.lc.transitioner
    // - null (default)
    MTLG.utils.gfx.fadeOut(currentScene, (gameState && gameState.transition) || (MTLG.lc.transitioner && MTLG.lc.transitioner(gameState)));
  }
}
/**
 * Set strategy to choose the next scene transition effect.
 * @function setTransitioner
 * @param {function} transitioner Function that is called with gameState as argument and must return the next transition effect label.
 * @memberof MTLG.lc#
 **/
MTLG.lc.setTransitioner = (transitioner) => MTLG.lc.transitioner = transitioner;

module.exports = {
  getText,
  getShape,
  // fragment shader effects (require StageGL)
  /**
   * (webgl) Create a noisy outline.
   * @example
   * // red outline
   * MTLG.utils.gfx.outline(stickman, {color:"#ff0000"});
   * @memberof MTLG.utils.gfx#
   **/
  outline: (d, opt=null, label=null) => _applyFilter(d, OutlineFilter, opt, label),
  /**
   * (webgl) Create a colored animated glow.
   * @example
   * // yellow glow, 10px wide
   * MTLG.utils.gfx.outline(stickman, {color:"#ee8800", size:10});
   * @memberof MTLG.utils.gfx#
   **/
  glow: (d, opt=null, label=null) => _applyFilter(d, GlowFilter, opt, label),
  /**
   * (webgl) Fadeout with a specified pattern.
   * @example
   * // circular fade out lasting 2 seconds
   * MTLG.utils.gfx.fadeOut(stickman, {mask:"circle", duration:2000});
   * @param {string} opt.mask One of <code>circle</code> (default), <code>horizontal</code>, <code>perlin</code>, <code>pixel</code> or <code>noise</code>.
   * @memberof MTLG.utils.gfx#
   **/
  fadeOut: (d, opt=null, label=null) => _applyFilter(d, FadeFilter, opt, label),
  /**
   * (webgl) Vary from original colors to monochrom and back.
   * @example
   * // change from original color to fully blue and back within 2 seconds
   * MTLG.utils.gfx.alternateColor(stickman, {color:"#00f", interval:2000});
   * @memberof MTLG.utils.gfx#
   **/
  alternateColor: (d, opt=null, label=null) =>  _applyFilter(d, AlternateColorFilter, opt, label),

  // {x,y,rot,scale} transformation effects (dont require StageGL)
  /**
   * Zoom in and out.
   * @example
   * // make the object taller, then smaller, then original size again within 1 second
   * MTLG.utils.gfx.zoomInOut(stickman, {duration:1000, factor:1.1, repeat:false});
   * @memberof MTLG.utils.gfx#
   **/
  zoomInOut: (d, opt=null, label=null) => {
    var {duration, factor, repeat} = Object.assign({duration:1000, factor:1.1, repeat:false}, opt);
    _transformTween(d, [.25*duration, {scaleX:factor,scaleY:factor}, .5*duration, {scaleX:1/factor,scaleY:1/factor}, .25*duration,
      repeat ? 'repeat' : ''], label);
  },
  /**
   * Rotate in one direction and back.
   * @example
   * // make the object rotate right and left with a period duration of 1 second
   * MTLG.utils.gfx.wobble(stickman, {duration:1000, angle:5, repeat:1});
   * @memberof MTLG.utils.gfx#
   **/
  wobble: (d, opt=null, label=null) => {
    var {duration, angle, repeat} = Object.assign({duration:1000, angle:5, repeat:false}, opt);
    _transformTween(d, [.25*duration, {rotation:angle}, .5*duration, {rotation:-angle}, .25*duration, repeat ? 'repeat' : ''], label);
  },

  // helper
  /**
   * Remove all effect(s) from the specified object that have the specified label.
   * @param {DisplayObject} d Object the effects are removed from.
   * @param {string} label Group of effects to be removed.
   * @memberof MTLG.utils.gfx#
   **/
  reset: (d, label) => {
    d.filters = (d.filters || []).filter((f) => f.label != label);
    if(d._transforms) d._transforms = d._transforms.filter((t) => t.label != label);
  },
  /**
   * Set transformation anchor to object center. Changes object's <code>x</code>, <code>y</code>, <code>regX</code> and <code>regY</code> property. Useful if transformation effects shall be applied to the object's center.
   * @param {DisplayObject} d Object to be recentered.
   * @memberof MTLG.utils.gfx#
   **/
  recenter: (d) => {
    var {width,height} = d.getBounds();
    d.set(d.getMatrix().transformPoint(.5*width, .5*height));
    return d.set({regX:.5*width, regY:.5*height});
  },
  /**
   * Mingle colors into one color.
   * @param {arguments|array} colors Color values to be mingled.
   * @example
   * MTLG.utils.gfx.mixColors('#ff0000', '#0f00', [0,0,1.0,0], '#000');
   * // → [0.25, 0.25, 0.25, 0.5]
   * @memberof MTLG.utils.gfx#
   **/
  mixColors: (...colors) => colors.map(_parseColor).filter(color => color != null)
    .reduce((prev, curr, _, arr) =>
      !prev ? curr.map((_,channel) => curr[channel] / arr.length)
            : prev.map((_,channel) => prev[channel] + curr[channel] / arr.length),
      null)
};
