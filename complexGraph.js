/**
 * A slightly more complex graph structure.
 * @example <caption>Basic usage</caption>
 * //Create a new graph in container or on stage stageC
 * var myGraph = new MTLG.utils.complexGraph(stageC);
 * //Add a new logical node
 * var newNode = myGraph.addNode();
 * //Draw the new node at position 100,100
 * newNode.addVisualNode(100,100,stageC);
 * @example <caption>Colors</caption>
 * //Create a graph
 * var myGraph = new MTLG.utils.complexGraph(area);
 * //Change colors
 * myGraph.setColors('red', 'white', 'black'); //edge, nodeBg, nodeText
 * @namespace complexGraph
 * @memberof MTLG.utils
 */
var complexGraph = (function(){
  //Get the images to draw buttons
  require.context('./img');


  // Array von Kanten geloescht, ist schon im GraphStructure
  var edgeCounter = 1;
  var minLength = 150;

  /**
   * Inner class of complexGraph.
   * With Edge() you can create a new Edge with the following parameters
   * Note: Not really parameters, members
   * @class Edge
   * @memberof MTLG.utils.complexGraph
   * @param {MTLG.utils.complexGraph.Graph} parentGraph
   */
  var Edge = function(parentGraph) {
    var edgeColour = parentGraph.getColors().edge;
    var nodeBackgroundColour = parentGraph.getColors().nodeBg;

    /**
     * The graph this edge belongs to.
     * @name parentGraph
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.parentGraph = parentGraph;
    /**
     * The node where the edge begins.
     * @name nodeFrom
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.nodeFrom = 0; //prompt("Bezeichnung des ausgehenden Knoten eingeben");
    /**
     * The node where the edge ends.
     * @name nodeTo
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.nodeTo = 0; //prompt("Bezeichnung des eingehenden Knoten eingeben");
    /**
     * The value at the beginning of the edge.
     * @name valueFrom
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.valueFrom = "1,n"; //prompt("Wert des ausgehenden Knoten eingeben");
    /**
     * The value at the end of the edge.
     * @name valueTo
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.valueTo = "1"; //prompt("Wert des eingehenden Knoten eingeben");
    /**
     * Unique ID, which is increased by one for each edge.
     * @name id
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.id = elementCounter;
    /**
     * Shows the type of the object (0= node, 1= edge).
     * @name typeOf
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.typeOf = 1 //typeOf 0=Node, 1=Edge, 2 = Storage Controller
    /**
     * Boolean, whether the edge is visible or not.
     * @name visible
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.visible = true;
    /**
     * Node ID, if nodeFrom ist a topnode, otherwise 0.
     * @name representFrom
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.representFrom = 0;
    /**
     * Node ID, if nodeTo ist a topnode, otherwise 0.
     * @name representTo
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.representTo = 0; // 0 means it is not a representive edge, otherwise it is the node-ID of the subnode
    /**
     * Container of the edgeView.
     * @name container
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.container = new createjs.Container();
    /**
     * Boolean, indicates whether the left arrow is set.
     * @name leftArrowVisible
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.leftArrowVisible = false;
    /**
     * Boolean, indicates whether the right arrow is set.
     * @name rightArrowVisible
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.rightArrowVisible = false;
    /**
     * Boolean, indicates whether the left value is set.
     * @name leftValueVisible
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.leftValueVisible = false;
    /**
     * Boolean, indicates whether the right value is set.
     * @name rightValueVisible
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.rightValueVisible = false;
    /**
     * The text above the edge.
     * @name title
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.title = "Edge " + this.id;
    /**
     * Whether the edge is dashed, either true or false.
     * @name dashed
     * @memberof MTLG.utils.complexGraph.Edge#
     */
    this.dashed = false;
    this.valueToChange=null;

    elementCounter++;
    //edges.push(this);



    this.deleteEdge = function() {
      //NÖTIG?
    };

    this.setValueFrom = function(value) {
      this.valueFrom = value;
    };

    this.setValueTo = function(value) {
      this.valueTo = value;
    };

    this.getValueFrom = function() {
      return this.valueFrom;
    };

    this.getValueTo = function() {
      return this.valueTo;
    };
    this.getNodeFrom = function() {
      return this.nodeFrom;
    };
    this.getNodeTo = function() {
      return this.nodeTo;
    };

    this.setNodeTo = function(nodeId) {
      this.nodeTo = nodeId;
    };
    this.setNodeFrom = function(nodeId) {
      this.nodeFrom = nodeId;
    };

    this.setEdgeTitle = function(title) {
      this.title = title;
    };

    this.getEdgeTitle = function() {
      return this.title;
    };

    this.getId = function() {
      return this.id;
    };

    this.setVisible = function(bool) {
      this.visible = bool;
    };

    this.getVisible = function() {
      return this.visible;
    };

    this.setRepresentTo = function(bool) {
      this.representTo = bool;
    };

    this.getRepresentTo = function() {
      return this.representTo;
    };

    this.setRepresentFrom = function(bool) {
      this.representFrom = bool;
    };

    this.getRepresentFrom = function() {
      return this.representFrom;
    };

    this.setEdgeId = function(newId) {
      this.id = newId;
    };

    this.getType = function() {
      return this.typeOf;
    };

    this.getDashed = function(){
      return this.dashed;
    }

    this.setDashed = function(dashed){
      this.dashed = dashed;
    }

    this.getLeftArrowVisible = function(){
      return this.leftArrowVisible;
    }

    this.setLeftArrowVisible = function(visible){
      this.leftArrowVisible = visible;
    }

    this.getRightArrowVisible = function(){
      return this.rightArrowVisible;
    }

    this.setRightArrowVisible = function(visible){
      this.rightArrowVisible = visible;
    }

    this.getLeftValueVisible = function(){
      return this.leftValueVisible;
    }

    this.setLeftValueVisible = function(visible){
      this.leftValueVisible = visible;
    }

    this.getRightValueVisible = function(){
      return this.rightValueVisible;
    }

    this.setRightValueVisible = function(visible){
      this.rightValueVisible = visible;
    }


    //EdgeView

    /**drawEdgeView calculate which side the egde should be aligned to and run edgeViewHelp to draw the EdgeView */
    /**@param area: aera to draw in
     * @param xFrom: the x-coordinate of the edge's starting point
     * @param yFrom: the y-coordinate of the edge's starting point
     * @param xTo: the x-coordinate of the edge's endpoint
     * @param yTo: the y-coordinate of the edge's endpoint
     * @param center: coordinates of the center of the edge (local variable)
     * @param {Boolean} leftBool: true if the edge's center is closer to the left side than to the right (local variable)
     * @param {Boolean} aboveBool: true if the edge#s center is closer to the upper side than to the lower side (local variable)
     * @param angle: lead angle of the edge (helpful to align the edge)(local variable )*/
    this.drawEdgeView = function(area, xFrom, yFrom, xTo, yTo,edgeColour) {
      var offset = 0;
      var stageFrom = this.parentGraph.getArea().globalToLocal(xFrom, yFrom);
      var stageTo = this.parentGraph.getArea().globalToLocal(xTo, yTo);
      var center = {
        x: (stageTo.x + stageFrom.x) / 2,
        y: (stageTo.y + stageFrom.y) / 2
      };

      //Sometimes areas do not have bounds. To still calculate a center, use the global stage in this case
      var activeArea;
      if(this.parentGraph.getArea().getBounds() != null){
        activeArea = this.parentGraph.getArea();
      }else{
        activeArea = MTLG.getStage();
        console.log("Given Area does not have bounds! Using stage bounds instead.");
      }

      var distanceLeft = center.x;
      var distanceRight = activeArea.getBounds().width - center.x;
      var distanceAbove = center.y;
      var distanceBelow = activeArea.getBounds().height - center.y;
      var leftBool;
      var aboveBool;
      //which side is closer to the center
      if (distanceLeft < distanceRight) {
        leftBool = true;
      } else {
        leftBool = false;
      }
      if (distanceAbove < distanceBelow) {
        aboveBool = true;
      } else {
        aboveBool = false;
      }

      //calculate the angle
      if (xFrom > xTo) {
        offset = 180;
      }
      var angle = 0;
      var dx = xTo - xFrom;
      var dy = yTo - yFrom;
      var length;
      if (yFrom == yTo) {
        if (xFrom < xTo) {
          angle = 0;
          length = dx;
        }
        if (xFrom > xTo) {
          angle = 180;
          length = -dx;
        }

      } else {
        angle = (Math.atan(dy / dx) / Math.PI) * 180 + offset;
        length = dy / Math.sin(angle * Math.PI / 180);
      }

      var nodeFrom = this.parentGraph.getNodetoId(this.nodeFrom);
      var nodeTo = this.parentGraph.getNodetoId(this.nodeTo);
      //align
      switch (nodeFrom.container.rotation) {
        case 0:
          if (nodeTo.container.rotation == nodeFrom.container.rotation) {
            if (angle > -90 && angle < 90) {
              this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
              return;
            } else if (angle > 90 && angle < 270) {
              this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
              return;
            }
          }
          break;
        case 90:
          if (nodeTo.container.rotation == nodeFrom.container.rotation) {
            if (angle > 0 && angle < 180) {
              this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
              return;
            } else if ((angle > 180 && angle < 270) || (angle > -90 && angle < 0)) {
              this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
              return;
            }
          }
          break;
        case 180:
          if (nodeTo.container.rotation == nodeFrom.container.rotation) {
            if (angle > -90 && angle < 90) {
              this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
              return;
            } else if (angle > 90 && angle < 270) {
              this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
              return;
            }
          }
          break;
        case -90:
          if (nodeTo.container.rotation == nodeFrom.container.rotation) {
            if (angle > 0 && angle < 180) {
              this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
              return;
            } else if ((angle > 180 && angle < 270) || (angle > -90 && angle < 0)) {
              this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
              return;
            }
          }
          break;
        default:

      }
      if ((angle >= 0 && angle < 45) || (angle >= -45 && angle < 0) || (angle >= 135 && angle < 225)) {
        if ((angle >= 0 && angle < 45) || (angle >= -45 && angle < 0)) {
          if (aboveBool) {
            return this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
          } else {
            return this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
          }
        } else {
          if (aboveBool) {
            return this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
          } else {
            return this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
          }
        }
      } else {
        if (angle >= 45 && angle < 135) {
          if (leftBool) {
            return this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
          } else {
            return this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
          }
        } else {
          if (leftBool) {
            return this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
          } else {
            return this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
          }
        }
      }

      //this.drawEdgeViewHelp(area, xFrom, yFrom, xTo, yTo, true);
      this.drawEdgeViewHelp(area, xTo, yTo, xFrom, yFrom, false);
      console.log("HIER SOLLTE MAN NICHT HINKOMMEN KÖNNEN !!!");
      console.log("IN EDGESTRUCTURE DIE FUNKTION DRAWEDGEVIEW ÜBERPRÜFEN!!!");
    };


    /** auxiliary functions for the drawing process */
    this.addChildAndSetVisible = function(parent, child) {
      parent.addChild(child);
      child.visible = true;
    }

    this.removeChildAndVisible = function(parent, child) {
      parent.removeChild(child);
      child.visible = false;
    }

    //TODO constants
    var textSize = 30;
    var letterHeight = 1;
    var letterWidth = 0.45;

    this.drawValue = function(position, h, text, maximumWidth) {
      var width = textSize * letterWidth * (text.length + 1);
      var height = textSize * letterHeight;
      if (width > maximumWidth) {
        width = maximumWidth;
      }
      var value = MTLG.utils.gfx.getText(text, textSize + 'px Arial', edgeColour);
      value.visible = false;
      value.maxWidth = maximumWidth;
      value.x = position;
      value.y = h;
      value.bigButton = MTLG.utils.gfx.getShape();
      value.bigButton.graphics.beginFill('red').drawRect(-width / 2, 0, width, height);
      value.bigButton.x = value.x;
      value.bigButton.y = value.y;
      value.bigButton.alpha = 0.01;
      //console.log("value", value);

      return value;
    }

    /** in drawEdgeViewHelp the visual component of an edge is drawn
         proceeding: there are two points given and the distance(length) and the angle(angle) between them are calculated
         proceeding: at first an edge with the calculated length is drawn horizontally in its own container
         proceeding: then we rotate the edge by the calculated angle*/
    /**drawEdgeView calculate which side the egde should be aligned to and run edgeViewHelp to draw the EdgeView */
    /**@param area: aera to draw in
     * @param xFrom: x-coordinate of the edge's starting point
     * @param yFrom: y-coordinate of the edge's starting point
     * @param xTo: x-coordinate of the edge's endpoint
     * @param yTo: y-coordinate of the edge's endpoint
     * @param {Boolean} rightDirection: indicates to which side the edge is aligned to
     * @param angle: lead angel of the edge (local variable)
     * @param length: edge's length(local variable)
     * @param height: distance between edgemenu and edge (local variable)*/
    this.drawEdgeViewHelp = function(area, xFrom, yFrom, xTo, yTo, rightDirection) {
      var edgeColour = parentGraph.getColors().edge;
      var nodeBackgroundColour = parentGraph.getColors().nodeBg;
      var container = this.container;
      var edge = this;
      var offset = 0;
      if (xFrom > xTo) {
        offset = 180;
      }
      var angle = 0;
      var dx = xTo - xFrom;
      var dy = yTo - yFrom;
      var length;
      if (yFrom == yTo) {
        if (xFrom < xTo) {
          angle = 0;
        }
        if (xFrom > xTo) {
          angle = 180;
        }

      } else {
        angle = (Math.atan(dy / dx) / Math.PI) * 180 + offset;
        length = dy / Math.sin(angle * Math.PI / 180);
      }

      if (angle == 180) length = -dx;
      if (angle == 0) length = dx;
      var maxWidth = 2 / 10 * length;
      var maxWidthText = 6 / 10 * length;
      var height = 30;
      container.x = xFrom;
      container.y = yFrom;
      //console.log("edge "+edge.id + " angle "+angle+ " länge "+length+" xFrom " +xFrom+" xTo "+xTo+" yFrom "+yFrom+ " yTo "+yTo +" "+rightDirection);

      var kante = this.drawMainEdge(length, this.dashed,parentGraph);

      if (length >= minLength) {
        //edge.setEdgeTitle("Kante "+ edge.getId());
        var edgeTitle = this.getEdgeTitle();
        var newTitle;
        //console.log(this);
        var edgeText = this.drawValue(length / 2, -1.5 * height, edgeTitle, maxWidthText);
        edgeText.textAlign = "center";
        edgeText.bigButton.addEventListener('mousedown', function() {
        });
      }
      if (rightDirection) {
        var rightArrowButton = this.drawRightArrowButton(length * (1 / 2) + 30 + 20, height);
        rightArrowButton.visible = false;
        var leftArrowButton = this.drawLeftArrowButton(length * (1 / 2) - 30 - 20, height);
        var leftValueButton = this.drawValueButton(length * (1 / 10), height);
        var rightValueButton = this.drawValueButton(length * (9 / 10), height);
        var deleteButton = this.drawDeleteButton(length * (1 / 2), height);
        var rightArrow = this.drawRightArrow(length);
        var leftArrow = this.drawLeftArrow(0);
      } else {
        var leftArrowButton = this.drawRightArrowButton(length * (1 / 2) + 30 + 20, height);
        var rightArrowButton = this.drawLeftArrowButton(length * (1 / 2) - 30 - 20, height);
        var rightValueButton = this.drawValueButton(length * (1 / 10), height);
        var leftValueButton = this.drawValueButton(length * (9 / 10), height);
        var deleteButton = this.drawDeleteButton(length * (1 / 2), height);
        var leftArrow = this.drawRightArrow(length);
        var rightArrow = this.drawLeftArrow(0);
      }
      var leftValue = new Object();
      var rightValue = new Object();
      leftValue.visible = false;
      rightValue.visible = false;

      if (edge.leftArrowVisible) {
        this.addChildAndSetVisible(container, leftArrow);
      }
      if (edge.rightArrowVisible) {
        this.addChildAndSetVisible(container, rightArrow);
      }
      if (edge.leftValueVisible && (length >= minLength)) {
        if (rightDirection) {
          leftValue = this.drawValue(length * (1 / 10), -1.5 * height, edge.getValueFrom(), maxWidth);
        } else {
          leftValue = this.drawValue(length * (9 / 10), -1.5 * height, edge.getValueFrom(), maxWidth);
        }
        leftValue.textAlign = "center";
        this.addChildAndSetVisible(container, leftValue);
        this.addChildAndSetVisible(container, leftValue.bigButton);
        leftValue.bigButton.addEventListener('click', function() {
        });
      }

      if (edge.rightValueVisible && (length >= minLength)) {
        if (rightDirection) {
          rightValue = this.drawValue(length * (9 / 10), -1.5 * height, edge.getValueTo(), maxWidth);
        } else {
          rightValue = this.drawValue(length * (1 / 10), -1.5 * height, edge.getValueTo(), maxWidth);
        }
        rightValue.textAlign = "center";
        this.addChildAndSetVisible(container, rightValue);
        this.addChildAndSetVisible(container, rightValue.bigButton);
        rightValue.bigButton.addEventListener('click', function() {
        });
      }


      kante.bigButton.on('click', function(evt) { //the edgemenu appears and disappers with a click on the edge
        if (length >= minLength) {
          if (rightArrowButton.visible) {

            this.removeChildAndVisible(container, rightArrowButton);
            this.removeChildAndVisible(container, rightArrowButton.bigButton);

            this.removeChildAndVisible(container, leftArrowButton);
            this.removeChildAndVisible(container, leftArrowButton.bigButton);

            this.removeChildAndVisible(container, leftValueButton);
            this.removeChildAndVisible(container, leftValueButton.bigButton);

            this.removeChildAndVisible(container, rightValueButton);
            this.removeChildAndVisible(container, rightValueButton.bigButton);

            this.removeChildAndVisible(container, deleteButton);
            this.removeChildAndVisible(container, deleteButton.bigButton);
          } else {

            this.addChildAndSetVisible(container, rightArrowButton)
            this.addChildAndSetVisible(container, rightArrowButton.bigButton);

            this.addChildAndSetVisible(container, leftArrowButton);
            this.addChildAndSetVisible(container, leftArrowButton.bigButton);

            this.addChildAndSetVisible(container, leftValueButton);
            this.addChildAndSetVisible(container, leftValueButton.bigButton);

            this.addChildAndSetVisible(container, rightValueButton);
            this.addChildAndSetVisible(container, rightValueButton.bigButton);

            this.addChildAndSetVisible(container, deleteButton);
            this.addChildAndSetVisible(container, deleteButton.bigButton);
          }
        }
        //console.log(edge);
      }.bind(this));
      var start = new Object();
      kante.bigButton.addEventListener('mousedown', function(evt) {
        start = {
          x: evt.stageX,
          y: evt.stageY
        };
      });
      kante.bigButton.addEventListener('pressup',function(evt){
        if(Math.abs(evt.stageX-start.x)>100 || Math.abs(evt.stageY-start.y)>100){
          if (edge.dashed === false) {
            edge.dashed = true;
          } else {
            edge.dashed = false;
          }
          parentGraph.getNodetoId(edge.nodeFrom).moveConnectors(area);
        }
      });

      leftValueButton.bigButton.on('click', function(evt) { // the left value appears by a click on the valuebutton
        if (leftValue.visible) {
          edge.leftValueVisible = false;
          this.removeChildAndVisible(container, leftValue);
          this.removeChildAndVisible(container, leftValue.bigButton);
        } else {
          edge.leftValueVisible = true;
          if (rightDirection) {
            leftValue = this.drawValue(length * (1 / 10), -1.5 * height, edge.getValueFrom(), maxWidth);
          } else {
            leftValue = this.drawValue(length * (9 / 10), -1.5 * height, edge.getValueFrom(), maxWidth);
          }
          leftValue.textAlign = "center";
          this.addChildAndSetVisible(container, leftValue);
          this.addChildAndSetVisible(container, leftValue.bigButton);
          leftValue.bigButton.addEventListener('click', function() {
          });
        }
      }.bind(this));

      rightValueButton.bigButton.on('click', function(evt) { //the right value appears by a click on the valuebutton
        if (rightValue.visible) {
          edge.rightValueVisible = false;
          this.removeChildAndVisible(container, rightValue);
          this.removeChildAndVisible(container, rightValue.bigButton);
        } else {
          edge.rightValueVisible = true;
          if (rightDirection) {
            rightValue = this.drawValue(length * (9 / 10), -1.5 * height, edge.getValueTo(), maxWidth);
          } else {
            rightValue = this.drawValue(length * (1 / 10), -1.5 * height, edge.getValueTo(), maxWidth);
          }
          rightValue.textAlign = "center";
          this.addChildAndSetVisible(container, rightValue);
          this.addChildAndSetVisible(container, rightValue.bigButton);
          rightValue.bigButton.addEventListener('click', function() {
          });
        }
      }.bind(this));

      rightArrowButton.bigButton.on('click', function(evt) { //the right arrow appears by a click on the rightarrowbutton
        if (rightArrow.visible) {
          edge.rightArrowVisible = false;
          this.removeChildAndVisible(container, rightArrow);
        } else {
          edge.rightArrowVisible = true;
          this.addChildAndSetVisible(container, rightArrow);
        }
      }.bind(this));

      leftArrowButton.bigButton.on('click', function(evt) { //the left arrow appears by a click on the leftarrowbutton
        if (leftArrow.visible) {
          edge.leftArrowVisible = false;
          this.removeChildAndVisible(container, leftArrow);
        } else {
          edge.leftArrowVisible = true;
          this.addChildAndSetVisible(container, leftArrow);
        }
      }.bind(this));

      deleteButton.bigButton.on('click', function(evt) { // we can delete the edge by a click on the deletebutton
        parentGraph.removeEdge(edge.id);
      });




      container.rotation = angle;
      if (length >= minLength) {
        container.addChild(edgeText, edgeText.bigButton);
        edgeText.visible = true;
      }
      container.addChild(kante, kante.bigButton);
      area.addChild(container);

      this.setVisualTitle = function(){
        var newTitle = edge.getEdgeTitle();
        if (newTitle != null) {
          edge.setEdgeTitle(newTitle);
          edgeText.text = newTitle;
          var width = textSize * letterWidth * (newTitle.length + 1);
          var height = textSize * letterHeight;
          if (width > edgeText.maxWidth) {
            width = edgeText.maxWidth;
          }
          edgeText.bigButton.graphics.beginFill('red').drawRect(-width / 2, 0, width, height);
        }
      };

      this.setVisualValueFrom = function(){
        var newValue =edge.getValueFrom();
        if (newValue != null) {
          edge.setValueFrom(newValue);
          leftValue.text = newValue;
          var width = textSize * letterWidth * (newValue.length + 1);
          var height = textSize * letterHeight;
          if (width > leftValue.maxWidth) {
            width = leftValue.maxWidth;
          }
          leftValue.bigButton.graphics.beginFill('red').drawRect(-width / 2, 0, width, height);
        }
      }

      this.setVisualValueTo = function(){
        var newValue =edge.getValueTo();
        if (newValue != null) {
          edge.setValueTo(newValue);
          rightValue.text = newValue;
          var width = textSize * letterWidth * (newValue.length + 1);
          var height = textSize * letterHeight;
          if (width > rightValue.maxWidth) {
            width = rightValue.maxWidth;
          }
          rightValue.bigButton.graphics.beginFill('red').drawRect(-width / 2, 0, width, height);
        }
      }
    };

    /** this function removes the edgeView */
    this.deleteEdgeView = function(area) {
      //area.removeChild(this.container);
      this.container.removeAllChildren();
    };





    this.drawMainEdge = function(length, dashed,parentGraph) {
      //var edgeColour = parentGraph.getColors().edge;
      var edge = MTLG.utils.gfx.getShape();
      edge.bigButton = MTLG.utils.gfx.getShape();
      edge.bigButton.graphics.beginFill(edgeColour).drawRect(0, -15, length, 30);
      if (dashed === true) {
        var tmpLength = length;
        var l = 5;
        var xPos = 0;
        var black = true;
        while (tmpLength > l) {
          if (black) {

            edge.graphics.beginStroke(edgeColour).moveTo(xPos, 0).lineTo(xPos + l, 0);
            xPos += l;
            black = false;
          } else {
            xPos += l;

            black = true;
          }
          tmpLength -= l;
        }
        if (black) {

          edge.graphics.beginStroke(edgeColour).moveTo(xPos, 0).lineTo(xPos + tmpLength, 0);
          xPos += l;
          black = false;
        }
      } else {
        edge.graphics.beginStroke(edgeColour).moveTo(0, 0).lineTo(length, 0);
      }
      edge.x = 0;
      edge.y = 0;
      edge.bigButton.x = edge.x;
      edge.bigButton.y = edge.y;
      edge.bigButton.alpha = 0.01;

      return edge;
    }

    this.drawRightArrowButton = function(position, h) {
      var arrow = MTLG.utils.gfx.getShape();
      arrow.visible = false;
      arrow.graphics.beginFill(edgeColour).moveTo(0, h).lineTo(-10, 10 + h).lineTo(-10, -10 + h).lineTo(0, h);
      arrow.x = position;
      arrow.bigButton = MTLG.utils.gfx.getShape();
      arrow.bigButton.graphics.beginStroke(edgeColour).beginFill(nodeBackgroundColour).drawRect(-25, h - 15, 40, 30);
      arrow.bigButton.x = arrow.x;
      arrow.bigButton.y = arrow.y;
      arrow.bigButton.alpha = 0.01;

      return arrow;
    }

    this.drawLeftArrowButton = function(position, h) {
      var arrow = MTLG.utils.gfx.getShape();
      arrow.visible = false;
      arrow.graphics.beginFill(edgeColour).moveTo(0, h).lineTo(10, 10 + h).lineTo(10, -10 + h).lineTo(0, h);
      arrow.x = position;
      arrow.bigButton = MTLG.utils.gfx.getShape();
      arrow.bigButton.graphics.beginStroke(edgeColour).beginFill(nodeBackgroundColour).drawRect(-15, h - 15, 40, 30);
      arrow.bigButton.x = arrow.x;
      arrow.bigButton.y = arrow.y;
      arrow.bigButton.alpha = 0.01;

      return arrow;
    }

    this.drawValueButton = function(position, h) {

      var value = MTLG.utils.gfx.getShape();
      value.visible = false;
      value.graphics.beginStroke(edgeColour).moveTo(-10, h - 10).lineTo(10, h - 10).lineTo(10, h + 10).lineTo(-10, h + 10).lineTo(-10, h - 10);
      value.graphics.moveTo(0, h + 8).lineTo(0, h - 8).lineTo(-5, h);
      value.x = position;
      value.bigButton = MTLG.utils.gfx.getShape();
      value.bigButton.graphics.beginFill(nodeBackgroundColour).drawRect(-20, h - 15, 40, 30);
      value.bigButton.x = value.x;
      value.bigButton.y = value.y;
      value.bigButton.alpha = 0.01;

      return value;
    }

    this.drawDeleteButton = function(position, h) {
      var button = MTLG.utils.gfx.getShape();
      var d = 2;
      var d1 = Math.sin(45) * (d / 2);
      button.visible = false;
      //button.graphics.beginStroke(edgeColour).moveTo(-10, h - 10).lineTo(10, h + 10).moveTo(-10, h + 10).lineTo(10, h - 10);
      button.graphics.beginFill(edgeColour).moveTo(-d, h).lineTo(-10 - d1, -10 + d1 + h).lineTo(-10 + d1, -10 - d1 + h).lineTo(0, h - d).lineTo(10 - d1, h - 10 - d1).lineTo(10 + d1, h - 10 + d1).lineTo(d, h).lineTo(10 + d1, 10 - d1 + h);
      button.graphics.moveTo(10 + d1, 10 - d1 + h).lineTo(10 - d1, 10 + d1 + h).lineTo(0, d + h).lineTo(-10 + d1, 10 + d1 + h).lineTo(-10 - d1, 10 - d1 + h).lineTo(-d, h);
      button.x = position;
      button.bigButton = MTLG.utils.gfx.getShape();
      button.bigButton.graphics.beginStroke(edgeColour).beginFill(nodeBackgroundColour).drawRect(-20, h - 15, 40, 30);
      button.bigButton.x = button.x;
      button.bigButton.y = button.y;
      button.bigButton.alpha = 0.01;

      return button;
    }

    this.drawRightArrow = function(position) {
      var arrow = MTLG.utils.gfx.getShape();
      arrow.visible = false;
      arrow.graphics.beginFill(edgeColour).moveTo(0, 0).lineTo(-20, -10).lineTo(-20, 10).lineTo(0, 0);
      arrow.x = position;

      return arrow;
    }

    this.drawLeftArrow = function(position) {
      var arrow = MTLG.utils.gfx.getShape();
      arrow.visible = false;
      arrow.graphics.beginFill(edgeColour).moveTo(0, 0).lineTo(20, -10).lineTo(20, 10).lineTo(0, 0);
      arrow.x = position;

      return arrow;
    }
  }

  /**
   * Constructor of node structure
   */
  var areaBounds = {x:2000, y:2000};

  /**
   * A class representing a node in a graph
   * @class Node
   * @memberof MTLG.utils.complexGraph
   * @param {boolean} ready
   * @param {object} parentGraph The graph this node belongs to
   **/
  function Node(ready,parentGraph) {
    var edgeColour = parentGraph.getColors().edge;
    var nodeBackgroundColour = parentGraph.getColors().nodeBg;
    var nodeTextColour = parentGraph.getColors().nodeTx;

    this.parentGraph = parentGraph;

    // set node id and raise nodeCounter
    this.id = elementCounter;
    elementCounter += 1;

    /**
     * @name name
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.name = "New Node";
    /**
     * Type is either 0=node or 1=edge.
     * @name typeOf
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.typeOf = 0;
    /**
     * Set visibility.
     * @name visible
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.visible = true;
    /**
     * Array of own nodes, contains node Ids.
     * @name edges
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.edges = new Array();
    /**
     * Array of own edges, contains edge Ids.
     * @name subNodes
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.subNodes = new Array();
    /**
     * Visual position for storing/loading. default 0, bis merge mit connector.
     * @name xPos
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.xPos = 0; //default 0, bis merge mit connector
    /**
     * Visual position for storing/loading. default 0, bis merge mit connector.
     * @name yPos
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.yPos = 0; //default 0, bis merge mit connector

    // container for visual node
    this.container = new createjs.Container();

    //Improved(?) x, y setting
    this.container.__x = this.container.x;
    this.container.__y = this.container.y;
    var that = this;
    Object.defineProperty(this.container, 'x', {
      get: function(){
        return this.__x;
      },
      set: function(newX){
        this.__x = newX;
        that.xPos = newX;
      }
    });
    Object.defineProperty(this.container, 'y', {
      get: function(){
        return this.__y;
      },
      set: function(newY){
        this.__y = newY;
        that.yPos = newY;
      }
    });

    /**
     * Size of a visual node.
     * @name size
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.size = 100;

    /**
     * Connector arrays.
     * @name sides
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.sides = new Array();
    for (var i = 0; i < 4; i++) {
      //ein Array für jede Seite in die Connestoren eingefügt werden
      // 0: oben
      // 1: rechts
      // 2: unten
      // 3: links
      this.sides[i] = [];
    }
    this.invisibleConnectors = new Array();
    /**
     * @name invisibleEdgesSC
     * @memberof MTLG.utils.complexGraph.Node#
     */
    this.invisibleEdgesSC = new Array();
    this.dx = 0;
    this.dy = 0;
    this.deleted = false;
    this.subDistUp = 0;
    this.subDistDown = 0;
    this.subDistLeft = 0;
    this.subDistRight = 0;
    if (ready === false) {
      this.ready = ready;
    } else {
      this.ready = true;
    }
    this.connectionCounter = 0;


    /** Kanten an Oberknoten werden auch in edges hinzugefuegt.
          Array von Kanten der Sub-Knoten, das Edge-Array ist dann leer */
    // this.subEdges = new Array();


    /**
     * Funktion zum Zurueckgeben der ID
     * @function getId
     * @memberof MTLG.utils.complexGraph.Node#
     * @return {number} ID of this node
     **/
    this.getId = function() {
      return this.id;
    };

    /** Funktion zum Zurueckgeben des Namens */
    this.getName = function() {
      return this.name;
    };

    /** Funktion zum Zurueckgeben von visible */
    this.isVisible = function() {
      return this.visible;
    };

    /** Funktion zum Setzen des Namens */
    this.setName = function(newName) {
      this.name = newName;
    };
    // Function to set a new visual name after changing the logical
    this.setVisualName = function() {
      var stringKey = this.getName();
      //Cut off too long name
      if (stringKey.length >= 9) {
        var shortString = stringKey.slice(0, 8);
        stringKey = shortString.concat("...");
      }
      this.container.getChildByName("NodeName").text = stringKey;
    }

    /** Funktion zum Setzen der Sichtbarkeit */
    this.setVisible = function(status) {
      this.visible = status;
    };
    this.getVisible = function() {
      return this.visible;
    };

    this.setId = function(newId) {
      this.id = newId;
    };

    this.setEdges = function(newEdges) {
      this.edges = newEdges;
    };

    this.setSubNodes = function(newSubnodes) {
      this.subNodes = newSubnodes;
    };

    this.setSides = function(newSides) {
      this.sides = newSides;
    };

    this.getxPos = function() {
      return this.xPos;
    };

    this.setxPos = function(newX) {
      this.xPos = newX;
    };

    this.getyPos = function() {
      return this.yPos;
    };

    this.setyPos = function(newY) {
      this.yPos = newY;
    };

    this.getType = function() {
      return this.typeOf;
    };

    this.setInvisibleEdgesSC = function(array) {
      this.invisibleEdgesSC = array;
    };

    this.getInvisibleEdgesSC = function() {
      return this.invisibleEdgesSC;
    };

    this.getHighestTopNode = function() {
      if (this.getVisible()) {
        return this;
      }
      console.log("suche highest topNode von " + this.id);

      var topNode = this.getTopNode();
      var visible = topNode.getVisible();
      while (!visible) {
        topNode = topNode.getTopNode();
        visible = topNode.getVisible();
      }
      console.log("es ist node " + topNode.id);
      return topNode;
    };

    this.getTopNode = function() {
      var id = this.getId();
      if (this.getVisible()) {
        return 0;
      }
      for (var i = 0; i < this.parentGraph.nodes.length; i++) {
        if (isInArray(this.parentGraph.nodes[i].subNodes, id)) {
          return id;
        }
      }
    };

    this.removeEdgeAndConnectors = function(edgeId) {
      var node = this;
      for (var i = 0; i < node.sides.length; i++) {
        for (var j = 0; j < node.sides[i].length; j++) {
          if (node.sides[i][j].edge.id == edgeId) {
            node.container.removeChild(node.sides[i][j].view);
            node.sides[i].splice(j, 1);
            break;
          }
        }
      }
      if (node.getVisible()) {
        node.setConnectorPosition();
        node.updateEdgeViews(this.parentGraph.getArea());
      }
      for (var j = 0, len = node.edges.length; j < len; j++) {
        if (edgeId == node.edges[j]) {
          node.edges.splice(j, 1);
          break;
        }
      }
      //console.log("KANTE "+edgeId+" GELÖSCHT FÜR KNOTEN "+node.id);
    };
    this.deleteEdgeInAllSubNodes = function(edge) {
      var node = this;
      var subNode;
      if (node.getId() == edge.getNodeFrom()) {
        subNode = this.parentGraph.getNodetoId(edge.getRepresentFrom());
      } else {
        subNode = this.parentGraph.getNodetoId(edge.getRepresentTo());
      }
      //console.log(node);
      //console.log(subNode);
      while (node.getId() != subNode.getId()) {
        subNode.removeEdgeAndConnectors(edge.getId());
        subNode = subNode.getTopNode();
      }
    };

    this.getTopNode = function() {
      var id = this.id;
      for (var i = 0; i < this.parentGraph.nodes.length; i++) {
        if (isInArray(this.parentGraph.nodes[i].subNodes, id)) {
          return this.parentGraph.nodes[i];
        }
      }
      //console.log("this is not a subNode");
      return this;
    };

    this.findSimilarEdge = function(node2) {
      var allNodeIds = [];
      var allEdgeIds = [];
      allNodeIds = node2.findAllSubNodes(allNodeIds);
      allEdgeIds = this.findAllSubEdges(allEdgeIds);
      for (var i = 0; i < allEdgeIds.length; i++) {
        for (var j = 0; j < allNodeIds.length; j++) {
          if (allNodeIds[j] == this.parentGraph.getEdgetoId(allEdgeIds[i]).getNodeFrom() || allNodeIds[j] == this.parentGraph.getEdgetoId(allEdgeIds[i]).getNodeTo()) {
            return {
              edge: allEdgeIds[i],
              node: allNodeIds[j]
            };
          }
        }
      }
      return {
        edge: 0,
        node: 0
      };
    };

    this.findAllSubNodes = function(array) {
      var subNodes = this.subNodes;
      array.push(this.getId());
      if (subNodes.length != 0) {
        for (var i = 0; i < subNodes.length; i++) {
          array = this.parentGraph.getNodetoId(subNodes[i]).findAllSubNodes(array);
        }
      }
      return array;
    };

    this.findAllSubEdges = function(array) {
      var subNodes = this.subNodes;
      var edges = this.edges;
      if (subNodes.length != 0) {
        for (var i = 0; i < subNodes.length; i++) {
          array = this.parentGraph.getNodetoId(subNodes[i]).findAllSubEdges(array);
        }
      } else {
        for (var i = 0; i < edges.length; i++) {
          array.push(edges[i]);
        }
      }
      return array;
    };

    this.setSubGraphBounds = function() {
      if (this.subNodes.length == 0) {
        return;
      }
      var node = this;
      var tmpNode;
      var rightDist;
      var downDist;
      node.subDistRight = 0;
      node.subDistLeft = 30000;
      node.subDistDown = 0;
      node.subDistUp = 30000;
      for (var i = 0; i < node.subNodes.length; i++) {
        tmpNode = this.parentGraph.getNodetoId(node.subNodes[i]);
        rightDist = tmpNode.getxPos() - node.getxPos();
        downDist = tmpNode.getyPos() - node.getyPos();
        if (rightDist > node.subDistRight) {
          node.subDistRight = rightDist;
        }
        if (rightDist < node.subDistLeft) {
          node.subDistLeft = rightDist;
        }
        if (downDist > node.subDistDown) {
          node.subDistDown = downDist;
        }
        if (downDist < node.subDistUp) {
          node.subDistUp = downDist;
        }

      }
      node.subDistRight += this.getxPos();
      node.subDistLeft += this.getxPos();
      node.subDistDown += this.getyPos();
      node.subDistUp += this.getyPos();
    };

    this.getSubGraphBounds = function() {
      var bounds = new Object();
      var scaling = getScaling();

      if (scaling < 1) {
        scaling = (1 / scaling);
      }

      bounds.left = this.subDistLeft - this.size * scaling;
      bounds.right = this.subDistRight + this.size * scaling;
      bounds.up = this.subDistUp - this.size * scaling;
      bounds.down = this.subDistDown + this.size * scaling;
      return bounds;
    };

    this.moveNodesForSubGraph = function() {
      if (this.subNodes.length == 0) {
        return;
      }
      var scaling = getScaling();

      if (scaling < 1) {
        scaling = (1 / scaling);
      }
      var topNode = this;
      var tmpNode;
      var bounds = this.getSubGraphBounds();
      for (var i = 0; i < this.parentGraph.nodes.length; i++) {
        if (this.parentGraph.nodes[i].getVisible() && this.parentGraph.nodes[i].getId() != topNode.getId()) {
          if ((this.parentGraph.nodes[i].container.x > bounds.left && this.parentGraph.nodes[i].container.x < bounds.right) || (this.parentGraph.nodes[i].container.y > bounds.up && this.parentGraph.nodes[i].container.y < bounds.down)) {
            for (var j = 0; j < topNode.subNodes.length; j++) {
              tmpNode = this.parentGraph.getNodetoId(topNode.subNodes[j]);
              var dx = this.parentGraph.nodes[i].container.x - tmpNode.container.x;
              var dy = this.parentGraph.nodes[i].container.y - tmpNode.container.y;

              if ((Math.abs(dx) < tmpNode.size * scaling) && (Math.abs(dy) < tmpNode.size * scaling)) {
                var sideDist = [this.parentGraph.nodes[i].container.y - bounds.up, bounds.right - this.parentGraph.nodes[i].container.x, bounds.down - this.parentGraph.nodes[i].container.y, this.parentGraph.nodes[i].container.x - bounds.left];
                var index = 0;
                for (var m = 0; m < sideDist.length; m++) {
                  if (sideDist[m] < sideDist[index]) {
                    index = m;
                  }
                }
                var xPos = this.parentGraph.nodes[i].container.x;
                var yPos = this.parentGraph.nodes[i].container.y;
                switch (index) {
                  case 0: //up
                    yPos = bounds.up - 0.5 * tmpNode.size * scaling;
                    break;
                  case 1: //right
                    xPos = bounds.right + 0.5 * tmpNode.size * scaling;
                    break;
                  case 2: //down
                    yPos = bounds.down + 0.5 * tmpNode.size * scaling;
                    break;
                  case 3: //left
                    xPos = bounds.left - 0.5 * tmpNode.size * scaling;
                    break;
                  default:

                }
                this.parentGraph.nodes[i].moveTo(xPos, yPos, index);
              }
            }
          }
        }
      }
    };

    this.moveTo = function(xPos, yPos, direction) {
      var node = this;
      var scaling = getScaling();

      if (scaling < 1) {
        scaling = (1 / scaling);
      }
      for (var i = 0; i < this.parentGraph.nodes.length; i++) {
        var dx = this.parentGraph.nodes[i].container.x - xPos;
        var dy = this.parentGraph.nodes[i].container.y - yPos;

        if ((Math.abs(dx) < this.parentGraph.nodes[i].size * scaling) && (Math.abs(dy) < this.parentGraph.nodes[i].size * scaling) && (this.parentGraph.nodes[i].getId() != node.getId()) && this.parentGraph.nodes[i].getVisible()) {
          switch (direction) {
            case 0: //up
              node.moveTo(xPos, yPos - this.parentGraph.nodes[i].size * scaling, direction);
              break;
            case 1: //right
              node.moveTo(xPos + this.parentGraph.nodes[i].size * scaling, yPos, direction);
              break;
            case 2: //down
              node.moveTo(xPos, yPos + this.parentGraph.nodes[i].size * scaling, direction);
              break;
            case 3: //left
              node.moveTo(xPos - this.parentGraph.nodes[i].size * scaling, yPos, direction);
              break;
            default:

          }
          return;
        }
      }
      this.container.x = xPos;
      this.setxPos(xPos);
      this.container.y = yPos;
      this.setyPos(yPos);
      this.onScreen();
      this.moveConnectors(this.parentGraph.getArea());
    };

    //console.log("Neuer Knoten erzeugt mit Id:" + this.id);

    /** Schnittstellen: View mit als Funktion zum logischen Knoten hinzugefuegt */
    // View Knoten

    //Test Array Container Arrays
    var containerArray = new Array();

    /** getScaling() returns the value scaling, which is a percentage based factor to scale down containers based on a 1080p screen.*/
    var getScaling = function() {
      scaling = (screen.width / MTLG.getOptions().width);
      return scaling;
    }

    //scaling gibt den Faktor der Skalierung in Bezug zu einem 1080p Bildschirm an. Dieser Wert kann in game.config.js geändert werden.
    // Voraussetzung zur korrekten Funktionalität ist der Fullscreen Modus.
    var scaling = getScaling();

    /**
     * This function has to be called to create a visual node to the corresponding logic node.
     * In other words this function draws the node onto the stage.
     * @function addVisualNode
     * @memberof MTLG.utils.complexGraph.Node#
     * @param {number} mouseX The x position of the node
     * @param {number} mouseY The y position of the node
     * @param {object} area The area this node is drawn in.
     * @return {number} ID of this node
     **/
    this.addVisualNode = function(mouseX, mouseY, area) {
      //test 13.11
      var Knoten = this.container;
      var node = this;
      var size = this.size;
      //** addVisualNode fügt einen graphischen Knoten hinzu und benötigt die Mauskoordinaten.*/
      //

      /* Check before drawing for visibility
       * if false, return without drawing
       */
      if (this.visible != true) {
        return 0;
      }

      this.container.id = this.getId();

      //Erstellung des Knotens

      Knoten.x = mouseX;
      Knoten.y = mouseY;
      Knoten.regX = Knoten.regY = (size / 2);

      //Save first position in logical node
      //var movingNode = this.parentGraph.getNodetoId(this.id);
      var movingNode = this.parentGraph.getNodetoId(this.id);
      movingNode.setxPos(mouseX);
      movingNode.setyPos(mouseY);

      var targetrotation;

      var rotate = function(index) {
        if (index === 0) {
          targetrotation = 90;
          if (Knoten.rotation < 90) {
            createjs.Ticker.setFPS(60);
            createjs.Ticker.addEventListener("tick", uprot);
          } else {
            createjs.Ticker.setFPS(60);
            createjs.Ticker.addEventListener("tick", downrot);
          }
        } else if (index === 1) {
          targetrotation = (-90);
          if (Knoten.rotation === 180) {
            Knoten.rotation = (-180);
          }
          if (Knoten.rotation < (-90)) {
            createjs.Ticker.setFPS(60);
            createjs.Ticker.addEventListener("tick", uprot);
          } else {
            createjs.Ticker.setFPS(60);
            createjs.Ticker.addEventListener("tick", downrot);
          }
        } else if (index === 2) {
          targetrotation = 180;
          if (Knoten.rotation === (-90)) {
            Knoten.rotation = 270;
          }
          if (Knoten.rotation < 180) {
            createjs.Ticker.setFPS(60);
            createjs.Ticker.addEventListener("tick", uprot);
          } else {
            createjs.Ticker.setFPS(60);
            createjs.Ticker.addEventListener("tick", downrot);
          }
        } else if (index === 3) {
          targetrotation = 0;
          if (Knoten.rotation < 0) {
            createjs.Ticker.setFPS(60);
            createjs.Ticker.addEventListener("tick", uprot);
          } else {
            createjs.Ticker.setFPS(60);
            createjs.Ticker.addEventListener("tick", downrot);
          }
        }
      }

      function uprot() {
        if (Knoten.rotation != targetrotation) {
          Knoten.rotation = (Knoten.rotation + 3);
          Knoten.rotation = (Knoten.rotation + 3);
          Knoten.rotation = (Knoten.rotation + 3);
          Knoten.rotation = (Knoten.rotation + 3);
          Knoten.rotation = (Knoten.rotation + 3);
          Knoten.rotation = (Knoten.rotation + 3);
          node.moveConnectors(parentGraph.getArea());
          //theArea.update();
        }
        if (Knoten.rotation === targetrotation) {
          createjs.Ticker.removeEventListener("tick", uprot);
          normalize();
        }
      }

      function downrot() {
        if (Knoten.rotation != targetrotation) {
          Knoten.rotation = (Knoten.rotation - 3);
          Knoten.rotation = (Knoten.rotation - 3);
          Knoten.rotation = (Knoten.rotation - 3);
          Knoten.rotation = (Knoten.rotation - 3);
          Knoten.rotation = (Knoten.rotation - 3);
          Knoten.rotation = (Knoten.rotation - 3);
          node.moveConnectors(parentGraph.getArea());
          //theArea.update();
        }
        if (Knoten.rotation === targetrotation) {
          createjs.Ticker.removeEventListener("tick", downrot);
          normalize();
        }
      }

      function normalize() {
        /*  if (knoten.rotation === 270){
          knoten.rotation = (-90);
        }
        if (knoten.rotation === (-180)){
          knoten.rotation = 180;
        }
        if (knoten.rotation === 360){
          knoten.rotation = 0;
        }
        if (knoten.rotation === 450){
          knoten.rotation = 90;
        }
        if (knoten.rotation === (-90)){
          knoten.rotation = 90;
        }
        */
        if (Knoten.rotation < (-90)) {
          Knoten.rotation = (Knoten.rotation + 360)
        }
        if (Knoten.rotation >= (270)) {
          Knoten.rotation = (Knoten.rotation - 360)
        }
      }

      //Rotation
      var Align = function() {
        var KnotenMitteX = (Knoten.x);
        var KnotenMitteY = (Knoten.y);
        var areaCanvasBounds;
        if(parentGraph.getArea().getBounds() != null){
          areaCanvasBounds = {x: parentGraph.getArea().getBounds().width, y: parentGraph.getArea().getBounds().height};
        }else{
          areaCanvasBounds = {x:MTLG.getStage().canvas.width, y:MTLG.getStage().canvas.height};
          console.log("Given Area does not have bounds! Using stage bounds instead.");
        }
        var leftdist = KnotenMitteX;
        var rightdist = (areaCanvasBounds.x - KnotenMitteX);
        var updist = KnotenMitteY;
        var downdist = (areaCanvasBounds.y - KnotenMitteY);
        var arr = [leftdist, rightdist, updist, downdist];

        //  console.log("left " + leftdist + " right " + rightdist + " up " + updist + " down " + downdist);
        //  console.log("knoten.x " + Knoten.x + " knoten.y " + Knoten.y  + " width " + areaCanvasBounds.x + " height " + areaCanvasBounds.y);


        var index = 0;
        var value = arr[0];
        for (var i = 1; i < arr.length; i++) {
          if (arr[i] < value) {
            value = arr[i];
            index = i;
          }
        }
        rotate(index);

        node.onScreen();

        if (!(node.subNodes.length == 0) && !node.deleted) {
          console.log("node.deleted" + node.deleted);
          console.log("subNodes " + node.subNodes);
          var tmpNode;
          for (var i = 0; i < node.subNodes.length; i++) {
            console.log("HIER KOMME ICH REIN");
            tmpNode = parentGraph.getNodetoId(node.subNodes[i]);
            tmpNode.container.x += node.dx;
            tmpNode.container.y += node.dy;
            tmpNode.setxPos(tmpNode.container.x);
            tmpNode.setyPos(tmpNode.container.y);
            //console.log("x " + tmpNode.container.x);
            tmpNode.onScreen();
          }
          node.setSubGraphBounds();
        }
        node.dx = 0;
        node.dy = 0;

        node.moveConnectors(parentGraph.getArea());
      }

      /** adds red garbagecans for each side*/
      var warnung = MTLG.utils.gfx.getShape();
      warnung.graphics.beginFill('red').drawRoundRect(0, 0, 180, 140, 20);
      warnung.alpha = 0.5;
      var warnung1 = warnung.clone(true);
      var warnung2 = warnung.clone(true);
      var warnung3 = warnung.clone(true);

      /** is the garbagecan already red? */
      var alreadyred = false;
      var alreadyred1 = false;
      var alreadyred2 = false;
      var alreadyred3 = false;


      var eventTrigger = 1;

      var NodeMove = function(evt) {
        if (node.ready === false) {
          return 0;
        }
        if (eventTrigger === 1) {
          var coord = area.globalToLocal(evt.stageX, evt.stageY);

          node.dx += coord.x - Knoten.x;
          node.dy += coord.y - Knoten.y;


          Knoten.x = coord.x;
          Knoten.y = coord.y;
          var areaMouseX = Knoten.x;
          var areaMouseY = Knoten.y;

          //Save new position in logical Node for Redraw after loading
          var movingNode = this.parentGraph.getNodetoId(this.id);
          movingNode.setxPos(Knoten.x);
          movingNode.setyPos(Knoten.y);

          node.moveConnectors(parentGraph.getArea());
        }
      };

      var listener = this.container.on('pressmove', NodeMove.bind(this));

      this.container.on('pressup', Align);



      var Umriss = MTLG.utils.gfx.getShape().set({
        Target: "target"
      });

      Umriss.graphics.beginStroke(nodeTextColour).beginFill(nodeBackgroundColour).drawRect(0, 0, size, size);
      this.container.addChild(Umriss);

      /** Lupe */
      var Lupe = new createjs.Bitmap('img/magnifying-glass.png');
      let testID = this.getId();
      Lupe.draw;
      Lupe.scaleX = 0.020;
      Lupe.scaleY = 0.020;
      this.container.addChild(Lupe);
      Lupe.x = 74;
      Lupe.y = 0;
      Lupe.addEventListener('click', function() {
        // hiding the small node due to staggered node turning and otherwise visible small node
        this.parentGraph.getNodetoId(testID).container.removeChild(Umriss);
        this.parentGraph.getNodetoId(testID).container.removeChild(foldout1);
        this.parentGraph.getNodetoId(testID).container.removeChild(foldout2);
        this.parentGraph.getNodetoId(testID).container.removeChild(Lupe);
        this.parentGraph.getNodetoId(testID).container.removeChild(nedge);
        this.parentGraph.getNodetoId(testID).container.removeChild(NameChange);
        //this.parentGraph.getNodetoId(testID).container.removeChild(NodeName);

        /**
         * the bigger version of the visualnode
         * @param size the size of the node gets expanded 4 times.
         */
        var Umrissb = MTLG.utils.gfx.getShape().set({
          Target: "target"
        });

        node.size = node.size * 4;

        Umrissb.graphics.beginStroke(nodeTextColour).beginFill(nodeBackgroundColour).drawRect(0, 0, node.size, node.size);
        this.parentGraph.getNodetoId(testID).container.addChild(Umrissb);
        this.parentGraph.getNodetoId(testID).container.regX = node.size / 2;
        this.parentGraph.getNodetoId(testID).container.regY = node.size / 2;

        //Zoom function retained for the larger node
        if (node.subNodes.length > 0) {
          var Zoomb = new createjs.Bitmap('img/icons6.png');
          Zoomb.addEventListener('mousedown', function() {
            node.ready = true;
            this.parentGraph.removeNode(node.getId());
            area.removeChild(node.container);
          });
        } else if (node.subNodes.length == 0) {
          var Zoomb = new createjs.Bitmap('img/icons6grau.png');
        }
        Zoomb.draw;
        Zoomb.scaleX = 0.06;
        Zoomb.scaleY = 0.06;
        Zoomb.x = 0;
        Zoomb.y = 320;

        var NameChangeb = new createjs.Bitmap('img/NameChange.png');
        NameChangeb.draw;
        NameChangeb.scaleX = 0.060;
        NameChangeb.scaleY = 0.060;
        NameChangeb.x = 320;
        NameChangeb.y = 320;
        NameChangeb.addEventListener('mousedown', function(){
        })





        // Detailed view for the larger node
        // does a linewrap after 350 units beginning at unit 50 so the text is symmetrical
        var Namebig = this.parentGraph.getNodetoId(testID).getName();
        var NodeNameb = MTLG.utils.gfx.getText(Namebig, "32px Arial", nodeTextColour);
        NodeNameb.x = 50;
        NodeNameb.y = 100;
        NodeNameb.textBaseline = "alphabetic";
        NodeNameb.draw;
        NodeNameb.set({
          lineWidth: 350
        });
        this.parentGraph.getNodetoId(testID).container.addChild(NodeNameb);
        // if the given Text inside the node exceeds the amount of node space a second page is
        // added and a button in order to switch between pages is created at the bottom of the big node
        var stringKeyb = Namebig;
        var stringKeybi = Namebig;
        if (stringKeyb.length >= 150) {
          var additionalStringb = stringKeybi.slice(150 - 300);
          var shortStringb = stringKeyb.slice(0, 149);
          stringKeyb = shortStringb.concat("..>");

        }
        var counterpage = 0;
        NodeNameb.text = stringKeyb;
        if (stringKeyb.length >= 150) {
          var Textnextpage = new createjs.Bitmap('img/book.png');
          Textnextpage.draw;
          Textnextpage.scaleX = 0.1;
          Textnextpage.scaleY = 0.1;
          this.parentGraph.getNodetoId(testID).container.addChild(Textnextpage);
          Textnextpage.x = 160;
          Textnextpage.y = 320;
          Textnextpage.addEventListener('mousedown', function() {
            if (counterpage == 0) {
              NodeNameb.text = additionalStringb;
              counterpage = 1;
            } else {
              counterpage = 0;
              NodeNameb.text = stringKeyb;
            }
          });
        }



        // Adding edges to the larger node yet still using the same ID so the nodes retain their original edges
        var nedgeb = new createjs.Bitmap('img/Kreis.png');
        let startID = this.parentGraph.getNodetoId(testID).getId();
        nedgeb.id = startID;
        nedgeb.scaleX = 0.060;
        nedgeb.scaleY = 0.060;
        this.parentGraph.getNodetoId(testID).container.addChild(nedgeb);
        nedgeb.x = 0;
        nedgeb.y = 0;




        var that = this;
        var edgeColour = edgeColour;
        nedgeb.addEventListener("mousedown", function() {
          eventTrigger = 0;
          //console.log("trigger 0");

          //nedge.parent.off('pressmove', listener);
          //var nodeA = this.parentGraph.getNodestoGraph()[(Knoten.id-1)];
          var nodeA = this.parentGraph.getNodetoId(startID);
          //console.log("id", this.parentGraph.getNodestoGraph()[(Knoten.id-1)]);
          console.log("nodeA ", nodeA, "startID " + startID);
          var listenerId = MTLG.getStage().addEventListener("pressup", function() {
            eventTrigger = 1;
            MTLG.getStage().removeEventListener("pressup",listenerId);
          })

          var edgeColour = edgeColour;
          var that2 = that;
          initialPress("mousedown", nodeA, nedgeb,this.parentGraph);
          console.log("HIER KOMME ICH AUCH RAUS");
        }.bind(this));


        // redrawing the smaller node and removing the bigger versions of everything
        // and resurfacing the smaller ones
        var Lupeslim = new createjs.Bitmap('img/magnifying-glass-slim.png');
        Lupeslim.draw;
        Lupeslim.scaleX = 0.06;
        Lupeslim.scaleY = 0.06;
        this.parentGraph.getNodetoId(testID).container.addChild(Lupeslim);
        Lupeslim.x = 320;
        Lupeslim.y = 0;
        Lupeslim.addEventListener('mousedown', function() {

          node.size = node.size / 4;

          this.parentGraph.getNodetoId(testID).container.removeChild(Umrissb);
          this.parentGraph.getNodetoId(testID).container.removeChild(Zoomb);
          this.parentGraph.getNodetoId(testID).container.removeChild(Lupeslim);
          this.parentGraph.getNodetoId(testID).container.removeChild(nedgeb);
          this.parentGraph.getNodetoId(testID).container.removeChild(Textnextpage);
          this.parentGraph.getNodetoId(testID).container.removeChild(NodeNameb);
          this.parentGraph.getNodetoId(testID).container.removeChild(NameChangeb);
          Umriss.graphics.beginStroke(nodeTextColour).beginFill(nodeBackgroundColour).drawRect(0, 0, node.size, node.size);
          // resizing for rotation and movement purposes.
          this.parentGraph.getNodetoId(testID).container.regX = node.size / 2;
          this.parentGraph.getNodetoId(testID).container.regY = node.size / 2;

          this.parentGraph.getNodetoId(testID).container.addChild(Umriss);
          this.parentGraph.getNodetoId(testID).container.addChild(Lupe);
          this.parentGraph.getNodetoId(testID).container.addChild(nedge);
          this.parentGraph.getNodetoId(testID).container.addChild(NodeName);

          for (var i = 0; i < node.sides.length; i++) {
            for (var j = 0; j < node.sides[i].length; j++) {
              node.container.setChildIndex(node.sides[i][j].view, node.container.children.length - 1);
            }
          }
          node.onScreen();
          node.moveConnectors(parentGraph.getArea());

        }.bind(this));


        for (var i = 0; i < node.sides.length; i++) {
          for (var j = 0; j < node.sides[i].length; j++) {
            node.container.setChildIndex(node.sides[i][j].view, node.container.children.length - 1);
          }
        }
        node.onScreen();
        node.moveConnectors(parentGraph.getArea());
      }.bind(this));

      /** Folding out a top node */
      node.subNodes.length;
      if (node.subNodes.length > 0) {
        var foldout1 = new createjs.Bitmap('img/icons6.png');
        foldout1.draw;
        foldout1.scaleX = 0.020;
        foldout1.scaleY = 0.020;
        foldout1.x = 0;
        foldout1.y = 74;
        foldout1.addEventListener('mousedown', function() {
          node.ready = true;
          this.parentGraph.removeNode(node.getId());
          area.removeChild(node.container);
        });
      } else if (node.subNodes.length == 0) {
        var foldout2 = new createjs.Bitmap('img/icons6grau.png');
        foldout2.draw;
        foldout2.scaleX = 0.020;
        foldout2.scaleY = 0.020;
        foldout2.x = 0;
        foldout2.y = 74;
      }

      /** change name */
      var NameChange = new createjs.Bitmap('img/NameChange.png');
      NameChange.draw;
      NameChange.scaleX = 0.020;
      NameChange.scaleY = 0.020;
      NameChange.x = 74;
      NameChange.y = 74;


      /** Kanten hinzufuegen */
      var nedge = new createjs.Bitmap('img/Kreis.png');
      //Test für Übergabe des Startknotens bei Ziehen von Kanten
      let startID = this.getId();
      nedge.id = startID;
      nedge.scaleX = 0.020;
      nedge.scaleY = 0.020;
      this.container.addChild(nedge);
      nedge.x = 0;
      nedge.y = 0;


      nedge.addEventListener("mousedown", function() {
        eventTrigger = 0;
        //console.log("trigger 0");

        //nedge.parent.off('pressmove', listener);
        //var nodeA = this.parentGraph.getNodestoGraph()[(Knoten.id-1)];
        var nodeA = this.parentGraph.getNodetoId(startID);
        //console.log("id", this.parentGraph.getNodestoGraph()[(Knoten.id-1)]);
        console.log("nodeA ", nodeA, "startID " + startID);
        var listenerId2 = MTLG.getStage().addEventListener("pressup", function() {
          eventTrigger = 1;
          MTLG.getStage().removeEventListener("pressup",listenerId2);
        })

        initialPress("mousedown", nodeA, nedge,this.parentGraph);
        console.log("HIER KOMME ICH AUCH RAUS");
      }.bind(this));

      /** Name */
      var NodeName = MTLG.utils.gfx.getText(this.getName(), "18px Arial", nodeTextColour);
      NodeName.name = "NodeName";
      NodeName.x = 10;
      NodeName.y = 50;
      NodeName.textBaseline = "alphabetic";
      NodeName.draw;
      var self = this;
      Knoten.addChild(NodeName);
      var NodeNameIndex = this.container.getChildIndex(NodeName);
      //console.log(NodeNameIndex);

      //Cut off too long name
      var stringKey = this.getName();
      if (stringKey.length >= 8) {
        var shortString = stringKey.slice(0, 7);
        stringKey = shortString.concat("...");
      }
      NodeName.text = stringKey;

      // change name
      NameChange.addEventListener('mousedown', function() {
      });





      /** sorts the four side arrays */
      this.sortConnectorArrays = function() {
        for (var i = 0; i < this.sides.length; i++) {
          this.sides[i] = sortByAngle(this.sides[i]);
        }
      };

      /** sets all connector on its calculated position */
      this.setConnectorPosition = function() {
        for (var i = 0; i < this.sides.length; i++) {
          switch (i) {
            case 0:
              for (var j = 0; j < this.sides[i].length; j++) {
                this.sides[i][j].view.x = this.size * (j + 1) / (this.sides[i].length + 1);
                this.sides[i][j].view.y = 0;
              }
              break;
            case 1:
              for (var j = 0; j < this.sides[i].length; j++) {
                this.sides[i][j].view.x = this.size;
                this.sides[i][j].view.y = this.size * (j + 1) / (this.sides[i].length + 1);
              }
              break;
            case 2:
              for (var j = 0; j < this.sides[i].length; j++) {
                this.sides[i][j].view.x = this.size - this.size * (j + 1) / (this.sides[i].length + 1);
                this.sides[i][j].view.y = this.size;
              }
              break;
            case 3:
              for (var j = 0; j < this.sides[i].length; j++) {
                this.sides[i][j].view.x = 0;
                this.sides[i][j].view.y = this.size - this.size * (j + 1) / (this.sides[i].length + 1);
              }
              break;
            default:

          }
        }
      }

      /** updates the views of all edges of the node
       * @param area: area to draw in*/
      this.updateEdgeViews = function(area) {
        var pt1; //position of the first connector
        var pt2; //position of the second connecotr
        for (var i = 0; i < this.sides.length; i++) {
          for (var j = 0; j < this.sides[i].length; j++) {
            if (this.sides[i][j].edge.getVisible()) {
              this.sides[i][j].edge.deleteEdgeView();
              pt1 = this.sides[i][j].node.container.localToLocal(this.sides[i][j].view.x, this.sides[i][j].view.y, area);
              pt2 = this.sides[i][j].partner.node.container.localToLocal(this.sides[i][j].partner.view.x, this.sides[i][j].partner.view.y, area);
              if (this.sides[i][j].node.id == this.sides[i][j].edge.nodeFrom) {
                this.sides[i][j].edge.drawEdgeView(area, pt1.x, pt1.y, pt2.x, pt2.y,edgeColour);
              } else {
                this.sides[i][j].edge.drawEdgeView(area, pt2.x, pt2.y, pt1.x, pt1.y,edgeColour);
              }
            }

          }
        }

      };

      /**updates the positon of all connecors and their partners and draws the new edges
       * @param area: area to draw in*/
      this.moveConnectors = function(area) {
        this.updateAllConnectorAngles(area);
        this.sortAllConnectors(area);
        this.setConnectorPosition();
        this.updateEdgeViews(area);
      };

      /** calculates the new angle of every connector of this node
       * @param area: area to draw in */
      this.updateAllConnectorAngles = function(area) {
        for (var i = 0; i < this.sides.length; i++) {
          for (var j = 0; j < this.sides[i].length; j++) {
            var angle1 = this.sides[i][j].angle;
            setConnectorAngle(area, this.sides[i][j]);
            this.sides[i][j].partner.node.changeConnectorsArray(area, this.sides[i][j].partner);
            this.sides[i][j].partner.node.setConnectorPosition();
            this.sides[i][j].partner.node.updateEdgeViews(area);
          }
        }
      };
      /** cahnge the array the connector is in if it is necessary
       * @param area: area to draw in
       * @param connector: the connector */
      this.changeConnectorsArray = function(area, connector) {
        var index = nodeSideIndex(area, connector);
        index = rotatedNodeSideIndex(connector, index.i1);
        var id = connector.id;
        for (var i = 0; i < this.sides.length; i++) {
          for (var j = 0; j < this.sides[i].length; j++) {
            if (this.sides[i][j].id == id) {
              if (i != index) {
                popAt(this.sides[i], j);
                this.sides[index].push(connector);
              }
              this.sides[index] = sortByAngle(this.sides[index]);
            }
          }
        }
      };

      /** sorts all connectors of the node
       * @param area: area to draw in */
      this.sortAllConnectors = function(area) {
        var tmpSides = [];
        var tmpIndex;
        for (var k = 0; k < 4; k++) {
          tmpSides[k] = [];
        }
        for (var i = 0; i < this.sides.length; i++) {
          for (var j = 0; j < this.sides[i].length; j++) {
            var index = nodeSideIndex(area, this.sides[i][j]);
            index = rotatedNodeSideIndex(this.sides[i][j], index.i1);
            tmpSides[index].push(this.sides[i][j]);
          }
        }
        for (var l = 0; l < tmpSides.length; l++) {
          tmpSides[l] = sortByAngle(tmpSides[l]);
          this.sides[l] = tmpSides[l];
        }
      }


      this.onScreen = function() {
        var scaling = getScaling();

        if (scaling < 1) {
          scaling = (1 / scaling);
        }
        if (this.container.x < this.size * scaling / 2) {
          this.container.x = this.size * scaling / 2;
        }
        if (this.container.x > areaBounds.x - this.size * scaling / 2) {
          this.container.x = areaBounds.x - this.size * scaling / 2;
        }
        if (this.container.y < this.size * scaling / 2) {
          this.container.y = this.size * scaling / 2;
        }
        if (this.container.y > areaBounds.y - this.size * scaling / 2) {
          this.container.y = areaBounds.y - this.size * scaling / 2;
        }
      }

      this.align = function() {
        Align();
      };

      Align();

      if (scaling < 1) {
        scaling = (1 / scaling);
      };
      this.container.scaleX = scaling;
      this.container.scaleY = scaling;

      area.addChild(this.container);
    }
  }

  /*Globale Variable, die allen Elementen (Kanten und Knoten)
   * eine eindeutige ID zuweist.
   * default-Wert: 1
   */
  var elementCounter = 1;

  /**
   * Constructor of graph structure. This is also the function that is returned in complexGraph.
   * @class Graph
   * @memberof MTLG.utils.complexGraph
   */
  function Graph(area) {
    var edgeColour = 'black';
    var nodeBackgroundColour = 'grey';
    var nodeTextColour = 'red';

    /**
     * @function getColors
     * @memberof MTLG.utils.complexGraph.Graph#
     * @return {object} Object with the color-properties <code>edge</code>, <code>nodeBg</code> and <code>nodeTx</code>.
     */
    this.getColors = function(){return{edge:edgeColour, nodeBg:nodeBackgroundColour, nodeTx:nodeTextColour}};
    /**
     * @function setColors
     * @memberof MTLG.utils.complexGraph.Graph#
     * @param {string} edgeColour The color to draw the edges in. Any valid value for the CSS color attribute is acceptable (ex. "#F00", "red", or "#FF0000").
     * @param {string} nodeBackgroundColour Backgroundcolor of nodes.
     * @param {string} nodeTextColour Textcolor of text in nodes.
     */
    this.setColors = function(a,b,c){
      edgeColour = a;
      nodeBackgroundColour = b;
      nodeTextColour = c;
    }

    //The graph keeps track of the area it is drawn on
    this.drawingArea = area;

    /**
     * This function returns the current drawing area
     * @returns {createjs.Container} The current drawing area.
     */
    this.getArea = function(){return this.drawingArea;};

    // All nodes will be stored in this array
    this.nodes = new Array();

    // All edges will be stored in this array
    this.edges = new Array();

    /**
     * This function returns the graph itself
     * @returns {Graph} the graph itself
     */
    this.getGraph = function() {
      return this;
    };

    /**
     * This function returns the array of nodes associated to the graph
     * @returns {Array} array of nodes
     */
    this.getNodestoGraph = function() {
      return this.nodes;
    }

    /**
     * This function returns the array of edges associated to the graph
     * * @returns {Array} array of edges
     */
    this.getEdgestoGraph = function() {
      return this.edges;
    }

    /**
     * This function adds a logical node to the graph. This does not yet draw the node.
     * @see MTLG.utils.complexGraph.Node
     * @function addNode
     * @memberof MTLG.utils.complexGraph.Graph#
     * @param {boolean} ready status of the new node. Optional
     * @returns {Array} array of nodes
     **/
    this.addNode = function(ready) {
      var toAddNode = new Node(ready,this, edgeColour, nodeBackgroundColour, nodeTextColour);
      this.nodes.push(toAddNode);

      return toAddNode;
    };

    /**
     * This function adds a logical edge to the graph
     * @function addEdge
     * @memberof MTLG.utils.complexGraph.Graph#
     * @param {MTLG.utils.complexGraph.Edge} toAddEdge the edge to be added
     * @returns {Array} array of Edges
     */
    this.addEdge = function(toAddEdge) {
      // temporary store the Id of the added edge
      var toAddEdgeId = toAddEdge.getId();

      // temporary store the Id of the nodes who have been connected
      var ersterKnotenId = toAddEdge.nodeTo;
      var zweiterKnotenId = toAddEdge.nodeFrom;

      // temporary store the nodes belonging to the Ids above
      var ersterKnoten = this.getNodetoId(ersterKnotenId);
      var zweiterKnoten = this.getNodetoId(zweiterKnotenId);

      // save the edge Id in both nodes
      //ersterKnoten.edges.push(toAddEdgeId);
      //zweiterKnoten.edges.push(toAddEdgeId);

      // add edge to the graph
      this.edges.push(toAddEdge);
      return this.edges;
    }

    /**
     * This function returns the length of the node-array
     * @returns {number} the length of the nodes-array associated to the graph
     */
    this.getLength = function() {
      return this.nodes.length;
    };

    /**
     * This function adds a logical subgraph to the graph
     * @param toAddNodes: array of node Ids of the added nodes
     */
    this.addSubGraph = function(toAddNodes, area) {
      // generate new TopNode
      if (toAddNodes.length < 2) {
        console.log("NOT ENOUGH NODES ! YOU NEED AT LEAST 2");
        return 0;
      }
      var newTopNode = this.addNode(false);
      newTopNode.setName("TopNode " + newTopNode.getId());
      // draw the visual Topnode at the position of the first node in toAddNodes
      var xPos = 0; //this.getNodetoId(toAddNodes[0]).container.x;
      var yPos = 0; //this.getNodetoId(toAddNodes[0]).container.y;



      // save the Ids of the added nodes in the subNodes array

      for (var i = 0; i < toAddNodes.length; i++) {
        newTopNode.subNodes.push(toAddNodes[i]);
      }
      newTopNode.addVisualNode(xPos, yPos, area);
      var graph = this;
      var tmpNode;
      var tmpEdge;
      var tmpConnector;
      var edgeClone = false;

      // remove all views of nodes of toAddNodes
      for (var i = 0; i < toAddNodes.length; i++) {
        tmpNode = graph.getNodetoId(toAddNodes[i]);
        xPos += tmpNode.container.x;
        yPos += tmpNode.container.y;
        tmpNode.setVisible(false);
        //tmpNode.container.removeAllChildren();
        area.removeChild(tmpNode.container);


        for (var j = 0; j < tmpNode.sides.length; j++) {
          for (var k = 0; k < tmpNode.sides[j].length; k++) {
            tmpConnector = tmpNode.sides[j][k];
            tmpEdge = tmpConnector.edge;
            if (!isInArray(toAddNodes, tmpEdge.nodeFrom) || !isInArray(toAddNodes, tmpEdge.nodeTo)) {
              if (tmpEdge.nodeFrom == toAddNodes[i]) {
                for (var m = 0; m < newTopNode.edges.length; m++) {
                  //is there an edge to the same node as tmpEdge? (edgeClone)
                  if (tmpEdge.nodeTo == graph.getEdgetoId(newTopNode.edges[m]).nodeFrom || tmpEdge.nodeTo == graph.getEdgetoId(newTopNode.edges[m]).nodeTo) {
                    // console.log("edgeClone " + tmpEdge.id + " in node " + tmpEdge.nodeTo);
                    edgeClone = true;
                  }
                }
                if (tmpEdge.getRepresentFrom() == 0) {
                  tmpEdge.setRepresentFrom(tmpEdge.nodeFrom);
                }
                tmpEdge.nodeFrom = newTopNode.getId();
              } else {
                for (var m = 0; m < newTopNode.edges.length; m++) {
                  //is there an edge to the same node as tmpEdge? (edgeClone)
                  if (tmpEdge.nodeFrom == graph.getEdgetoId(newTopNode.edges[m]).nodeFrom || tmpEdge.nodeFrom == graph.getEdgetoId(newTopNode.edges[m]).nodeTo) {
                    //console.log("edgeClone " + tmpEdge.id + " in node " + tmpEdge.nodeFrom);
                    edgeClone = true;
                  }
                }
                if (tmpEdge.getRepresentTo() == 0) {
                  tmpEdge.setRepresentTo(tmpEdge.nodeTo);
                }
                tmpEdge.nodeTo = newTopNode.getId();
              }
              tmpConnector.node = newTopNode;
              if (edgeClone) { // hide the edgeClone and its connector and its partner in invisibleConnectors
                newTopNode.invisibleConnectors.push(tmpConnector);

                newTopNode.invisibleEdgesSC.push(tmpEdge.getId());
                //console.log("hiddenEdge " + tmpEdge.id);
                tmpEdge.setVisible(false);
                tmpEdge.deleteEdgeView();
                for (var m = 0; m < tmpConnector.partner.node.sides.length; m++) {
                  for (var n = 0; n < tmpConnector.partner.node.sides[m].length; n++) {
                    if (tmpConnector.partner.id == tmpConnector.partner.node.sides[m][n].id) {
                      popAt(tmpConnector.partner.node.sides[m], n);
                      tmpConnector.partner.node.container.removeChild(tmpConnector.partner.view);
                      tmpConnector.partner.node.invisibleConnectors.push(tmpConnector.partner);

                      tmpConnector.partner.node.invisibleEdgesSC.push(tmpEdge.getId());
                    }
                  }
                }
                edgeClone = false;
              } else { //add view of the edge and its connector
                newTopNode.sides[j].push(tmpConnector);
                newTopNode.container.addChild(tmpConnector.view);
              }
              newTopNode.edges.push(tmpEdge.getId());
            } else { //edge between two nodes in the SubGraph
              tmpEdge.setVisible(false);
              tmpEdge.deleteEdgeView();
            }


          }
        }
        //console.log(this.nodes);
      }
      xPos = xPos / toAddNodes.length;
      yPos = yPos / toAddNodes.length;
      newTopNode.addVisualNode(xPos, yPos, area);
      for (var i = 0; i < newTopNode.sides.length; i++) {
        for (var j = 0; j < newTopNode.sides[i].length; j++) {
          newTopNode.container.setChildIndex(newTopNode.sides[i][j].view, newTopNode.container.children.length - 1);
        }
      }
      newTopNode.moveConnectors(area);
      newTopNode.setSubGraphBounds();
      console.log("Neuer Oberknoten hinzugefuegt");
      newTopNode.ready = true;

      return newTopNode;
    };



    /**
     * This function removes a logical node from the graph
     * @param removedNode: the Id of the removed node
     */
    this.removeNode = function(removedNodeId) {
      var removedNode = this.getNodetoId(removedNodeId);
      if (!removedNode.ready) {
        console.log("removedNode.ready" + removedNode.ready);
        console.log("NODE ISNT READY !!! YOU CANT REMOVE");
        return 0;

      }
      console.log("zu löscheder Knoten");
      console.log(removedNode);


      // check if removed node is a TopNode
      if (removedNode.subNodes.length > 0) {
        // removed node is a TopNode, process in the following function
        this.removeTopNodeExtension(removedNode);
      } else if (removedNode.subNodes.length == 0) {
        // removed node has no subgraph
        for (var i = 0, len = removedNode.edges.length; i < len; i++) {
          // delete all edges associated to the node
          this.removeEdge(removedNode.edges[0]);
        }
      }
      var indexOfRemoved = this.nodes.indexOf(removedNode);
      // remove node from the node array
      this.nodes.splice(indexOfRemoved, 1);

      // if removed node was the last existing node there ist nothing to save
      if (this.getNodestoGraph().length == 0) {
        somethingtoSave = 0;
      }
    };

    /**
     * This function will be activated if a logical TopNode was removed.
     * There ist no need to call this function, to remove a node
     * call in any case the function removeNode()
     * @param removed: the removed TopNode
     */
    this.removeTopNodeExtension = function(removed) {
      var graph = this;
      var tmpNode;
      var tmpNodeDelete
      var tmpConnector;
      var tmpEdge;
      var changedEdges = [];
      removed.moveNodesForSubGraph();
      for (var i = 0; i < removed.subNodes.length; i++) {
        tmpNode = graph.getNodetoId(removed.subNodes[i]);
        tmpNode.setVisible(true);

        for (var j = 0; j < tmpNode.sides.length; j++) {
          for (var k = 0; k < tmpNode.sides[j].length; k++) {
            tmpConnector = tmpNode.sides[j][k];
            tmpEdge = tmpConnector.edge;
            if (!isInArray(changedEdges, tmpEdge.getId())) {
              if (tmpConnector.edge.getVisible()) {
                tmpConnector.node = tmpNode;
                tmpNode.container.addChild(tmpConnector.view);
                if (tmpEdge.nodeFrom == removed.getId()) {
                  tmpConnector.edge.nodeFrom = tmpNode.getId();
                  if (tmpEdge.getRepresentFrom() == tmpNode.getId()) {
                    tmpEdge.setRepresentFrom(0);
                  }
                } else {
                  tmpEdge.nodeTo = tmpNode.getId();
                  if (tmpEdge.getRepresentTo() == tmpNode.getId()) {
                    tmpEdge.setRepresentTo(0);
                  }
                }
              } else if (!(isInArray(removed.subNodes, tmpEdge.nodeFrom) && isInArray(removed.subNodes, tmpEdge.nodeTo))) {
                tmpConnector.node = tmpNode;
                if (tmpEdge.nodeFrom == removed.getId()) {
                  tmpConnector.edge.nodeFrom = tmpNode.getId();
                  if (tmpEdge.getRepresentFrom() == tmpNode.getId()) {
                    tmpEdge.setRepresentFrom(0);
                  }
                } else {
                  tmpEdge.nodeTo = tmpNode.getId();
                  if (tmpEdge.getRepresentTo() == tmpNode.getId()) {
                    tmpEdge.setRepresentTo(0);
                  }
                }
                if (!tmpConnector.partner.node.getVisible()) {
                  var highestTopNode = tmpConnector.partner.node.getHighestTopNode();
                  var subNode = tmpConnector.partner.node;

                  if (tmpConnector.partner.node.getId() == tmpEdge.getNodeFrom()) {
                    if (tmpEdge.getRepresentFrom() == 0) {
                      tmpEdge.setRepresentFrom(tmpConnector.partner.node.getId());
                    }
                  } else {
                    if (tmpEdge.getRepresentTo() == 0) {
                      tmpEdge.setRepresentTo(tmpConnector.partner.node.getId());
                    }
                  }

                  while (subNode.getId() != highestTopNode.getId()) {
                    var foundSth = false;
                    for (var p = 0; p < subNode.invisibleConnectors.length; p++) {
                      if (subNode.invisibleConnectors[p].id == tmpConnector.partner.id) {
                        popAt(subNode.invisibleConnectors, p);
                        subNode.sides[0].push(tmpConnector.partner);
                        foundSth = true;
                        break;
                      }
                    }
                    if (!foundSth && !isInArray(subNode.edges, tmpEdge.getId())) {
                      subNode.edges.push(tmpEdge.getId());
                      subNode.sides[0].push(tmpConnector.partner);
                      foundSth = true;
                    }
                    subNode = subNode.getTopNode();
                  }
                  if (tmpConnector.partner.node.getId() == tmpEdge.nodeFrom) {
                    tmpEdge.nodeFrom = highestTopNode.getId();
                  } else {
                    tmpEdge.nodeTo = highestTopNode.getId();
                  }
                  tmpConnector.partner.node = highestTopNode;
                  highestTopNode.edges.push(tmpEdge.getId());

                }
                if (isThereSimilarEdgeVisible(tmpNode, tmpEdge) || isThereSimilarTopNodeEdge(tmpNode, tmpEdge, removed)) {
                  tmpNode.invisibleConnectors.push(tmpConnector);

                  tmpNode.invisibleEdgesSC.push(tmpEdge.getId());
                  popAt(tmpNode.sides[j], k);
                  k--;
                  tmpNode.container.removeChild(tmpConnector.view);

                } else {
                  tmpNode.container.addChild(tmpConnector.view);
                  tmpEdge.setVisible(true);
                  tmpConnector.partner.node.sides[0].push(tmpConnector.partner);
                  tmpConnector.partner.node.container.addChild(tmpConnector.partner.view);
                  for (var m = 0; m < tmpConnector.partner.node.invisibleConnectors.length; m++) {
                    if (tmpConnector.partner.id == tmpConnector.partner.node.invisibleConnectors[m].id) {
                      popAt(tmpConnector.partner.node.invisibleConnectors, m);
                    }
                  }
                }
              } else {
                tmpEdge.setVisible(true);
              }
              changedEdges.push(tmpEdge.getId());
            }
          }
        }
        //tmpNode.addVisualNode(tmpNode.getxPos(), tmpNode.getyPos(), theArea);
        //tmpNode.addVisualNode(500,500, theArea);
        //tmpNode.moveConnectors(theArea);
        this.drawingArea.addChild(tmpNode.container);

      }
      for (var j = 0; j < 5; j++) {
        for (var i = 0; i < removed.subNodes.length; i++) {
          graph.getNodetoId(removed.subNodes[i]).align();
        }
      }


      //console.log(this.nodes);
      //console.log(this.edges);

    };

    var isThereSimilarEdgeVisible = function(node, edge) {
      var tmpEdge;
      for (var i = 0; i < node.sides.length; i++) {
        for (var j = 0; j < node.sides[i].length; j++) {
          tmpEdge = node.sides[i][j].edge;
          if (tmpEdge.getId() != edge.getId() && tmpEdge.getVisible()) {
            if ((tmpEdge.nodeFrom == edge.nodeFrom && tmpEdge.nodeTo == edge.nodeTo) || (tmpEdge.nodeFrom == edge.nodeTo && tmpEdge.nodeTo == edge.nodeFrom)) {
              return true;
            }
          }
        }
      }
      return false;
    };
    var isThereSimilarTopNodeEdge = function(node, edge, topNode) {
      //return false;
      var tmpEdge;
      for (var i = 0; i < node.sides.length; i++) {
        for (var j = 0; j < node.sides[i].length; j++) {
          tmpEdge = node.sides[i][j].edge;
          if (tmpEdge.getId() != edge.getId() && tmpEdge.getVisible()) {
            if (tmpEdge.getNodeFrom() == topNode.getId()) {
              if ((tmpEdge.nodeTo == edge.nodeTo) || (tmpEdge.nodeTo == edge.nodeFrom)) {
                return true;
              }
            } else if (tmpEdge.getNodeTo() == topNode.getId()) {
              if ((tmpEdge.nodeFrom == edge.nodeTo) || (tmpEdge.nodeFrom == edge.nodeFrom)) {
                return true;
              }
            }
          }
        }
      }
      return false;
    };

    /** Funktion soll eine beliebige Kanten im Graphen loeschen
     * Als Eingabe erhaelt diese die ID der zu loeschenden Kanten
     */
    /**
     * This function removes al logical edge from the graph
     * @param removedEdgeId: the Id of the removed edge
     */
    this.removeEdge = function(removedEdgeId) {
      var removedEdge = this.getEdgetoId(removedEdgeId);
      var node;

      // check if the edge represents an edge of a subnode

      // delete the edge in the subnode

      if (removedEdge.getRepresentFrom() != 0) {
        var node = this.getNodetoId(removedEdge.getNodeFrom());
        node.deleteEdgeInAllSubNodes(removedEdge);
      }
      if (removedEdge.getRepresentTo() != 0) {
        var node = this.getNodetoId(removedEdge.getNodeTo());
        node.deleteEdgeInAllSubNodes(removedEdge);
      }

      // delete the edge in the edge array
      var indexOfRemoved = this.edges.indexOf(removedEdge);
      this.edges.splice(indexOfRemoved, 1);

      /**Loeschen im eingehenden Knoten */
      var nodeTo = this.getNodetoId(removedEdge.getNodeTo());
      nodeTo.removeEdgeAndConnectors(removedEdge.getId());
      var nodeFrom = this.getNodetoId(removedEdge.getNodeFrom());
      nodeFrom.removeEdgeAndConnectors(removedEdge.getId());


      removedEdge.deleteEdgeView(this.drawingArea);

      var similar = nodeFrom.findSimilarEdge(nodeTo);
      if (similar.edge != 0) {
        var connectors = [2];
        var simNodes = [2];
        var highestTopNodes = [2];
        simNodes[0] = this.getNodetoId(similar.node);
        simNodes[1] = 0;
        for (var i = 0; i < simNodes[0].sides.length; i++) {
          for (var j = 0; j < simNodes[0].sides[i].length; j++) {
            if (similar.edge == simNodes[0].sides[i][j].edge.getId()) {
              connectors[0] = simNodes[0].sides[i][j];
              break;
            }
          }
        }
        for (var i = 0; i < simNodes[0].invisibleConnectors.length; i++) {
          if (similar.edge == simNodes[0].invisibleConnectors[i].edge.getId()) {
            connectors[0] = simNodes[0].invisibleConnectors[i];
            break;
          }
        }
        connectors[1] = connectors[0].partner;
        for (var i = 0; i < simNodes.length; i++) {
          simNodes[i] = connectors[i].node;
          highestTopNodes[i] = simNodes[i].getHighestTopNode();
          if (simNodes[i].getId() == connectors[i].edge.getNodeFrom()) {
            if (connectors[i].edge.getRepresentFrom() == 0) {
              connectors[i].edge.setRepresentFrom(simNodes[i].getId());
            }
          } else {
            if (connectors[i].edge.getRepresentTo() == 0) {
              connectors[i].edge.setRepresentTo(simNodes[i].getId());
            }
          }

          var last = false;
          var from = false;
          while (!last) {
            if (simNodes[i].getId() == connectors[i].edge.getNodeFrom()) from = true;
            var foundSth = false;
            for (var j = 0; j < simNodes[i].invisibleConnectors.length; j++) {
              if (simNodes[i].invisibleConnectors[j].id == connectors[i].id) {
                popAt(simNodes[i].invisibleConnectors, j);
                simNodes[i].sides[0].push(connectors[i]);
                simNodes[i].container.addChild(connectors[i].view);
                foundSth = true;
                j--;

              }
            }
            if (!foundSth && !isInArray(simNodes[i].edges, connectors[i].edge.getId())) {
              simNodes[i].edges.push(connectors[i].edge.getId());
              simNodes[i].sides[0].push(connectors[i]);
              simNodes[i].container.addChild(connectors[i].view);
              foundSth = true;
            }
            if (simNodes[i].getId() == highestTopNodes[i].getId()) {
              connectors[i].node = highestTopNodes[i];
              if (from) {
                connectors[i].edge.setNodeFrom(highestTopNodes[i].getId());
              } else {
                connectors[i].edge.setNodeTo(highestTopNodes[i].getId());
              }
              highestTopNodes[i].moveConnectors(this.drawingArea);
              connectors[i].edge.setVisible(true);
              last = true;
            } else {
              simNodes[i] = simNodes[i].getTopNode();
            }
          }

        }
      }


      //console.log(this.parentGraph.nodes);

    };

    /**
     * This function returns the node to the passed Id
     * @param searchId: Id of the node you search for
     */
    this.getNodetoId = function(searchId) {
      for (var i = 0, len = this.nodes.length; i < len; i++) {
        if (this.nodes[i].getId() == searchId) {
          return this.nodes[i];
        }
      }
      return 0;
    };

    /**
     * This function returns the edge to the passed Id
     * @param searchId: Id of the edge you search for
     */
    this.getEdgetoId = function(searchId) {
      for (var i = 0, len = this.edges.length; i < len; i++) {
        if (this.edges[i].getId() == searchId) {
          return this.edges[i];
        }
      }
    };

    /**
     * This function redraws the actual Graph after reloading it from strorage
     */
    this.redrawGraph = function(area) {

      // Redraw nodes
      for (var i = 0, len = this.nodes.length; i < len; i++) {
        this.nodes[i].addVisualNode(this.nodes[i].getxPos(), this.nodes[i].getyPos(), area);
        if (this.nodes[i].visible == false) {
          this.nodes[i].setVisible(true);
          this.nodes[i].addVisualNode(this.nodes[i].getxPos(), this.nodes[i].getyPos(), area);
          this.nodes[i].setVisible(false);
          area.removeChild(this.nodes[i].container);
        }
      }

      // Redraw edges
      for (var j = 0, len = this.edges.length; j < len; j++) {
        // Get nodes belonging to the edge
        //var nodeTo = this.parentGraph.getNodetoId(this.edges[j].getNodeTo());
        //var nodeFrom = this.parentGraph.getNodetoId(this.edges[j].getNodeFrom());

        // Call connector function
        //connectorAddEdge(area,nodeFrom,nodeTo);
        var edge = this.parentGraph.edges[j];
        //console.log(edge);
        if (edge.getVisible()) {
          addConnectorsForEdge(area, edge,false,parentGraph);
        } else {
          var tmpNode = this.parentGraph.getNodetoId(edge.getNodeFrom());
          if (!isInArray(tmpNode.invisibleEdgesSC, edge.getId())) {
            addConnectorsForEdge(area, edge,false,parentGraph);
          } else {
            addConnectorsForEdge(area, edge, true,parentGraph);
          }

        }

      }

      for (var m = 0; m < this.nodes.length; m++) {
        this.nodes[m].moveConnectors(area);
      }
    }

  }
  var isInArray = function(array, object) {
    for (var i = 0; i < array.length; i++) {
      if (array[i] == object) return true;
    }
    return false;
  };


  /** counts the total connectors */
  var connectorCounter = 1;

  /** draws an edge between two node1 and node2
   *@param area: area to draw in
   *connector attributes:
   * @param view: the shape, which is the visual component of the connecor
   * @param partner: the connector from the other side of the edge
   * @param angle: the angle between the two connected nodes
   * @param edge: the eddge between the partners
   * @param node: the node in which the connector is located
   * @param id: every connector has a unique ID */
  var connectorAddEdge = function(area, node1, node2,parentGraph) {
    var edge = new Edge(parentGraph);
    edge.nodeFrom = node1.id;
    edge.nodeTo = node2.id;
    node1.edges.push(edge.getId());
    node2.edges.push(edge.getId());

    parentGraph.addEdge(edge);

    return addConnectorsForEdge(area, edge,false,parentGraph);
  };
  var addConnectorsForEdge = function(area, edge, invisible,parentGraph) {
    var node1 = parentGraph.getNodetoId(edge.getNodeFrom());
    var node2 = parentGraph.getNodetoId(edge.getNodeTo());
    var cons = new Object();
    cons.i = new Object();
    cons.ii = new Object();
    cons.i.partner = cons.ii;
    cons.ii.partner = cons.i;
    cons.i.node = node1;
    cons.ii.node = node2;
    cons.i.id = connectorCounter;
    connectorCounter++;
    cons.ii.id = connectorCounter;
    connectorCounter++;



    cons.i.edge = edge;
    cons.ii.edge = edge;
    var size = 7;
    cons.i.view = MTLG.utils.gfx.getShape();
    cons.i.view.graphics.beginStroke(parentGraph.getColors().edge).drawCircle(0, 0, size);
    cons.ii.view = MTLG.utils.gfx.getShape();
    cons.ii.view.graphics.beginStroke(parentGraph.getColors().edge).drawCircle(0, 0, size);

    if (invisible === true) {
      node1.invisibleConnectors.push(cons.i);
      node2.invisibleConnectors.push(cons.ii);
    } else {
      var index = nodeSideIndex(area, cons.i);
      placeConnectorInSideArray(cons.i, index.i1);
      placeConnectorInSideArray(cons.ii, index.i2);

      node1.sortConnectorArrays();
      node2.sortConnectorArrays();
      node1.setConnectorPosition();
      node2.setConnectorPosition();
      node1.updateEdgeViews(area);
      node2.updateEdgeViews(area);

      cons.i.view.name = "connector";
      cons.ii.view.name = "connector";
      node1.container.addChild(cons.i.view);
      node2.container.addChild(cons.ii.view);
    }

    if (edge.getRepresentFrom() != 0) {
      var subNode = parentGraph.getNodetoId(edge.getRepresentFrom());
      while (subNode.getId() != node1.getId()) {
        subNode.sides[0].push(cons.i);
        //subNode.container.addChild(cons.i.view);
        subNode = subNode.getTopNode();
      }
    }
    if (edge.getRepresentTo() != 0) {
      var subNode = parentGraph.getNodetoId(edge.getRepresentTo());
      while (subNode.getId() != node2.getId()) {
        subNode.sides[0].push(cons.ii);
        //subNode.container.addChild(cons.ii.view);
        subNode = subNode.getTopNode();
      }
    }



    return cons;
  }

  /** adds the connector to the fitting sidesarray
   *@param connector: connector to be added
   *@param index:  */
  var placeConnectorInSideArray = function(connector, index) {
    var i = rotatedNodeSideIndex(connector, index);
    connector.node.sides[i].push(connector);
  }

  /** calculates the side index for a rotated node with the general side index
   *@param connector: connector to be added
   *@param index: general side index
   *@returns index of the of the correct sidesarray
   */
  var rotatedNodeSideIndex = function(connector, index) {
    var offset;
    var rot = connector.node.container.rotation;
    switch (rot) {
      case 0:
        offset = 0;
        break;
      case 90:
        offset = 1;
        break;
      case 180:
        offset = 2;
        break;
      case -90:
        offset = 3;
        break;
      default:
        offset = 0;

    }
    var i = (index - offset) % 4;
    if (i < 0) i += 4;
    return i;
  }

  /** sorts the connectorArray by the size of the angles
   * @param array : unsorted array
   * @returns sorted array  */
  var sortByAngle = function(array) {
    var tmpArray = [];
    var tmp;
    tmp = 0;

    var big = false;
    for (var i = 0; i < array.length; i++) {
      if (array[i].angle > 315) big = true;
      for (var j = 0; j < tmpArray.length; j++) {
        if (big && tmpArray[j].angle <= 315) {
          tmp = j;
          break;
        }
        if (array[i].angle < tmpArray[j].angle && !(!big && tmpArray[j].angle > 315)) {
          tmp = j;
          break;
        }
      }
      shiftAt(tmpArray, array[i], j);
      big = false;
    }
    return tmpArray;
  }

  /** this function adds the object to the array at the position index and shifts the remaining array by one
   *@param array: the array
   *@param object: object to be added
   *@param index: index where the object is going to be added */
  var shiftAt = function(array, object, index) {
    for (var i = array.length; i > index; i--) {
      array[i] = array[i - 1];
    }
    array[index] = object;
  }

  /** removes the object, which is saved at the position index, from the array
   * @param array: the array
   * @param index: the index of the object which is going to be removed  */
  var popAt = function(array, index) {
    if (index < array.length) {
      for (var i = index; i < array.length - 1; i++) {
        array[i] = array[i + 1];
      }
      array.pop();
    }
  };

  /** nodeSideIndex calculate the index of the side, in which the connector shall be ordered at
   * this function does not consider the rotation of the node
   * @param area: area to draw in
   * @param connector1
   * @returns general sideindex (0: up, 1:right, 2: down, 3: left)*/

  var nodeSideIndex = function(area, connector1) {
    var index;
    setConnectorAngle(area, connector1);

    if (connector1.angle <= 45 || 315 < connector1.angle) {
      //node1 is on the left of node2
      index = {
        i1: 1,
        i2: 3
      }; //conneccotr1 right connector2 left
      return index;
    } else if (connector1.angle <= 135) {
      //node1 is above node2
      index = {
        i1: 2,
        i2: 0
      }; //connector1 below connector2 above
      return index;
    } else if (connector1.angle <= 225) {
      //node1 is on the right of node2
      index = {
        i1: 3,
        i2: 1
      }; //connector1 left  connector2 right
      return index;
    } else {
      //node1 is below node2
      index = {
        i1: 0,
        i2: 2
      }; //connector1 above connector2 below
      return index;
    }
  }

  /** calculates the angle between the two nodes for the connector and its partner
   * the angle is between 0 an 360 degrees
   *@param area: area to draw in
   *@param connector1  */
  var setConnectorAngle = function(area, connector1) {
    var connector2 = connector1.partner;
    var node1 = connector1.node;
    var node2 = connector2.node;

    var node1pos = getTheMiddle(node1);
    var node2pos = getTheMiddle(node2);
    var n1pos = node1.container.localToLocal(node1pos.x, node1pos.y, area);
    var n2pos = node2.container.localToLocal(node2pos.x, node2pos.y, area);

    var offset = 0;
    if (n1pos.x > n2pos.x) {
      offset = 180;
    }
    var dx = n2pos.x - n1pos.x;
    var dy = n2pos.y - n1pos.y;
    if (n1pos.y == n2pos.y) {
      if (n1pos.x < n2pos.x) {
        connector1.angle = 0;
      }
      if (n1pos.x > n2pos.x) {
        connector1.angle = 180;
      }

    } else {
      connector1.angle = (Math.atan(dy / dx) / Math.PI) * 180 + offset;
    }
    while (connector1.angle < 0 || connector1.angle >= 360) {
      if (connector1.angle < 0) {
        connector1.angle += 360;
      } else {
        connector1.angle -= 360;
      }
    }
    connector2.angle = connector1.angle + 180;
    while (connector2.angle < 0 || connector2.angle >= 360) {
      if (connector2.angle < 0) {
        connector2.angle += 360;
      } else {
        connector2.angle -= 360;
      }
    }
  }

  /** returns the position of the top left corner of a node
   * @param node
   * @returns coordinates of the top left corner of the node*/
  var getTopLeftCorner = function(node) {
    var pos;
    if (node.container.rotation == 0) {
      pos = {
        x: 0,
        y: 0
      };
      return pos;
    } else if (node.container.rotation == 90) {
      pos = {
        x: 0,
        y: node.size
      };
      return pos;
    } else if (node.container.rotation == 180) {
      pos = {
        x: node.size,
        y: node.size
      };
      return pos;
    } else {
      pos = {
        x: node.size,
        y: 0
      };
      return pos;
    }

  }

  /**@param node
   * @returns coordinates of the  middle of the node */
  var getTheMiddle = function(node) {
    var pos = {
      x: node.size / 2,
      y: node.size / 2
    };
    return pos;
  }
  //test
  var connectorTest = function(area) {
    var node1 = this.parentGraph.addNode();
    node1.addVisualNode(200, 100, area);
    var node2 = this.parentGraph.addNode();
    node2.addVisualNode(200, 600, area);
    var node3 = this.parentGraph.addNode();
    node3.addVisualNode(500, 100, area);
    var node4 = this.parentGraph.addNode();
    node4.addVisualNode(500, 600, area);
    var node5 = this.parentGraph.addNode();
    node5.addVisualNode(700, 350, area);

    connectorAddEdge(area, node1, node2);
    connectorAddEdge(area, node2, node3);
    connectorAddEdge(area, node1, node3);
    connectorAddEdge(area, node4, node1);
    connectorAddEdge(area, node4, node2);
    connectorAddEdge(area, node4, node3);
    connectorAddEdge(area, node5, node1);
    connectorAddEdge(area, node5, node2);
    connectorAddEdge(area, node5, node3);
    connectorAddEdge(area, node5, node4);

    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < node1.sides[i].length; j++) {
        console.log("AngleNode1: " + node1.sides[i][j].angle);
      }
    }
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < node2.sides[i].length; j++) {
        console.log("AngleNode2: " + node2.sides[i][j].angle);
      }
    }
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < node3.sides[i].length; j++) {
        console.log("AngleNode3: " + node3.sides[i][j].angle);
      }
    }
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < node4.sides[i].length; j++) {
        console.log("AngleNode4: " + node4.sides[i][j].angle);
      }
    }



    /*
    console.log("Knoten1 TEST: ");
    console.log(node1);
    console.log("Knoten2 TEST: ");
    console.log(node2);
    console.log("Knoten3 TEST: ");
    console.log(node3);
    console.log("Knoten4 TEST: ");
    console.log(node4);*/
    var nodes = new Object();
    nodes.node1 = node1;
    nodes.node2 = node2;
    nodes.node3 = node3;
    nodes.node4 = node4;
    nodes.node5 = node5;
    return nodes;
  }

  //var ticker1 = new createjs.Ticker();
  var getScaling = function() {
    scaling = (screen.width / MTLG.getOptions().width);
    return scaling;
  }

  //scaling gibt den Faktor der Skalierung in Bezug zu einem 1080p Bildschirm an. Dieser Wert kann in game.config.js geändert werden.
  // Voraussetzung zur korrekten Funktionalität ist der Fullscreen Modus.
  // TODO: Should also work in non-fullscreen mode

  function initialPress(evt, idx, shape,parentGraph) {
    idx.connectionCounter++;
    idx.ready = false;
    var nedge = shape;
    var connection = null;
    var node1 = idx;
    var node2;
    var nedge = shape;

    var second = false;

    var coordx = nedge.x;
    var coordy = nedge.y;

    connection = MTLG.utils.gfx.getShape().set({
      x: coordx,
      y: coordy,

      mouseEnabled: false,
      //checkt wo das KLickevent ausgeführt wird aber das funktioniert nicht @daniel
      graphics: new createjs.Graphics().s("green").dc(0, 0, 50)
    });


    node1.container.addChild(connection);
    nedge.addEventListener('pressmove', drawLine);
    nedge.addEventListener('pressup', endDraw);




    function drawLine(evt) {
      var posMouse = MTLG.getStage().localToLocal(evt.stageX, evt.stageY, node1.container);
      connection.graphics.clear()
        .s("red")
        .mt(0, 0).lt(posMouse.x - connection.x, posMouse.y - connection.y);
    }

    function endDraw(evt) {
      var edgeColour = parentGraph.getColors().edge;
      var theArea;

      theArea = parentGraph.getArea();
      var scaling = getScaling()<1?1/getScaling():getScaling();
      var first = true;
      if (second === true) {
        first = false;
      }
      nedge.removeEventListener('pressmove', drawLine);
      nedge.removeEventListener('pressup', endDraw);
      var target, targets = MTLG.getStage().getObjectsUnderPoint(evt.stageX, evt.stageY);
      var coord = theArea.globalToLocal(evt.stageX, evt.stageY);
      var tmpNode;
      for (var i = 0; i < targets.length; i++) {
        if (targets[i].Target == "target") {
          tmpNode = parentGraph.getNodetoId(targets[i].parent.id);
          if(!tmpNode){
            continue;
          }
          if (tmpNode.getxPos() - (tmpNode.size * scaling / 2) <= coord.x && coord.x <= tmpNode.getxPos() + (tmpNode.size *scaling / 2) && tmpNode.getyPos() - (tmpNode.size * scaling / 2) <= coord.y && coord.y <= tmpNode.getyPos() + (tmpNode.size * scaling / 2)) { //TODO is this if necessary? Great for finding bugs though
            target = targets[i];
            break;
          } else {
            tmpNode = 0;
          }

        }
      }

      var endNode = tmpNode;
      node2 = endNode;
      if (target != null) {
        if (node1.getId() != node2.getId()&& node1.subNodes.length == 0 && node2.subNodes.length ==0) {
          connectorAddEdge(parentGraph.getArea(), node1, node2,parentGraph);
        }
        node1.container.removeChild(connection);
        node1.connectionCounter--;
        if (node1.connectionCounter == 0) {
          node1.ready = true;
        }
      } else if (first) {
        var timer = 120;
        var tempcirc = MTLG.utils.gfx.getShape();
        tempcirc.graphics.setStrokeStyle(5).ls(["darkblue","blue", "teal",

          "darkturquoise"], [0,0.33,0.66,1], (evt.stageX-20), (evt.stageY-20),

          (evt.stageX+20), (evt.stageY+20)).beginFill("rgba(0,0,0,0.01)").drawCircle

        (evt.stageX, evt.stageY, 20);
        MTLG.getStage().addChild(tempcirc);
        tempcirc.x = evt.stageX;
        tempcirc.y = evt.stageY;
        tempcirc.regX = (evt.stageX);
        tempcirc.regY = (evt.stageY);
        tempcirc.rotation = (Math.random() * 360);


        createjs.Ticker.setFPS(60);
        createjs.Ticker.addEventListener("tick", temprot);

        function temprot() {
          tempcirc.rotation = tempcirc.rotation +4;
          tempcirc.rotation = tempcirc.rotation +4;
          MTLG.getStage().update();
        }
        tempcirc.addEventListener('mousedown', function(evt) {
          tempcirc.graphics.clear();
          createjs.Ticker.removeEventListener("tick", temprot);
          createjs.Ticker.removeEventListener("tick", handleTick);
          MTLG.getStage().removeChild(tempcirc);
        });
        tempcirc.addEventListener('pressmove', drawLine);
        tempcirc.addEventListener('pressup', function(evt) {
          second = true;
          endDraw(evt);
          MTLG.getStage().removeChild(tempcirc);
        });
        createjs.Ticker.addEventListener("tick", handleTick);

        function handleTick(event) {
          console.log("tick");
          timer = (timer - 1);
          if (timer < 0) {
            MTLG.getStage().removeChild(tempcirc);
            createjs.Ticker.removeEventListener("tick", temprot);
            createjs.Ticker.removeEventListener("tick", handleTick);
            node1.container.removeChild(connection);
            node1.connectionCounter--;
            if (node1.connectionCounter == 0) {
              node1.ready = true;
            }
          }
        }
      } else {
        node1.container.removeChild(connection);
        node1.connectionCounter--;
        if (node1.connectionCounter == 0) {
          node1.ready = true;
        }
      }
    }

  }

return Graph;
}());

module.exports = complexGraph;
