/**
 * @namespace voting
 * @memberof MTLG.utils
 * @example
 * 
 * ...
 */

const VotingLogger = require('./logger.js');

var VotingModule = (function () {

    //Enumeration similar Object used to handle state of VotingInstance
    const stateEnum = Object.freeze({ "NEW": 0, "OPENED": 1, "EVALUATING": 2, "CLOSED": 3 });

    const predefinedActions = ["loginToVoting", "confirmResponse", "confirmTrustment", "unconfirmTrustment", "unconfirmResponse", "removeTrustment", "setTrustment", "switchResponseConfirmation", "switchTrustmentConfirmation", "giveResponse", "removeResponse"];

    //Default values of config (have not to be parameters, if someone creates a new VotingInstance)
    const defaultConfig = {
        distributed: false,
        timer: {
            active: false,
            duration: 5
        },
        allowSupervisor: false,
        allowTrustment: false,
        unanimous: false,
        requireConfirmation: false,
        requireResponse: false,
        question: { hint: "" },
        management: {
            isManaged: false,
            manager: null
        },
        debug: false
    }

    //Keys, that must be filled in config
    const globalCompulsoryConfigs = ["question", "choiceType", "response", "estimation"];

    /**
     * returns 
     * @param {Object} config Configs. For parameterts please read wiki
     * @returns {VotingInstance} The VotingsInstance, depending on which choice type is desired  
     */
    const VotingInstanceFactory = function (config) {
        //fill not given parameters with default values
        let keys = Object.keys(defaultConfig);
        keys.map(key => {
            if (isUndefined(config[key])) {
                config[key] = defaultConfig[key];
            }
        });

        if (!config.management.isManaged) {
            config.management.isManaged = false;
        }
        if (!config.management.manager) {
            config.management.manager = null;
        }

        //check, wheter compulsory parameters exist
        globalCompulsoryConfigs.forEach(e => {
            switch (e) {
                case "question":
                    if (!config[e]) {
                        console.log("Missing parameter question.");
                        return null;
                    }
                    if (!config[e].text) {
                        console.log("Missing parameter question.text.");
                        return null;
                    }
                    if (!config[e].hint) {
                        config[e].hint = "";
                    }
                    break;
                case "choiceType":
                    if (!config[e]) {
                        console.log("Missing parameter chioceType.");
                        return null;
                    }
                    break;
                case "response":
                    if (config["choiceType"] == "SingleChoice" || config["choiceType"] == "MultipleChoice") {
                        if (!config[e]) {
                            console.log("Missing parameter response.");
                            return null;
                        }
                        if (!config[e].options) {
                            console.log("Missing parameter response.options.");
                            return null;
                        }
                    }
                    break;
                case "estimation":
                    if (config["choiceType"] == "Estimation") {
                        if (!config[e]) {
                            console.log("Missing parameter estimation.");
                            return null;
                        }
                        if (!config[e].intervalLowerBoundary) {
                            console.log("Missing parameter estimation.intervalLowerBoundary.");
                            return null;
                        }
                        if (!config[e].intervalUpperBoundary) {
                            console.log("Missing parameter estimation.intervalUpperBoundary.");
                            return null;
                        }
                    }
                default:
                    break;
            }
        })

        //returns desired VotingInstance
        switch (config.choiceType) {
            case "MultipleChoice":
                return new MultipleChoice(config);
                break;
            case "SingleChoice":
                return new SingleChoice(config);
                break;
            case "Estimation":
                return new Estimation(config);
                break;
            default:
                console.log("Could not match given choice type to existing ones.");
                return null;
        }

    }

    const VotingSessionFactory = function (config) {
        return new VotingSession(config);
    }

    /**
     * Checks, wheter an Object is undefined or not
     * @param {Object} elem 
     * @returns {Boolean}
     */
    const isUndefined = function (elem) {
        if (elem === undefined) {
            return true;
        }
        return false;
    }

    /**
     * Checks, wheter two given Arrays are equal element by element
     * @param {Array} a Array to compare
     * @param {Array} b Array to compare
     */
    const arraysEqual = function (a, b) {
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length != b.length) return false;

        for (var i = 0; i < a.length; ++i) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    }

    /**
     * Clones a given object
     * @param {Object} orig 
     */
    const cloneObject = function (orig) {
        return Object.assign(Object.create(Object.getPrototypeOf(orig)), orig)
    }

    /**
     * Returns a key that is mapped to a given searchValues
     * @param {Map} map 
     * @param {Object} searchValue 
     */
    const getKeyByValue = function getByValue(map, searchValue) {
        for (let [key, value] of map.entries()) {
            if (value === searchValue)
                return key;
        }
    }

    /**
     * 
     */
    class VotingInstance {
        /**
         * Constructor for a VotingInstance
         * @param {Object} config Configs. For parameters please read wiki
         */
        constructor(config) {
            this._result = null;
            this._supervisor = new Object();
            this._state = stateEnum.NEW;
            this._sharedPlayerInformation = new Array();
            this._playerResponses = new Array();
            this._mapDistributedIds = new Map();
            this._config = {
                distributed: config.distributed,
                timer: config.timer,
                allowSupervisor: config.allowSupervisor,
                unanimous: config.unanimous,
                requireConfirmation: config.requireConfirmation,
                allowTrustment: config.allowTrustment,
                question: config.question,
                response: config.response,
                requireResponse: config.requireResponse,
                management: config.management,
                debug: config.debug
            }

            //set config values, if Estiamtion
            this._config.estimation = null;
            if (this.constructor.name == "Estimation") {
                this._config.estimation = {
                    intervalLowerBoundary: config.estimation.intervalLowerBoundary,
                    intervalUpperBoundary: config.estimation.intervalUpperBoundary
                }
            }

            this._players = new Array();

            this._trustments = new Array();

            this._logs = new Array();

            this._logger = new VotingLogger();

            //define behaviour of logger
            this._logger.on("action", (data) => {
                if (this._config.debug) {
                    //do anything for debug
                    console.log(data.exApiMsg);
                }
                this._logs.push(data);
            });

            this._logger.on("distributedAction", (data) => {
                if (this._config.debug) {
                    //do anything for debug
                    console.log(data.exApiMsg);
                }
                this._logs.push(data);
            });

            this._logger.on("message", (data) => {
                if (this._config.debug) {
                    //do anything for debug
                    console.log(data.msg);
                }
            })

            //register Listeners, if distributed
            if (this._config.distributed) {
                this._listenToDistributedActions();
            }

            //this._logger.logAction("Player 1", "giveResponse", "1");
        }

        get config() {
            return this._config;
        }

        get players() {
            return this._players;
        }

        get result() {
            return this._result;
        }

        get responseOptions() {
            return this._config.response.options;
        }

        /**
         * @param {Player} playerId New supervisor
         */
        set supervisor(playerId) {
            if (this._getPlayerIndex(playerId) != null) {
                this._supervisor = this.players[this._getPlayerIndex(playerId)];
            }
        }

        /**
         * @param {Player[]} obj New array of players
         */
        set players(obj) {
            if (this._state == stateEnum.NEW) {
                for (var i = 0; i < obj.length; i++) {
                    this.addPlayer(getIndexOfGlobalPlayer(obj[i].id));
                }
            }
        }

        /**
         * Registers Listeners in the room, where the instance is created
         */
        _listenToDistributedActions() {
            MTLG.distributedDisplays.actions.setCustomFunction("loginToVoting", function (requester, args) {
                this._logger.logDistributedAction(requester, "loginToVoting");
                var MTLGindex = playerFactory(args[0], args[1]);
                var worked = MTLGindex != null;
                if (worked) {
                    this.addPlayer(MTLGindex);
                    this._mapDistributedIds.set(requester, args[0]);
                }
                this._sendInitializedInformation("loginInformation", requester, worked);
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("confirmResponse", function (requester) {
                this._logger.logDistributedAction(requester, "confirmResponse");
                this.confirmResponse(this._mapDistributedIds.get(requester));
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("confirmTrustment", function (requester) {
                this._logger.logDistributedAction(requester, "confirmTrustment");
                this.confirmTrustment(this._mapDistributedIds.get(requester));
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("unconfirmTrustment", function (requester) {
                this._logger.logDistributedAction(requester, "unconfirmTrustment");
                this.unconfirmTrustment(this._mapDistributedIds.get(requester));
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("unconfirmResponse", function (requester) {
                this._logger.logDistributedAction(requester, "unconfirmResponse");
                this.unconfirmResponse(this._mapDistributedIds.get(requester));
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("removeTrustment", function (requester) {
                this._logger.logDistributedAction(requester, "removeTrustment");
                this.removeTrustment(this._mapDistributedIds.get(requester));
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("setTrustment", function (requester, args) {
                this._logger.logDistributedAction(requester, "setTrustment");
                this.setTrustment(this._mapDistributedIds.get(requester), args[0])
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("switchResponseConfirmation", function (requester) {
                this._logger.logDistributedAction(requester, "switchResponseConfirmation");
                this.switchResponseConfirmation(this._mapDistributedIds.get(requester));
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("switchTrustmentConfirmation", function (requester) {
                this._logger.logDistributedAction(requester, "switchTrustmentConfirmation");
                this.switchTrustmentConfirmation(this._mapDistributedIds.get(requester));
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("giveResponse", function (requester, args) {
                this._logger.logDistributedAction(requester, "giveResponse");
                this.giveResponse(this._mapDistributedIds.get(requester), args[0]);
            }.bind(this));

            MTLG.distributedDisplays.actions.setCustomFunction("removeResponse", function (requester) {
                this._logger.logDistributedAction(requester, "removeResponse");
                this.removeResponse(this._mapDistributedIds.get(requester));
            }.bind(this));
        }

        _delistenToDistributedActions() {
            for (let i = 0; i < predefinedActions.length; i++) {
                MTLG.distributedDisplays.actions.removeCustomFunction(predefinedActions[i]);
            }
            MTLG.utils.inactivity.removeAllListeners();
        }

        /**
         * returns the sharedInformation and also sends, if voting is distributed
         * @param {String} informationType 
         * @param {Object} sendTo either id of receiver or "room", to send to the whole room 
         * @param {Boolean} loginWorked if this is answer of the login-process, this flag is set
         */
        _sendInitializedInformation(informationType, sendTo, loginWorked) {
            let send = {
                players: [...this.players],
                config: this._copyConfigsForSend(),
                state: this._state,
                loginWorked: loginWorked,
                informationType: informationType
            }

            let sendObject = new MTLG.distributedDisplays.SharedObject(send, 0, true);
            sendObject.syncProperties = {
                players: { send: true, receive: false },
                config: { send: true, receive: false },
                state: { send: true, receive: false },
                loginWorked: { send: true, receive: false },
                informationType: { send: true, receive: false }
            }
            sendObject.init();

            if (this._config.distributed) {
                MTLG.distributedDisplays.communication.sendSharedObject(sendTo, sendObject, true, false);
                this._logger.log("Send shared object to " + sendTo);
            }
            return sendObject.createJsObject;

        }

        /**
         * Sends the result to slaves
         */
        _sendResult() {
            let send = cloneObject(this._result);
            send.state = this._state;
            send.informationTye = "evaluationInformation"

            let sendObject = new MTLG.distributedDisplays.SharedObject(send, 0, true);
            sendObject.syncProperties = {
                playerCorrect: { send: true, receive: false },
                responseCounter: { send: true, receive: false },
                supervisorDecided: { send: true, receive: false },
                votingCorrect: { send: true, receive: false },
                unanimous: { send: true, receive: false },
                confirmed: { send: true, receive: false },
                allResponded: { send: true, receive: false },
                decision: { send: true, receive: false },
                informationType: { send: true, receive: false }
            }
            sendObject.init();

            MTLG.distributedDisplays.communication.sendSharedObject("room", sendObject, true, false);
            this._logger.log("Send shared object to " + "room");
        }

        /**
         * Returns shared player information and also sends, if voting is distributed
         * @param {Number} playerIndex 
         */
        _syncSharedPlayerInformation(playerIndex) {
            this._sharedPlayerInformation[playerIndex].createJsObject.response = this._getResponseObject(playerIndex).response;
            this._sharedPlayerInformation[playerIndex].createJsObject.responseConfirmation = this._getResponseObject(playerIndex).responseConfirmation;
            this._sharedPlayerInformation[playerIndex].createJsObject.trustmentConfirmation = this._getResponseObject(playerIndex).trustmentConfirmation;
            this._sharedPlayerInformation[playerIndex].createJsObject.trustment = this._trustments[playerIndex];

            let slaveId = getKeyByValue(this._mapDistributedIds, this._getPlayerId(playerIndex));

            var send = this._sharedPlayerInformation[playerIndex];

            if (this._config.distributed) {
                MTLG.distributedDisplays.communication.sendSharedObject(slaveId, send, true, false);
                this._logger.log("Send shared object to " + slaveId);
            }
            return send.createJsObject;

        }

        /**
         * Returns an object of shared player informtion. Includes all information, that is needed in interaction with the system
         * @param {Number} playerIndex 
         */
        _createSharedPlayerInformation(playerIndex) {
            let res = this._getResponseObject(playerIndex);
            var send = {
                response: res.response,
                responseConfirmation: res.responseConfirmation,
                trustmentConfirmation: res.trustmentConfirmation,
                trustment: this._trustments[playerIndex],
                informationType: "interactionInformation"
            }

            var syncO = new MTLG.distributedDisplays.SharedObject(send, 0, true);
            syncO.syncProperties = {
                response: { send: true, receive: false },
                responseConfirmation: { send: true, receive: false },
                trustmentConfirmation: { send: true, receive: false },
                trustment: { send: true, receive: false },
                informationType: { send: true, receive: false }
            }
            syncO.init();

            return syncO;
        }

        /**
         * Return the object that stores shared player information
         * @param {Number} playerId 
         */
        _getSharedPlayerInformation(playerId) {
            if (null != this._getPlayerIndex(playerId)) {
                return this._sharedPlayerInformation[this._getPlayerIndex(playerId)];
            }
        }

        /**
         * Helping function, that copies configs for the sended voting information
         */
        _copyConfigsForSend() {
            var res = {
                distributed: this._config.distributed,
                timer: this._config.timer,
                allowSupervisor: this._config.allowSupervisor,
                unanimous: this._config.unanimous,
                requireConfirmation: this._config.requireConfirmation,
                allowTrustment: this._config.allowTrustment,
                question: this._config.question,
                response: this._config.response,
                automaticEvaluation: this._config.automaticEvaluation,
                requireResponse: this._config.requireResponse,
                management: {
                    isManaged: this._config.management.isManaged,
                    manager: null
                }
            }
            return res;
        }

        /**
         * Adds given player to VotingInstance
         * @param {Number} newMtlgPlayerIndex Index of player in MTLG.players
         */
        addPlayer(newMtlgPlayerIndex) {
            if (this._state === stateEnum.NEW) {
                this._players.push(MTLG.getPlayer(newMtlgPlayerIndex));
                this._addPlayerInitialize()
                this._logger.log("Player with ID " + this._players[this._players.length - 1].id + " added.");
            }
        }

        /**
         * Adds a player and creates Response objects
         */
        _addPlayerInitialize() {
            if (this.constructor.name == "Estimation") {
                this._playerResponses.push(new EstimationResponse());
            } else {
                this._playerResponses.push(new ChoiceResponse(this._config.response.options.length));
            }

            this._sharedPlayerInformation.push(this._createSharedPlayerInformation(this.players.length - 1));

            if (this._config.allowTrustment) {
                this._trustments.push(null);
            }
        }

        /**
         * adds all MTLG players
         */
        addAllMTLGPlayers() {
            for (let i = 0; i < MTLG.getPlayerNumber(); i++) {
                this.addPlayer(i);
            }
        }

        /**
         * Sets a trustment with the result, that giverId trusts representativeId. 
         * @param {Number} giverId 
         * @param {Number} representativeId 
         */
        setTrustment(giverId, representativeId) {
            if (this._state == stateEnum.OPENED) {
                if (this._config.allowTrustment) {
                    if (!isUndefined(this._getPlayerIndex(giverId)) && !isUndefined(this._getPlayerIndex(representativeId))) {
                        if (this._setTrustmentAllowed(this._getPlayerIndex(giverId), this._getPlayerIndex(representativeId))) {
                            this._trustments[this._getPlayerIndex(giverId)] = this._getPlayerIndex(representativeId);
                        } else if (giverId == representativeId) {
                            this.removeTrustment(giverId);
                        }
                        else {
                            this._logger.log("Circle trustment would occur. Trustment not set.");
                        }
                    }
                    else {
                        this._logger.log("Giver id or representative id is not known.")
                    }

                } else {
                    this._logger.log("VotingInstance has no trust mode.");
                }
            } else {
                this._logger.log("Voting not opened.");
            }
            this._logger.log(this._trustments);
        }

        /**
         * Returns trustment of given Player
         * @param {Number} giverIndex 
         */
        _getTrustment(giverIndex) {
            return this._trustments[giverIndex];
        }

        /**
         * Returns resulting trustment of given player. "Chain trustments" are taken into account.
         * @param {Number} giverIndex 
         */
        _getEndTrustment(giverIndex) {
            if (this._trustments[giverIndex] == null || !this._getResponseObject(giverIndex).trustmentConfirmation) {
                return giverIndex;
            } else {
                return this._getEndTrustment(this._trustments[giverIndex]);
            }
        }

        /**
         * Removes a trustment of a given player
         * @param {Number} giverId 
         */
        removeTrustment(giverId) {
            var playerIndex = this._getPlayerIndex(giverId);
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            this._trustments[playerIndex] = null;
            this._logger.logAction(giverId, "removeTrustment", null);
            return this._syncSharedPlayerInformation(playerIndex);
        }

        /**
         * returns a bool, wheter giver is allowed to trust representative (circle trustments must not occur)
         * @param {Number} giverIndex 
         * @param {Number} representativeIndex 
         */
        _setTrustmentAllowed(giverIndex, representativeIndex) {
            var copyOfTrustments = [...this._trustments];
            copyOfTrustments[giverIndex] = representativeIndex;
            return this._setTrustmentAllowedHelper(giverIndex, giverIndex, copyOfTrustments)
        }

        /**
         * Helps to determine, if trustment is allowed by evaluating wheter circle trustment occurs.
         * @param {Number} currentGiverIndex 
         * @param {Number} originalGiverIndex 
         * @param {Array} copyOfTrustments copy of trustment array
         */
        _setTrustmentAllowedHelper(currentGiverIndex, originalGiverIndex, copyOfTrustments) {
            if (copyOfTrustments[currentGiverIndex] == originalGiverIndex) {
                return false;
            } else if (copyOfTrustments[currentGiverIndex] == null) {
                return true;
            } else {
                return this._setTrustmentAllowedHelper(copyOfTrustments[currentGiverIndex], originalGiverIndex, copyOfTrustments);
            }

        }

        /**
         * Switchs state to next one
         */
        nextState() {
            switch (this._state) {
                case stateEnum.NEW:
                    this._state = stateEnum.OPENED;
                    this._sendInitializedInformation("votingOpened", "room");
                    break;
                case stateEnum.OPENED:
                    this._state = stateEnum.EVALUATING;
                    break;
                case stateEnum.EVALUATING:
                    this._state = stateEnum.CLOSED;
                    break;
                default:
                    this._state = stateEnum.CLOSED;
            }
            this._logger.log("State switched to " + this._state);
        }

        /**
         * Opens voting
         */
        openVoting() {
            if (this._state == stateEnum.NEW) {
                this.nextState();
                this._sendInitializedInformation("votingOpened", "room");
            }
        }

        /**
         * Changes state to given one
         * @param {State} hardState State to change
         */
        _changeStateHard(hardState) {
            if (hardState == stateEnum.NEW || hardState == stateEnum.OPENED || hardState == stateEnum.EVALUATING || hardState == stateEnum.CLOSED) {
                state = hardState;
            }
        }

        /**
         * Confirms a response of a player identified by playerId
         * @param {Number} playerId 
         */
        confirmResponse(playerId) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            this._logger.logAction(playerId, "confirmResponse", null);
            this._playerResponses[this._getPlayerIndex(playerId)].confirmResponse();
            return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
        }

        /**
         * Unconfirms a response of a player identified by playerId
         * @param {Number} playerId 
         */
        unconfirmResponse(playerId) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            this._logger.logAction(playerId, "unconfirmResponse", null);
            this._playerResponses[this._getPlayerIndex(playerId)].unconfirmResponse();
            return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
        }

        /**
         * Switchs confiormation of a player identified by playerId
         * @param {Number} playerId 
         */
        switchResponseConfirmation(playerId) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            var playerIndex = this._getPlayerIndex(playerId);
            if (this._confirmCheck(playerId)) {
                this._playerResponses[playerIndex].switchResponseConfirmation();
                if (this._playerResponses[playerIndex].responseConfirmation) {
                    this._logger.logAction(playerId, "confirmResponse", null);
                } else {
                    this._logger.logAction(playerId, "unconfirmResponse", null);
                }
                return this._syncSharedPlayerInformation(playerIndex);
            }
        }

        /**
         * Confirms a trustment
         * @param {Number} playerId 
         */
        confirmTrustment(playerId) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            this._logger.logAction(playerId, "confirmTrustment", null);
            this._playerResponses[this._getPlayerIndex(playerId)].confirmTrustment();
            return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
        }

        /**
         * Unconfirms trustment
         * @param {Number} playerId 
         */
        unconfirmTrustment(playerId) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            this._logger.logAction(playerId, "unconfirmTrustment", null);
            this._playerResponses[this._getPlayerIndex(playerId)].unconfirmTrustment();
            return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
        }

        /**
         * switchs confirmation of trustment
         * @param {Number} playerId 
         */
        switchTrustmentConfirmation(playerId) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            var playerIndex = this._getPlayerIndex(playerId);
            if (this._confirmCheck(playerId)) {
                this._playerResponses[playerIndex].switchTrustmentConfirmation();
                if (this._playerResponses[playerIndex].trustmentConfirmation) {
                    this._logger.logAction(playerId, "confirmTrustment", null);
                } else {
                    this._logger.logAction(playerId, "unconfirmTrustment", null);
                }
                return this._syncSharedPlayerInformation(playerIndex);
            }
        }


        /**
         * Returns index in this._players of given player identified by playerId
         * @param {Number} playerId 
         */
        _getPlayerIndex(playerId) {
            for (var i = 0; i < this._players.length; i++) {
                if (this._players[i].id == playerId) {
                    return i;
                }
            }
            return null;
        }

        /**
         * Returns player id of a player at given index
         * @param {Number} index 
         */
        _getPlayerId(index) {
            return this._players[index].id;
        }

        /**
         * Returns state of Confirmation of a given player identified by playerId
         * @param {Number} playerIndex identifies player
         * @returns {Boolean} 
         */
        _confirmationGiven(playerIndex) {
            return this._playerResponses[playerIndex].responseConfirmation;
        }

        /**
         * Checks, wheter every player has confirmed
         * @returns {Boolean}
         */
        _allConfirmationsGiven() {
            for (var i = 0; i < this._players.length; i++) {
                let currentConfirmationState = this._playerResponses[i].responseConfirmation;

                if (isUndefined(currentConfirmationState) || currentConfirmationState == false) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Returns response object
         * @param {Number} playerIndex
         * @returns {Object} 
         */
        _getResponseObject(playerIndex) {
            return this._playerResponses[playerIndex];
        }

        getResponse(playerIndex) {
            return this._getResponseObject(playerIndex).response;
        }

        /**
         * Checks, wheter all responses are given
         * @returns {Boolean}
         */
        _allResponsesGiven() {
            for (var i = 0; i < this._players.length; i++) {
                if (!this._responseGiven(i)) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Checks, wheter response of given player was given
         * @param {Number} playerIndex 
         */
        _responseGiven(playerIndex) {
            if (this._config.allowTrustment) {
                playerIndex = this._getEndTrustment(playerIndex);
            }
            let obj = this._getResponseObject(playerIndex).response;
            if (this.constructor.name == "Estimation") {
                if (isUndefined(obj)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                var flag = false;
                for (var i = 0; i < obj.length; i++) {
                    if (obj[i] == true) {
                        flag = true;
                    }
                }
                return flag;
            }
        }

        /**
         * Calculates, how many responeses were given
         * @returns {Number}
         */
        _getNumberOfResponses() {
            if (this._config.requireConfirmation) {
                let counter = 0;
                for (var i = 0; i < this._playerResponses.length; i++) {
                    if (this._playerResponses[i].responseConfirmation && this._responseGiven(i)) {
                        counter++;
                    }
                }
                return counter;
            } else {
                let counter = 0;
                for (var i = 0; i < this._playerResponses.length; i++) {
                    if (this._responseGiven(i)) {
                        counter++;
                    }
                }
                return counter;
            }
        }

        /**
         * Checks, wheter player is allowed to confirm
         * @param {Integer} playerIndex 
         */
        _confirmCheck(playerIndex) {
            if (playerIndex === null) {
                this._logger.log("No such player found.");
                return false;
            }

            if (this._state != stateEnum.OPENED) {
                this._logger.log("Voting not opened.");
                return false;
            }

            return true;
        }

        /**
         * Excecutes check, wheter giveResponse() is allowed 
         * @param {Number} playerIndex
         * @returns {Boolean}
         */
        _giveResponseCheck(playerIndex) {
            if (playerIndex === null) {
                this._logger.log("No such player found.");
                return false;
            }

            if (this._state != stateEnum.OPENED) {
                this._logger.log("Voting not opened.");
                return false;
            }

            if (this._config.requireConfirmation && this._confirmationGiven(playerIndex)) {
                this._logger.log("Player has already confirmed. Voting is blocked.");
                return false;
            }

            return true;
        }

        /**
         * Checks wheter systems allows automativ evaluation
         */
        _checkAutomaticEvaluation() {
            if (!this._config.automaticEvaluation) {
                return false;
            }
            if (!this._allResponsesGiven()) {
                return false;
            }
            if (this._config.requireConfirmation && !this._allConfirmationsGiven()) {
                return false;
            }
            this.evaluate();
            return true;
        }

        /**
         * Excecutes check, wheter evaluate is allowed 
         * @returns {Boolean}
         */
        _evaluateCheckBefore() {
            if (this._state !== stateEnum.OPENED) {
                this._logger.log("Status not OPENED. Evaluation blocked.");
                return false;
            }

            return true;
        }

        /**
         * Checks after counting answers, wheter preconditions are fullfilled
         */
        _evaluateCheckAfter() {
            if (this._config.requireConfirmation) {
                if (!this._allConfirmationsGiven()) {
                    this._logger.log("Confirmation-Mode on but not everyone has confirmed.");
                    this._result.confirmed = false;
                    this._result.votingCorrect = false;
                } else {
                    this._result.confirmed = true;
                }
            }

            if (this._config.requireResponse) {
                if (!this._allResponsesGiven()) {
                    this._logger.log("Every player has to response.");
                    this._result.allResponded = false;
                    this._result.votingCorrect = false;
                } else {
                    this._result.allResponded = true;
                }
            }
        }

        /**
         * Checks, wheter selection of a Player is the same as correct option 
         * @param {Number} playerIndex Index of player
         */
        _checkSelection(playerIndex) {
            if (this.constructor.name == "Estimation") {
                return this._checkEstimation(playerIndex);
            }

            if (this._config.allowTrustment) {
                playerIndex = this._getEndTrustment(playerIndex);
            }

            var playersSelection = new Array();
            for (var i = 0; i < this._getResponseObject(playerIndex).response.length; i++) {
                if (this._getResponseObject(playerIndex).response[i]) {
                    playersSelection.push(i);
                }
            }
            if (arraysEqual(playersSelection, this._config.response.correct)) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Checks, wheter response of a given player is within the radius of the estimation
         * @param {Number} playerIndex 
         */
        _checkEstimation(playerIndex) {
            if (this._playerResponses[playerIndex].response >= this._config.estimation.intervalLowerBoundary && this._playerResponses[playerIndex].response <= this._config.estimation.intervalUpperBoundary) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Returns an array of given responses of a player (containing indecies of answers)
         * @param {Number} playerIndex 
         */
        _getGivenResponses(playerIndex) {
            if (this._config.allowTrustment) {
                playerIndex = this._getEndTrustment(playerIndex);
            }
            var res = new Array();
            for (var i = 0; i < this._getResponseObject(playerIndex).response.length; i++) {
                if (this._getResponseObject(playerIndex).response[i]) {
                    res.push(i);
                }
            }
            return res;
        }

        /**
         * Checks, wheter voting was unanimous
         */
        _checkUnanimity() {
            if (this.constructor.name == "Estimation") {
                for (var i = 0; i < this._playerResponses.length; i++) {
                    if (this._playerResponses[0].response == this._playerResponses[i].response) {
                        return false;
                    }
                }
                return true;
            } else {
                for (var i = 0; i < this._playerResponses.length; i++) {
                    if (!arraysEqual(this._playerResponses[0].response, this._playerResponses[i].response)) {
                        return false;
                    }
                }
                return true;
            }

        }

        /**
         * Checks, wheter the given counter array has a clear majority and returns information on that
         * @param {Array} counter 
         */
        _getMostTakenOption(counter) {
            let most = 0;
            var single = true;
            var clearMajority;
            for (let i = 1; i < counter.length; i++) {
                if (counter[most] == counter[i]) {
                    single = false;
                } else if (counter[most] < counter[i]) {
                    most = i;
                    single = true;
                }
            }
            var allGivenAnswers = counter.reduce((prev, curr) => prev + curr);
            var percentage = counter[most] / allGivenAnswers;
            var unanimous;
            if (percentage > 0.5) {
                clearMajority = true;
            } else {
                clearMajority = false;
            }
            if (percentage == 1) {
                unanimous = true;
            } else {
                unanimous = false;
            }
            return { most: most, single: single, clearMajority: clearMajority, percentage: percentage, unanimous: unanimous };
        }

        /**
         * fills the result object correct if mode is Decision
         */
        _helpEvaluationNoCorrect() {
            if (this.constructor.name == "Estimation") {
                this._result.playerCorrect = null;
                let average = 0;
                let counter = 0;
                for (var j = 0; j < this._players.length; j++) {
                    if (this._config.requireConfirmation) {
                        //only count confirmed Repsonses
                        if (this._confirmationGiven(j) && this._responseGiven(j)) {
                            average += this.getResponse(j);
                            counter++;
                        }
                    } else {
                        //all given responses are considered
                        if (this._responseGiven(j)) {
                            average += this.getResponse(j);
                            counter++;
                        }
                    }
                }
                if (this._config.requireConfirmation) {
                    this._result.confirmed = this._allConfirmationsGiven();
                }
                average = average / counter;
                this._result.decision = average;
                this._result.votingCorrect = true;

                return this._resultHandler();
            }

            this._result.playerCorrect = null;

            let takenOptions = this._getMostTakenOption(this._result.responseCounter);

            this._result.votingCorrect = takenOptions.clearMajority;

            this._result.decision = null;

            if (this._config.allowSupervisor) {
                if (this._result.votingCorrect) {
                    this._result.supervisorDecided = false;
                } else {
                    //get response of supervisor
                    let supervisorsDecision = null;
                    for (let i = 0; i < this._getGivenResponses(this._supervisor.id).length; i++) {
                        if (this._getGivenResponses(this._supervisor.id)[i]){
                            supervisorsDecision = i;
                        }
                    }
                    this._result.supervisorDecided = supervisorsDecision == null;
                    this._result.votingCorrect = supervisorsDecision != null;
                    this._result.decision = supervisorsDecision;
                }
            }

            if (this._config.requireConfirmation) {
                this._result.confirmed = this._allConfirmationsGiven();
                this._result.votingCorrect = this._result.votingCorrect && this._result.confirmed;
            }

            if (this._config.unanimous) {
                this._result.unanimous = takenOptions.unanimous;
                this._result.votingCorrect = this._result.votingCorrect && this._result.unanimous;
            }

            if (this._config.requireResponse) {
                this._result.allResponded = this._allResponsesGiven();
                this._result.votingCorrect = this._result.votingCorrect && this._result.allResponded;
            }

            if(!this._result.supervisorDecided){
                this._result.decision = takenOptions.most;
            }

            return this._resultHandler();
        }

        /**
         * Closes voting and evaluates voting. Returns whole voting instance containing the result object
         */
        evaluate() {
            if (!this._evaluateCheckBefore()) {
                return null;
            }

            if (this._config.allowSupervisor && !isUndefined(this._supervisor)) {
                var orig = this._getResponseObject(this._getPlayerIndex(this._supervisor.id));
                var clone = cloneObject(orig);
                this._playerResponses.push(clone);
            }

            this.nextState();

            //instanciate result object
            this._result = {
                playerCorrect: new Object(),
                responseCounter: new Array(),
                supervisorDecided: null,
                votingCorrect: null,
                unanimous: null,
                confirmed: null,
                allResponded: null,
                decision: null
            }


            if (this.constructor.name == "Estimation") {
                //Estimation has no response Counter 
                this._result["responseCounter"] = null;
            } else {
                //initialize response Counter
                for (var i = 0; i < this._config.response.options.length; i++) {
                    this._result["responseCounter"][i] = 0;
                }
                for (var j = 0; j < this._players.length; j++) {
                    if (this._responseGiven(j)) {
                        for (var k = 0; k < this._getGivenResponses(j).length; k++) {
                            this._result["responseCounter"][this._getGivenResponses(j)[k]]++;
                        }
                    }
                }
            }

            var numberOfResponses = 0;
            var numberOfCorrectResponses = 0;
            var supDecided = false;

            if ((this.constructor.name != "Estimation" && this._config.response.correct != null) || (this.constructor.name == "Estimation" && this._config.estimation != null)) {
                //for all modes except decision mode
                for (var i = 0; i < this._playerResponses.length; i++) {
                    //only if voting requires supervisor, i gets value of _players.length
                    if (i != this._players.length) {
                        //main case

                        if (!this._responseGiven(i)) {
                            this._result["playerCorrect"][this._players[i].id] = undefined;
                        } else {
                            numberOfResponses++;
                            if (this._checkSelection(i)) {
                                numberOfCorrectResponses++;
                                this._result["playerCorrect"][this._players[i].id] = true;
                            } else {
                                this._result["playerCorrect"][this._players[i].id] = false;
                            }
                        }
                    } else {
                        //if voting not clear until now, supervisor has to decide
                        if (!this._votingCorrect(numberOfCorrectResponses, numberOfResponses) && numberOfCorrectResponses * 2 == numberOfResponses) {
                            supDecided = true;
                            numberOfResponses++;
                            if (this._checkSelection(i)) {
                                numberOfCorrectResponses++;
                            }
                        }
                    }
                }
            } else {
                //decision mode
                return this._helpEvaluationNoCorrect();
            }

            var numberOfFalseResponses = numberOfResponses - numberOfCorrectResponses;

            this._result.votingCorrect = this._votingCorrect(numberOfCorrectResponses, numberOfResponses);

            if (this._config.requireConfirmation) {
                this._result.confirmed = this._allConfirmationsGiven();
                this._result.votingCorrect = this._result.votingCorrect && this._result.confirmed;
            }

            if (this._config.unanimous) {
                this._result.unanimous = numberOfFalseResponses == 0 && numberOfResponses > 0;
                this._result.votingCorrect = this._result.votingCorrect && this._result.unanimous;
            }

            if (this._config.allowSupervisor) {
                this._result.supervisorDecided = supDecided;
            }

            if (this._config.requireResponse) {
                this._result.allResponded = this._allResponsesGiven();
                this._result.votingCorrect = this._result.votingCorrect && this._result.allResponded;
            }

            return this._resultHandler();
        }

        _resultHandler() {
            this.nextState();
            if (this._config.distributed) {
                this._sendResult();
            }
            if (this._config.management.isManaged) {
                this._config.management.manager.handleResult();
            }
            if (this.result.votingCorrect == true) {
                this._delistenToDistributedActions();
            }
            return this;
        }

        /**
         * starts timer and then evaluates
         */
        async startTimer() {
            if (!this._config.timer.active) {
                this._logger.log("Voting not active.");
                return null;
            }
            return new Promise((res, rej) => {
                setTimeout(function () {
                    res(this.evaluate());
                }.bind(this), this._config.timer.duration * 1000);
            })
        }

        /**
         * Checks, wheter a the number of correct responses has a majority
         * @param {Number} correct 
         * @param {Number} all 
         */
        _votingCorrect(correct, all) {
            if (correct * 2 <= all) {
                return false;
            }
            return true;
        }

    }

    class SingleChoice extends VotingInstance {
        constructor(config) {
            super(config);
        }

        /**
         * Removes response
         * @param {Number} playerIndex 
         */
        removeResponse(playerId) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            this._logger.logAction(playerId, "removeResponse", null);
            return this.giveResponse(playerId, null);
        }

        /**
         * Gives Response, returns wheter failed or worked
         * @param {Number} playerId 
         * @param {Number} responseNumber 
         */
        giveResponse(playerId, responseNumber) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            var playerIndex = this._getPlayerIndex(playerId);

            if (!this._giveResponseCheck(playerIndex)) {
                return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
            }

            if (responseNumber == null || responseNumber == undefined) {
                this._playerResponses[playerIndex].clearResponses();
                this._logger.logAction(playerId, "giveResponse", "abstain");
                return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
            }

            this._playerResponses[playerIndex].clearResponses();
            this._playerResponses[playerIndex].response[responseNumber] = true;

            this._logger.logAction(playerId, "giveResponse", responseNumber);

            return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
        }

    }

    class MultipleChoice extends VotingInstance {
        constructor(config) {
            super(config);
        }

        /**
         * Removes response
         * @param {Number} playerId
         */
        removeResponse(playerId, responseNumber) {
            var playerIndex = this._getPlayerIndex(playerId);
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            if (!this._giveResponseCheck(playerIndex)) {
                return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
            }

            this._logger.logAction(playerId, "removeResponse", responseNumber);

            this._playerResponses[playerIndex].response[responseNumber] = false;

            return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
        }

        /**
         * Gives Response, returns wheter failed or worked
         * @param {Number} playerId 
         * @param {Number} responseNumber 
         */
        giveResponse(playerId, responseNumber) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            var playerIndex = this._getPlayerIndex(playerId);

            if (!this._giveResponseCheck(playerIndex)) {
                return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
            }

            if (responseNumber == null) {
                this._playerResponses[playerIndex].clearResponses();
                this._logger.logAction(playerId, "giveResponse", "abstain");
                return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
            }

            this._playerResponses[playerIndex].response[responseNumber] = true;

            this._logger.logAction(playerId, "giveResponse", this._getResponseObject(playerIndex).response);

            return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));
        }
    }

    class Estimation extends VotingInstance {
        constructor(config) {
            super(config);
        }

        /**
         * Removes response
         * @param {Number} playerId
         */
        removeResponse(playerId) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            this._logger.logAction(playerId, "removeResponse", null);
            this.giveResponse(playerId, null);
        }

        /**
         * Gives Response, returns wheter failed or worked
         * @param {Number} playerId 
         * @param {Number} responseNumber 
         */
        giveResponse(playerId, responseNumber) {
            if (this._state != stateEnum.OPENED) {
                return null;
            }
            var playerIndex = this._getPlayerIndex(playerId);

            if (!this._giveResponseCheck(playerIndex)) {
                return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));;
            }

            if (!responseNumber) {
                this._playerResponses[playerIndex].clearResponses();
                this._logger.logAction(playerId, "giveResponse", "abstain");
                return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));;
            }

            this._playerResponses[playerIndex].clearResponses();
            this._playerResponses[playerIndex].response = responseNumber;

            this._logger.logAction(playerId, "giveResponse", responseNumber);

            return this._syncSharedPlayerInformation(this._getPlayerIndex(playerId));;
        }
    }

    /**
     * In those objects the information on responses are stored.
     */
    class Response {
        constructor() {
            this._responseConfirmation = false;
            this._trustmentConfirmation = false;
        }

        set response(response) {
            this._response = response;
        }

        get response() {
            return this._response;
        }

        set responseConfirmation(conf) {
            this._responseConfirmation = conf;
        }

        get responseConfirmation() {
            return this._responseConfirmation;
        }

        set trustmentConfirmation(conf) {
            this._trustmentConfirmation = conf;
        }

        get trustmentConfirmation() {
            return this._trustmentConfirmation;
        }

        switchResponseConfirmation() {
            this._responseConfirmation = !this._responseConfirmation;
        }

        unconfirmResponse() {
            this._responseConfirmation = false;
        }

        confirmResponse() {
            this._responseConfirmation = true;
        }

        switchTrustmentConfirmation() {
            this._trustmentConfirmation = !this._trustmentConfirmation;
        }

        unconfirmTrustment() {
            this._trustmentConfirmation = false;
        }

        confirmTrustment() {
            this._trustmentConfirmation = true;
        }

    }

    class EstimationResponse extends Response {
        constructor() {
            super();
            this._response = undefined;
        }

        clearResponses() {
            this.response = undefined;
        }
    }

    class ChoiceResponse extends Response {
        constructor(numberOfOptions) {
            super();
            this._response = new Array();

            for (var i = 0; i < numberOfOptions; i++) {
                this._response[i] = false;
            }
        }

        clearResponses() {
            for (var i = 0; i < this._response.length; i++) {
                this.clearResponse(i);
            }
        }

        clearResponse(responseNumber) {
            this._response[responseNumber] = false;
        }

    }

    class VotingSession {
        constructor(config) {
            this._config = config;
            this._config.management.manager = this;
            this._votingInstances = new Array();
            this._votingInstances.push(VotingInstanceFactory(this._config));
            this.getCurrentVotingInstance()._sendInitializedInformation("newInstance", "room");
        }

        /**
         * Returns the current voting instance
         */
        getCurrentVotingInstance() {
            return this._votingInstances[this._votingInstances.length - 1];
        }

        /**
         * creates a new voting instance with the same parameters
         */
        _repeatVoting() {
            this._votingInstances.push(VotingInstanceFactory(this._config));
            this.getCurrentVotingInstance().players = this._votingInstances[this._votingInstances.length - 2].players;
            this.getCurrentVotingInstance()._mapDistributedIds = this._votingInstances[this._votingInstances.length - 2]._mapDistributedIds;
            this.getCurrentVotingInstance().openVoting();
        }

        /**
         * handles result of a voting
         */
        handleResult() {
            if (!this.getCurrentVotingInstance().result.votingCorrect) {
                this._repeatVoting();
            }
        }

        /**
         * Checks, whether the last evaluation was correct
         */
        isLastEvaluationCorrect() {
            if (this.getCurrentVotingInstance().result == null) {
                return false;
            } else {
                return true;
            }
        }

        /**
         * returns last evaluated result
         */
        getLastEvaluatedResult() {
            if (this.isLastEvaluationCorrect()) {
                return this.getCurrentVotingInstance();
            } else {
                return this._votingInstances[this._votingInstances.length - 2];
            }
        }

        /**
         * returns the number of votings
         */
        getNumberOfVotings() {
            return this._votingInstances.length;
        }

        /**
         * Checks 
         * @param {Number} playerId 
         */
        getPlayerCorrectness(playerId) {
            var res = new Array();
            var flag = false;
            if (!this.isLastEvaluationCorrect()) {
                flag = true;
            }
            for (var i = 0; i < this._votingInstances.length; i++) {
                //don't take current evaluation into account, if it has not been evaluated yet
                if (!(flag && i == this._votingInstances.length - 1)) {
                    res.push(this._votingInstances[i].result.playerCorrect[playerId]);
                }
            }
            return res;
        }
    }

    /**
     * sends a voting action to the room
     * @param {String} action 
     * @param  {...any} args if action requires additional parameters 
     */
    const sendVotingAction = function (action, ...args) {

        if (!predefinedActions.includes(action)) {
            return false;
        }

        MTLG.distributedDisplays.communication.sendCustomAction("room", action, MTLG.distributedDisplays.rooms.getClientId(), args);
        console.log("Sended Action '" + action + "' with paramters:");
        console.log(args);
        return true;
    }

    /**
     * Returns the index in the global player Array of a player with given id
     * @param {Number} id 
     */
    const getIndexOfGlobalPlayer = function (id) {
        for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
            if (MTLG.getPlayer(i).id == id) {
                return i;
            }
        }
        return null;
    }

    /**
     * Return index of the inserted player in the global player array
     * @param {Number} _id 
     * @param {String} _name 
     */
    const playerFactory = function (_id, _name) {
        if (null == getIndexOfGlobalPlayer(_id)) {
            var newPlayer = {
                id: _id,
                name: _name
            };
            return MTLG.addPlayer(newPlayer);
        } else {
            return null;
        }

    }


    return {
        createNewVotingInstance: VotingInstanceFactory,
        createNewVotingPlayer: playerFactory,
        createNewVotingSession: VotingSessionFactory,
        getIndexOfGlobalPlayer: getIndexOfGlobalPlayer,
        slave: {
            sendVotingAction: sendVotingAction
        }
    }
})();
module.exports = VotingModule;