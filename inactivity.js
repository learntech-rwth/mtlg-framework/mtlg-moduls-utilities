/**
 * This namespace provides functionality to deal with user inactivity.
 * An activity of a user includes e.g. moving the mouse, clicking, touching, ...
 * This namespace can be used to define functions that are called if the users are inactive.
 * @example
 * with(MTLG.utils.inactivity){ // Shorter than writing MTLG.utils.inactivity all the time
 *   //Create a listener that triggers after 3 seconds of inactivity
 *   var myListener = addListener(3, function(){
 *     console.log("Detected 3 seconds of inactivity!");
 *     removeAllListeners(); // Remove all listeners
 *   });
 *   //Create a second listener that triggers after 5 seconds.
 *   //It will never trigger as the 3 second version removes all listeners.
 *   var mySecondListener = addListener(5, function(){
 *     console.log("Detected 5 seconds of inactivity!");
 *     removeListener(myListener); // Remove Listener if it should not trigger again
 *   });
 * }
 * @namespace inactivity
 * @memberof MTLG.utils
 */

var inactivity  = (function(){
  var inactivityCallbacks = []; //Saves Times, callbacks and progress in array [[t1,cb1,p1], [t2,c2,p2]]

  /*
   * This function defines, which interactions reset the progress
   * and starts the interval that increments the progress each second by one
   */
  var resetTimer_init = function() {
    (document.all) ? document.onmousemove = () => resetResetTimer(): window.onmousemove = () => resetResetTimer();
    (document.all) ? document.onkeypress = () => resetResetTimer(): window.onkeypress = () => resetResetTimer();
    (document.all) ? document.onclick = () => resetResetTimer(): window.onclick = () => resetResetTimer();
    (document.all) ? document.ontouchmove = () => resetResetTimer(): window.ontouchmove = () => resetResetTimer();
    (document.all) ? document.ontouchstart = () => resetResetTimer(): window.ontouchstart = () => resetResetTimer();

    resetResetTimer();
    window.setInterval(function() {
      incrementInactivityCounter();
    }, 1000);
  };

  /**
   * Resets the progress back to 0
   * @parameter {number} index The index of the progress to be reset. Resets all if undefined or null
   * @memberof MTLG.utils.inactivity#
   */
  var resetResetTimer = function(index) {
    if(index === null || index === undefined){
      for(progressAr of inactivityCallbacks){
        progressAr[2] = 0;
      }
    }else{
      if(index >= 0 && index <= inactivityCallbacks.length && inactivityCallbacks[index]){
        inactivityCallbacks[index][2] = 0;
      }
    }
  };

  /*
   * Increments progress and reacts on timeout when given
   */
  var incrementInactivityCounter = function() {
    for(cb of inactivityCallbacks){
      // only when cb is active
      if(cb[4]) {
        cb[2] = parseInt(cb[2]) + 1;
        if (cb[2] === cb[0] + 1){
          (cb[1])();
          if (cb[3]) {
            cb[2] = 0; //TODO: Trigger once or repeatedly?
          }
        }
      }
    }
  };

  /**
   * Add a function that is called after the specified amount of time passed without interactions.
   * Until the next action from a user, the function is only called once, not repeatedly.
   * An action from the user resets the countdown, meaning that it can be triggered again.
   * Note that changing the level does not reset or delete this timer.
   * @param {number} time The time that should pass without interaction until cb is called in seconds
   * @param {function} callback The callback that is called after time has passed
   * @return {number} Returns an identifier to remove this listener
   * @memberof MTLG.utils.inactivity#
   */
  var addListener = function(time, callback, repeatedly = false){
    let active = true;
    inactivityCallbacks.push([time, callback, 0, repeatedly, active]);
    return inactivityCallbacks.length-1;
  }

  /**
   * Remove the specified listener
   * @param {number} index The identifier of the listener that was returned in addListener
   * @memberof MTLG.utils.inactivity#
   */
  var removeListener = function(index){
    if(index < 0 || index > inactivityCallbacks.length -1){
      //TODO: Log
      return;
    }
    //inactivityCallbacks.splice(index,1);
    inactivityCallbacks[index][4] = false; // set active flag to false. Do not remove Listner since then would the order and indices change
  }

  /**
   * Remove all listeners
   * @memberof MTLG.utils.inactivity#
   */
  var removeAllListeners = function(){ //TODO: plural of listener
    inactivityCallbacks = [];
  }

  return {
    addListener: addListener,
    resetResetTimer: resetResetTimer,
    removeListener: removeListener,
    removeAllListeners: removeAllListeners,
    init: resetTimer_init,
  }
})();
module.exports = inactivity;
